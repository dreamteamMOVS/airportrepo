﻿using System;
using System.Windows.Threading;

namespace Airport
{
    public static class TimeManager
    {
        private static DispatcherTimer _clockTimer;
        private static Airport _airport;
        private static bool _up;

        public static DateTime Time { get; set; }
        public static int Multiplier { get; set; }
        public static int Sec { get; set; }

        public static DateTime Past => Time.Subtract(TimeSpan.FromSeconds(Sec));

        static TimeManager()
        {
            Multiplier = 1;
            Sec = 1;
            SetupTimeSpeed();
        }

        public static void SetAirport(Airport airport)
        {
            _airport = airport;
        }

        /// <summary>
        ///  Диспатчер вызывает AddSecond каждый промежуток времени, равный interval.
        ///  Так как при минимальном интервале секунды все равно идут медленно,
        ///  то увеличивается количество прибавляемых секунд - sec.
        /// </summary>
        private static void SetupTimeSpeed()
        {
            _clockTimer = new DispatcherTimer();
            double interval = 1000.0 / Multiplier;
            _clockTimer.Interval = TimeSpan.FromMilliseconds(interval);
            if (interval < 16)
            {
                Sec = _up ? Sec *= 4 : Sec /= 4;
            }
            else
            {
                Sec = 1;
            }
            _clockTimer.Tick += AddSecond;
        }

        public static void Start()
        {
            _clockTimer.Start();
        }

        /// <summary>
        /// Обновление игрового времени на основной форме.
        /// </summary>
        private static void AddSecond(object sender, EventArgs e)
        {
            Time = Time.AddSeconds(Sec);
            _airport.UpdateTime();
        }

        /// <summary>
        /// Возвобновляет работу таймера
        /// </summary>
        public static void Resume()
        {
            if (!_clockTimer.IsEnabled)
            {
                _clockTimer.Start();
            }
        }

        /// <summary>
        /// Останавливает работу таймера
        /// </summary>
        public static void Suspend()
        {
            if (_clockTimer.IsEnabled)
            {
                _clockTimer.Stop();
            }
        }

        /// <summary>
        /// Увеличение скорости течения времени в 4 раза.
        /// </summary>
        public static void SpeedUp()
        {
            Dispose();
            _up = true;
            Multiplier *= 4;
            SetupTimeSpeed();
            Start();
        }

        /// <summary>
        /// Уменьшение скорости течения времени в 4 раза.
        /// </summary>
        public static void SlowDown()
        {
            Dispose();
            _up = false;
            Multiplier /= 4;
            SetupTimeSpeed();
            Start();
        }

        /// <summary>
        /// Очищение памяти.
        /// </summary>
        public static void Dispose()
        {
            _clockTimer.Stop();
            _clockTimer = null;
        }
    }
}