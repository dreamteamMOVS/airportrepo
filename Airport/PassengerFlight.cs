﻿using System;

namespace Airport
{
    public enum ERegularity { Single, Dayly, Weekly, Monthly }

    public struct SRegularity
    {
        public ERegularity Regularity;
        public string Time;

        /// <summary>
        /// Конструктор регулярности рейса.
        /// </summary>
        /// <param name="regularity">Частота.</param>
        /// <param name="time">Информация для игрока.</param>
        public SRegularity(ERegularity regularity, string time)
        {
            Regularity = regularity;
            Time = time;
        }
    }

    public struct PeakSpan
    {
        public int Begin;
        public int End;
        public string Time;

        /// <summary>
        /// Конструктор промежутка активности.
        /// </summary>
        /// <param name="begin">Точное время начала промежутка активности.</param>
        /// <param name="end">Точное время конца промежутка активности.</param>
        /// <param name="time">Информация для игрока.</param>
        public PeakSpan(int begin, int end, string time)
        {
            Begin = begin;
            End = end;
            Time = time;
        }
    }

    public class PassengerFlight : Flight
    {
        public PeakSpan Peak { get; set; }
        public SRegularity Regularity { get; set; }
        public int Price { get; set; }
        public DateTime BackTime { get; set; }
        public Plane PlaneForRegularFlights { get; set; }
        public int IdBack { get; set; }

        // Дефолтный конструктор необходим для сериализации. Не удалять
        public PassengerFlight()
        {
      
        }

        /// <summary>
        /// Конструктор пассажирского рейса.
        /// </summary>
        /// <param name="dt">Текущее игровое время.</param>
        /// <param name="seed">Начальное значение для генератора псевдослучайных чисел.</param>
        public PassengerFlight(DateTime dt, int seed) : base(seed)
        {
            Type = FlightType.Passenger;

            GenRegularity();
            GenPeak();
            GenPrice();
            GenDateEnd(dt);
        }

        /// <summary>
        /// Конструктор такого же объекта, нужен для регулярных рейсов
        /// </summary>

        public PassengerFlight(PassengerFlight f)
        {
            BackTime = f.BackTime;
            CityFrom = f.CityFrom;
            CityTo = f.CityTo;
            DateEnd = f.DateEnd;
            Distance = f.Distance;
            Forfeit = f.Forfeit;
            Income = f.Income;
            Peak = f.Peak;
            Price = f.Price;
            Regularity = f.Regularity;
            TimeFrom = f.TimeFrom;
            TimeTo = f.TimeTo;
            Type = f.Type;
            PlaneForRegularFlights = f.PlaneForRegularFlights;
        }

        /// <summary>
        /// Цена за билет на рейс.
        /// </summary>
        private void GenPrice()
        {
            Price = Rnd.Next(50, 200) * 100;
        }

        /// <summary>
        /// Крайний срок реализации рейса, после которого необходимо уплатить неустойку,
        /// на 1-15 дней позже даты его взятия для разовых и 1, 7, или 30 дней для регулярных соответственно.
        /// </summary>
        /// <param name="dt">Текущее игровое время.</param>
        protected void GenDateEnd(DateTime dt)
        {
            DateEnd = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
            switch (Regularity.Regularity)
            {
                case ERegularity.Single:
                    DateEnd = DateEnd.AddDays(Rnd.Next(1, 15));
                    break;

                case ERegularity.Dayly:
                    DateEnd = DateEnd.AddDays(1 + Rnd.Next(1));
                    break;

                case ERegularity.Weekly:
                    DateEnd = DateEnd.AddDays(7 + Rnd.Next(7));
                    break;

                case ERegularity.Monthly:
                    DateEnd = DateEnd.AddMonths(1);
                    break;
            }
        }

        public void CalcIncome()
        {
            Income = 5000;
        }

        /// <summary>
        /// Рейс с вероятностью ~40% является регулярным, ~60% - разовым.
        /// </summary>
        private void GenRegularity()
        {
            ERegularity reg = 0;
            string name = "";
            if (Rnd.NextDouble() > 0.4)
            {
                reg = ERegularity.Single;
                name = "Разовый";
            }
            else
            {
                switch (Rnd.Next(0, 3))
                {
                    case 0:
                        reg = ERegularity.Dayly;
                        name = "Ежедневный";
                        break;

                    case 1:
                        reg = ERegularity.Weekly;
                        name = "Еженедельный";
                        break;

                    case 2:
                        reg = ERegularity.Monthly;
                        name = "Ежемесячный";
                        break;
                }
            }
            Regularity = new SRegularity(reg, name);
        }

        /// <summary>
        /// Три возможных периода активности: ночь, день и вечер.
        /// У каждого случайные значения начала и конца промежутка.
        /// </summary>
        private void GenPeak()
        {
            switch (Rnd.Next(0, 3))
            {
                case 0:
                    Peak = new PeakSpan(Rnd.Next(21, 24), Rnd.Next(7, 10), "Ночь"); break;
                case 1:
                    Peak = new PeakSpan(Rnd.Next(7, 10), Rnd.Next(13, 16), "День"); break;
                case 2:
                    Peak = new PeakSpan(Rnd.Next(13, 16), Rnd.Next(21, 24), "Вечер"); break;
            }
        }
    }
}