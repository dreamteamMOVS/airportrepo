﻿using System;

namespace Airport
{
    public enum Post { Pilot, Stewardess}

    public class Worker
    {
        public string Nickname { get; set; }

        public int Salary { get; private set; }

        public int MinSalary { get; set; }

        public Post WorkType { get; set; }

        public void ChangeSalary(int salary)
        {
            if (salary < MinSalary)
            {
                throw new ArgumentException("На такую зарплату сотрудник не согласен!");
            }
            Salary = salary;
        }
    }
}
