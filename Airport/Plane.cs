﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using static Airport.Flight;

namespace Airport
{
    public enum PlaneType { Passenger, Cargo }

    public enum TradeStatus { Hangar, Purchased, Rented, Leasing }

    public enum UsingStatus { Ready, IsUsed }

    public static class IdAble
    {
        private static int _maxId;
        public static int GetNewId => _maxId++;
    }

    [Serializable]
    [XmlInclude(typeof(PlaneCargo))]
    [XmlInclude(typeof(PlanePassenger))]
    public class Plane
    {
        protected Random _rnd;
        protected UsingStatus _usage;
        public static DateTime Min = new DateTime(2000, 1, 1);

        public static int OchenDorogoySamolet319Count = 0;
        public static int EtoTozheSamolet25Count = 0;
        public static int Vzih511Count = 0;
        public static int Vzih512Count = 0;
        public static int Vzih513Count = 0;
        public static int Vzih621Count = 0;
        public static int Tu165Count = 0;

        public double OldCrewAverageSalary { get; set; }
        public List<Worker> Stewardesses { get; set; }
        public int RequireStewardessess { get; set; }
        public bool IsCheckedForAge { get; set; }
        public DateTime NextPayment { get; set; }
        public City CurrentLocation { get; set; }
        public List<Worker> Pilots { get; set; }
        public int NewServicePrice { get; set; }
        public int FuelConsumption { get; set; }
        public TradeStatus Status { get; set; }
        public int LeasingBuyCost { get; set; }
        public int NewPlanePrice { get; set; }
        public int RequirePilots { get; set; }
        public int ServicePrice { get; set; }
        public DateTime Created { get; set; }
        public int FlightCount { get; set; }
        public PlaneType Type { get; set; }
        public int PlanePrice { get; set; }
        public int LeasePrice { get; set; }
        public bool IsStrike { get; set; }
        public string Model { get; set; }
        public int MaxRange { get; set; }
        public int Speed { get; set; }
        public int Id { get; set;}

        public TimeSpan Age => TimeManager.Time - Created;
        public double Avg => (OchenDorogoySamolet319Count + EtoTozheSamolet25Count + Vzih511Count
            + Vzih512Count + Vzih513Count + Vzih621Count + Tu165Count) / 7.0;

        public UsingStatus Usage
        {
            get => _usage;
            set
            {
                _usage = value;
                if (_usage == UsingStatus.IsUsed) FlightCount++;
            }
        }

        public Plane()
        {
            Pilots = new List<Worker>();
            Stewardesses = new List<Worker>();
            IsCheckedForAge = false;
        }

        public Plane(Random rnd)
        {
            _rnd = rnd;
            Stewardesses = new List<Worker>();
            Pilots = new List<Worker>();
            IsCheckedForAge = false;
        }

        public void MakeOld()
        {
            Created = Min.AddDays(_rnd.Next((TimeManager.Time - TimeSpan.FromDays(20) - Min).Days));
        }

        public void UpdatePlane()
        {
            if ((TimeManager.Past - Created).Days / (double) (TimeManager.Time - Min).Days <= 0.7 &&
                (TimeManager.Time - Created).Days / (double) (TimeManager.Time - Min).Days > 0.7)
            {
                IsCheckedForAge = false;
            }

            int max = (TimeManager.Time - TimeSpan.FromDays(20) - Min).Days;

            // 30 полетов == 5% износа
            var flightKoeff = 1 - FlightCount / 30.0 * 0.05;
            if (flightKoeff < 0.2) flightKoeff = 0.2;
            
            var ageKoeff = (max - Age.Days + 20) / (double) max;
            if (ageKoeff < 0.2) ageKoeff = 0.2;

            PlanePrice = (int)(NewPlanePrice * ageKoeff * flightKoeff);
            ServicePrice = (int)(NewServicePrice * (2 - flightKoeff) * (2 - ageKoeff));
            LeasePrice = (int)(PlanePrice / 12.0);
            LeasingBuyCost = PlanePrice - LeasePrice;
        }

        protected int GetNewPrice(int modelCount, int price)
        {
            var result = modelCount / Avg > 1.15 ? (int)(price * 1.15) : price;

            return result;
        }

        public double CrewAverageSalary
        {
            get
            {
                int crewCount = Pilots.Count + Stewardesses.Count;
                int crewSalary = Pilots.Sum(pilot => pilot.Salary) + Stewardesses.Sum(steward => steward.Salary);
                if (crewCount > 0)
                    return crewSalary / (double) crewCount;
                return -1;
            }
        }

        public string TypeName()
        {
            switch (Type)
            {
                case PlaneType.Cargo:
                    return "Грузовой";

                case PlaneType.Passenger:
                    return "Пассажирский";
            }
            return "";
        }

        public void AddWorker(Worker worker)
        {
            switch (worker.WorkType)
            {
                    case Post.Pilot:
                    if (Pilots.Count>=RequirePilots)
                        throw new ArgumentException("Пилотов уже хватает!");
                    else
                    {
                        Pilots.Add(worker);
                    }
                    break;
                    case Post.Stewardess:
                    if (Stewardesses.Count>=RequireStewardessess)
                        throw new ArgumentException("Стюардесс уже хватает!");
                    else
                    {
                        Stewardesses.Add(worker);
                    }
                    break;
            }
        }

        public void DeleteWorker(Worker worker)
        {
            if (worker == null)
                return;
            switch (worker.WorkType)
            {
                case Post.Pilot:
                    Pilots.Remove(worker);
                    break;
                case Post.Stewardess:
                    Stewardesses.Remove(worker);
                    break;
            }
        }

        public void UpdateOldCrewAverageSalary()
        {
            OldCrewAverageSalary = CrewAverageSalary;
        }
    }

    public class PlaneCargo : Plane
    {
        public int CarryingCapacity { get; set; }

        public PlaneCargo()
        {
            Type = PlaneType.Cargo;
        }

        public PlaneCargo(Random rnd) : base(rnd)
        {
            Type = PlaneType.Cargo;
            Pilots = new List<Worker>();
            Stewardesses = new List<Worker>();
        }

        public void SetTu165()
        {
            Id = IdAble.GetNewId;
            Status = TradeStatus.Hangar;
            Usage = UsingStatus.Ready;
            Created = TimeManager.Time - TimeSpan.FromDays(20);
            Model = "Ту-165";
            Speed = 400;
            PlanePrice = GetNewPrice(Tu165Count, 20000);
            NewPlanePrice = PlanePrice;
            LeasePrice = (int)(PlanePrice / 12.0);
            ServicePrice = 200;
            NewServicePrice = ServicePrice;
            MaxRange = 3500;
            FuelConsumption = 30;
            CarryingCapacity = 600;
            RequirePilots = 2;
            RequireStewardessess = 0;
        }

        public void SetVzih621()
        {
            Id = IdAble.GetNewId;
            Status = TradeStatus.Hangar;
            Usage = UsingStatus.Ready;
            Created = TimeManager.Time - TimeSpan.FromDays(20);
            Speed = 650;
            Model = "Вжих-621";
            PlanePrice = 20000;
            PlanePrice = GetNewPrice(Vzih621Count, 20000);
            NewPlanePrice = PlanePrice;
            LeasePrice = (int)(PlanePrice / 12.0);
            ServicePrice = 450;
            NewServicePrice = ServicePrice;
            MaxRange = 6000;
            FuelConsumption = 30;
            CarryingCapacity = 300;
            RequirePilots = 2;
            RequireStewardessess = 0;
        }
    }

    public class PlanePassenger : Plane
    {
        public int Spaciousness { get; set; }

        public PlanePassenger()
        {
            Type = PlaneType.Passenger;
        }

        public PlanePassenger(Random rnd) : base(rnd)
        {
            Type = PlaneType.Passenger;
            Pilots = new List<Worker>();
            Stewardesses = new List<Worker>();
            Spaciousness = _rnd.Next(12, 40) * 5;
        }

        public void SetVzih511()
        {
            Id = IdAble.GetNewId;
            Status = TradeStatus.Hangar;
            Usage = UsingStatus.Ready;
            Created = TimeManager.Time - TimeSpan.FromDays(20);
            Speed = 700;
            Model = "Вжих-511";
            PlanePrice = 10000;
            PlanePrice = GetNewPrice(Vzih511Count, 20000);
            NewPlanePrice = PlanePrice;
            LeasePrice = (int)(PlanePrice / 12.0);
            ServicePrice = 100;
            NewServicePrice = ServicePrice;
            MaxRange = 3000;
            FuelConsumption = 10;
            Spaciousness = 180;
            RequirePilots = 2;
            RequireStewardessess = 6;
        }

        public void SetEtoTozheSamolet25()
        {
            Id = IdAble.GetNewId;
            Status = TradeStatus.Hangar;
            Usage = UsingStatus.Ready;
            Created = TimeManager.Time - TimeSpan.FromDays(20);
            Speed = 700;
            Model = "Это тоже самолёт-25";
            PlanePrice = 3000;
            PlanePrice = GetNewPrice(EtoTozheSamolet25Count, 20000);
            NewPlanePrice = PlanePrice;
            LeasePrice = (int)(PlanePrice / 12.0);
            ServicePrice = 80;
            NewServicePrice = ServicePrice;
            MaxRange = 1000;
            FuelConsumption = 12;
            Spaciousness = 50;
            RequirePilots = 2;
            RequireStewardessess = 2;
        }

        public void SetOchenDorogoySamolet319()
        {
            Id = IdAble.GetNewId;
            Status = TradeStatus.Hangar;
            Usage = UsingStatus.Ready;
            Created = TimeManager.Time - TimeSpan.FromDays(20);
            Speed = 750;
            Model = "Очень дорогой самолёт-319";
            PlanePrice = 100000;
            PlanePrice = GetNewPrice(OchenDorogoySamolet319Count, 20000);
            NewPlanePrice = PlanePrice;
            LeasePrice = (int)(PlanePrice / 12.0);
            ServicePrice = 1000;
            NewServicePrice = ServicePrice;
            MaxRange = 10000;
            FuelConsumption = 50;
            Spaciousness = 1000;
            RequirePilots = 3;
            RequireStewardessess = 40;
        }

        public void SetVzih512()
        {
            Id = IdAble.GetNewId;
            Status = TradeStatus.Hangar;
            Usage = UsingStatus.Ready;
            Created = TimeManager.Time - TimeSpan.FromDays(20);
            Speed = 700;
            Model = "Вжих-512";
            PlanePrice = 10001;
            PlanePrice = GetNewPrice(Vzih512Count, 20000);
            NewPlanePrice = PlanePrice;
            LeasePrice = (int)(PlanePrice / 12.0);
            ServicePrice = 110;
            NewServicePrice = ServicePrice;
            MaxRange = 2000;
            FuelConsumption = 18;
            Spaciousness = 170;
            RequirePilots = 2;
            RequireStewardessess = 6;
        }

        public void SetVzih513()
        {
            Id = IdAble.GetNewId;
            Status = TradeStatus.Hangar;
            Usage = UsingStatus.Ready;
            Created = TimeManager.Time - TimeSpan.FromDays(20);
            Speed = 700;
            Model = "Вжих-513";
            PlanePrice = 10002;
            PlanePrice = GetNewPrice(Vzih513Count, 20000);
            NewPlanePrice = PlanePrice;
            LeasePrice = (int)(PlanePrice / 12.0);
            ServicePrice = 200;
            NewServicePrice = ServicePrice;
            MaxRange = 1500;
            FuelConsumption = 12;
            Spaciousness = 140;
            RequirePilots = 2;
            RequireStewardessess = 5;
        }
    }
}