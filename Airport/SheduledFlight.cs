﻿using System;
using static Airport.Flight;

namespace Airport
{
    [Serializable]
    public class ScheduledFlight
    {
        public enum EStatus { Pending, Executing }

        public Plane Plane { get; set; }
        public int IdBack { get; set; }
        public EStatus Status { get; set; }
        public string Model { get; set; }
        public FlightType Type { get; set; }
        public DateTime TimeFrom { get; set; }
        public DateTime TimeTo { get; set; }
        public City CityFrom { get; set; }
        public City CityTo { get; set; }
        public double Distance { get; set; }
        public int Income { get; set; }
        public int Forfeit { get; set; }

        // Дефолтный конструктор необходим для сериализации. Не удалять
        public ScheduledFlight()
        {
        }
        
        public ScheduledFlight(Plane plane, string model, FlightType type, DateTime timeFrom, DateTime timeTo, City cityFrom, City cityTo, double distance, int income, int forfeit, int idBack)
        {
            Status = EStatus.Pending;
            Plane = plane;
            Model = model;
            Type = type;
            TimeFrom = timeFrom;
            TimeTo = timeTo;
            CityFrom = cityFrom;
            CityTo = cityTo;
            Distance = distance;
            Income = income;
            Forfeit = forfeit;
            IdBack = idBack;
        }
    }
}