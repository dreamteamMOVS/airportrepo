﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Airport
{
    public partial class Airport : Form
    {
        private int balance = 5000000;
        private DateTime time;
        private TimeManager timer;
        private int maxmult = 1024;

        List<Plane> planes;
        public List<Plane> Planes
        {
            get
            {
                return planes;
            }
            set
            {
                planes = value;
                FillPlanes();
            }
        } 
        
        public void UpdateTime(double sec)
        {
            time = time.Add(TimeSpan.FromSeconds(sec));

            mcCalendar.SetDate(time);
            lbCurrTime.Text = String.Format("{0:HH:mm:ss}", time);
        }

        public DateTime GetCurrentTime()
        {
            return time;
        }

        public int Balance
        {
            get { return balance; }
            set
            {
                balance = value;
                tbBalance.Text = balance.ToString();
            }
        }
                
        public Airport()
        {
            InitializeComponent();
            time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            StartPosition = FormStartPosition.CenterScreen;
            timer = new TimeManager(this);
            UpdateTime(0);
            SyncBtns();

            CreatePlanes();

            Balance = 5000000;
            btnDown.BackgroundImage = Properties.Resources.double_left;
            btnDown.BackgroundImageLayout = ImageLayout.Zoom;
            btnUp.BackgroundImage = Properties.Resources.double_right;
            btnUp.BackgroundImageLayout = ImageLayout.Zoom;
        }

        void CreatePlanes()
        {
            // мб у пользователя есть изначально самолёты или они здесь загружаются
            Planes = new List<Plane>();
        }

        void FillPlanes()
        {
            dGVPlane.Rows.Clear();

            foreach (Plane plane in Planes)
            {
                int rowIndex = dGVPlane.Rows.Add();
                DataGridViewRow row = dGVPlane.Rows[rowIndex];

                row.Cells["modelAirplane"].Value = plane.Model;
                row.Cells["typeAirplane"].Value = plane.TypeName();
                row.Cells["volumeAirplane"].Value = plane is PlaneCargo ?
                    ((PlaneCargo)plane).CarryingCapacity :
                    ((PlanePassenger)plane).Spaciousness;
                row.Cells["distanceAirplane"].Value = plane.MaxRange;
                row.Cells["costOfService"].Value = plane.ServicePrice;
                row.Cells["consumptionAirplane"].Value = plane.FuelConsumption;
            }
        }

        private void btBuyOrRent_Click(object sender, EventArgs e)
        {
            BuyOrRent FormBuy = new BuyOrRent(this);
            this.Hide();
            FormBuy.ShowDialog();
            this.Show();
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            timer.SlowDown();
            SyncBtns();
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            timer.SpeedUp();
            SyncBtns();
        }

        private void SyncBtns()
        {
            int m = timer.Multiplier;
            lbCurrMult.Text = "x" + m.ToString();

            lbCurrMult.Left = btnDown.Right + (btnUp.Left - btnDown.Right) / 2 - lbCurrMult.Width / 2;

            btnDown.Enabled = m != 1;
            btnUp.Enabled = m != maxmult;
        }
    }
}