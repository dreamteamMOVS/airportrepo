﻿using System;
using static Airport.Flight;

namespace Airport.Utils
{
    public static class Info
    {
        private static Random rnd = new Random();
        private static double _fuelPrice = 20;
        private static DateTime _starTime;
        private static DateTime _endTime;
        private static bool _isUp;

        public static double FulePrice { get { return _fuelPrice; } }

        /// <summary>
        /// Вычисление расстояния между городами по их широте и долготе.
        /// </summary>
        /// <param name="from">Пункт отправления.</param>
        /// <param name="to">Пункт прибытия.</param>
        /// <returns>Расстояние.</returns>
        public static double GetDist(City from, City to)
        {
            // Перевод координат в радианы.
            double lat1 = from.Latitude * Math.PI / 180;
            double lat2 = to.Latitude * Math.PI / 180;
            double long1 = from.Longitude * Math.PI / 180;
            double long2 = to.Longitude * Math.PI / 180;

            // Косинусы и синусы широт, разности долгот.
            double cl1 = Math.Cos(lat1);
            double cl2 = Math.Cos(lat2);
            double sl1 = Math.Sin(lat1);
            double sl2 = Math.Sin(lat2);
            double delta = long2 - long1;
            double cdelta = Math.Cos(delta);
            double sdelta = Math.Sin(delta);

            // Вычисление длины большого круга.
            double y = Math.Sqrt(Math.Pow(cl2 * sdelta, 2) + Math.Pow(cl1 * sl2 - sl1 * cl2 * cdelta, 2));
            double x = sl1 * sl2 + cl1 * cl2 * cdelta;

            // Радиус земли равен 6372795 м.
            double ad = Math.Atan2(y, x);
            double dist = ad * 6372795;

            return Math.Round(dist / 1000);
        }

        public static void UpdateFuelPrice(DateTime time)
        {
            if (_endTime < time)
            {
                _starTime = time;
                _endTime = _starTime.AddDays(rnd.Next(1, 5));
                _isUp = rnd.NextDouble() > 0.5;
            }

            double incr;
            if (_isUp)
            {
                incr = (rnd.NextDouble() > 0.25 ? _fuelPrice : -_fuelPrice) / 4000;
            }
            else
            {
                incr = (rnd.NextDouble() > 0.75 ? _fuelPrice : -_fuelPrice) / 4000;
            }
            _fuelPrice += incr;
        }
    }
}