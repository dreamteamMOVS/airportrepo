﻿using System;
using Airport.Utils;
using static Airport.Utils.Info;

namespace Airport
{
    public enum FlightType { Passenger, Cargo }

    [Serializable]
    public class Flight
    {
        public static City[] Cities = {
            new City(55.755826, 37.6173, "Москва"), new City(59.9342802, 30.33509, "Санкт-Петербург"),
            new City(48.856614, 2.3522219, "Париж"), new City(32.0852999, 34.781767, "Тель-Авив"),
            new City(52.3702157, 4.89516789, "Амстердам"), new City(48.1351253, 11.58198, "Мюнхен"),
            new City(43.6028, 39.7341543, "Сочи"), new City(44.952117, 34.102417, "Симферополь"),
            //new City(44.8857008, 37.319919, "Анапа"), new City(51.5073509, -0.127758, "Лондон"),
            //new City(41.9027835, 12.4963655, "Рим"), new City(52.286974, 104.305, "Иркутск")
        };

        public struct City
        {
            public double Latitude;
            public double Longitude;
            public string Name;

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="latitude">Широта.</param>
            /// <param name="longitude">Долгота.</param>
            /// <param name="name">Название города.</param>
            public City(double latitude, double longitude, string name)
            {
                Latitude = latitude;
                Longitude = longitude;
                Name = name;
            }
        }

        protected Random Rnd;
        public City CityTo { get; set; }
        public City CityFrom { get; set; }
        public DateTime TimeFrom { get; set; }
        public DateTime TimeTo { get; set; }
        public double Distance { get; set; }
        public DateTime DateEnd { get; set; }
        public int Forfeit { get; set; }
        public FlightType Type { get; set; }
        public int Income { get; set; }

        // Дефолтный конструктор необходим для сериализации. Не удалять
        public Flight()
        {
        }

        /// <summary>
        /// Конструктор принимает параметром число, являющееся начальным
        /// значением для генератора псевдослучайных чисел.
        /// </summary>
        /// <param name="seed">Начальное значение.</param>
        public Flight(int seed)
        {
            Rnd = new Random(seed);

            GenPlaces();
            GenForfeit();
            Distance = GetDist(CityFrom, CityTo);
        }

        /// <summary>
        /// Два случайных неповторяющихся города из списка.
        /// </summary>
        protected void GenPlaces()
        {
            int from, to = Rnd.Next(0, Cities.Length);
            CityTo = Cities[to];
            do
            {
                from = Rnd.Next(0, Cities.Length);
            } while (from == to);
            CityFrom = Cities[from];
        }

        /// <summary>
        /// Неустойка - случайная сумма (5 000 - 10 000)
        /// </summary>
        protected void GenForfeit()
        {
            Forfeit = Rnd.Next(5000, 10000);
        }
    }
}