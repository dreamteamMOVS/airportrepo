﻿namespace Airport
{
    partial class BuyOrRent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuyOrRent));
            this.label1 = new System.Windows.Forms.Label();
            this.dGVBuy = new System.Windows.Forms.DataGridView();
            this.playerPlanesTabControl = new System.Windows.Forms.TabControl();
            this.purchasedTab = new System.Windows.Forms.TabPage();
            this.sellPurshaseBtn = new System.Windows.Forms.Button();
            this.dGVPurchased = new System.Windows.Forms.DataGridView();
            this.idPColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modelPColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pilotPColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stewardessPColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sellPColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typePColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.volumePColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnPSpeed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.distancePColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costOfServicePColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.consumptionPColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rentedTab = new System.Windows.Forms.TabPage();
            this.returnRentBtn = new System.Windows.Forms.Button();
            this.dGVRented = new System.Windows.Forms.DataGridView();
            this.idRColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modelRColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pilotRColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stewardessRColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nextPaymentRColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monthPriceRColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeRColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.volumeRColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnRSpeed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.distanceRColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costOfServiceRColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.consumptionRColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.leasingTab = new System.Windows.Forms.TabPage();
            this.buyLeasingButton = new System.Windows.Forms.Button();
            this.returnLeasingButton = new System.Windows.Forms.Button();
            this.dGVLeasing = new System.Windows.Forms.DataGridView();
            this.idLColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modelLColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pilotLColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stewardessLColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buyCostLColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nextPaymentLColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monthPriceLColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeLColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.volumeLColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnLSpeed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.distanceLColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costOfServiceLColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.consumptionLColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.okButton = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lbBalance = new System.Windows.Forms.Label();
            this.tbBalance = new System.Windows.Forms.TextBox();
            this.lbPrice = new System.Windows.Forms.Label();
            this.tbPrice = new System.Windows.Forms.TextBox();
            this.airportButton = new System.Windows.Forms.Button();
            this.modelColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pilotColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stewardessColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monthPriceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.volumeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnSpeed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.distanceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costOfServiceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.consumptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dGVBuy)).BeginInit();
            this.playerPlanesTabControl.SuspendLayout();
            this.purchasedTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGVPurchased)).BeginInit();
            this.rentedTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGVRented)).BeginInit();
            this.leasingTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGVLeasing)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(510, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Доступные самолёты";
            // 
            // dGVBuy
            // 
            this.dGVBuy.AllowUserToAddRows = false;
            this.dGVBuy.AllowUserToDeleteRows = false;
            this.dGVBuy.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dGVBuy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVBuy.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.modelColumn,
            this.pilotColumn,
            this.stewardessColumn,
            this.costColumn,
            this.monthPriceColumn,
            this.typeColumn,
            this.volumeColumn,
            this.clmnSpeed,
            this.distanceColumn,
            this.costOfServiceColumn,
            this.consumptionColumn});
            this.dGVBuy.Location = new System.Drawing.Point(0, 30);
            this.dGVBuy.MultiSelect = false;
            this.dGVBuy.Name = "dGVBuy";
            this.dGVBuy.ReadOnly = true;
            this.dGVBuy.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dGVBuy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dGVBuy.Size = new System.Drawing.Size(1154, 195);
            this.dGVBuy.TabIndex = 7;
            this.dGVBuy.SelectionChanged += new System.EventHandler(this.dGVBuy_SelectionChanged);
            // 
            // playerPlanesTabControl
            // 
            this.playerPlanesTabControl.Controls.Add(this.purchasedTab);
            this.playerPlanesTabControl.Controls.Add(this.rentedTab);
            this.playerPlanesTabControl.Controls.Add(this.leasingTab);
            this.playerPlanesTabControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.playerPlanesTabControl.Location = new System.Drawing.Point(0, 260);
            this.playerPlanesTabControl.Name = "playerPlanesTabControl";
            this.playerPlanesTabControl.SelectedIndex = 0;
            this.playerPlanesTabControl.Size = new System.Drawing.Size(1155, 283);
            this.playerPlanesTabControl.TabIndex = 9;
            // 
            // purchasedTab
            // 
            this.purchasedTab.Controls.Add(this.sellPurshaseBtn);
            this.purchasedTab.Controls.Add(this.dGVPurchased);
            this.purchasedTab.Location = new System.Drawing.Point(4, 22);
            this.purchasedTab.Name = "purchasedTab";
            this.purchasedTab.Padding = new System.Windows.Forms.Padding(3);
            this.purchasedTab.Size = new System.Drawing.Size(1147, 257);
            this.purchasedTab.TabIndex = 0;
            this.purchasedTab.Text = "Покупка";
            this.purchasedTab.UseVisualStyleBackColor = true;
            // 
            // sellPurshaseBtn
            // 
            this.sellPurshaseBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sellPurshaseBtn.Location = new System.Drawing.Point(1018, 227);
            this.sellPurshaseBtn.Name = "sellPurshaseBtn";
            this.sellPurshaseBtn.Size = new System.Drawing.Size(121, 22);
            this.sellPurshaseBtn.TabIndex = 3;
            this.sellPurshaseBtn.Text = "Продать";
            this.sellPurshaseBtn.UseVisualStyleBackColor = true;
            this.sellPurshaseBtn.Click += new System.EventHandler(this.sellPurshaseBtn_Click);
            // 
            // dGVPurchased
            // 
            this.dGVPurchased.AllowUserToAddRows = false;
            this.dGVPurchased.AllowUserToDeleteRows = false;
            this.dGVPurchased.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dGVPurchased.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVPurchased.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idPColumn,
            this.modelPColumn,
            this.pilotPColumn,
            this.stewardessPColumn,
            this.sellPColumn,
            this.typePColumn,
            this.volumePColumn,
            this.clmnPSpeed,
            this.distancePColumn,
            this.costOfServicePColumn,
            this.consumptionPColumn});
            this.dGVPurchased.Location = new System.Drawing.Point(0, 0);
            this.dGVPurchased.Name = "dGVPurchased";
            this.dGVPurchased.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dGVPurchased.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dGVPurchased.Size = new System.Drawing.Size(1147, 221);
            this.dGVPurchased.TabIndex = 2;
            this.dGVPurchased.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dGVPurchased_RowsAdded);
            this.dGVPurchased.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dGVPurchased_RowsRemoved);
            this.dGVPurchased.SelectionChanged += new System.EventHandler(this.dGVPurchased_SelectionChanged);
            // 
            // idPColumn
            // 
            this.idPColumn.HeaderText = "id";
            this.idPColumn.Name = "idPColumn";
            this.idPColumn.Visible = false;
            // 
            // modelPColumn
            // 
            this.modelPColumn.HeaderText = "Модель";
            this.modelPColumn.Name = "modelPColumn";
            this.modelPColumn.ReadOnly = true;
            // 
            // pilotPColumn
            // 
            this.pilotPColumn.HeaderText = "Пилоты";
            this.pilotPColumn.Name = "pilotPColumn";
            // 
            // stewardessPColumn
            // 
            this.stewardessPColumn.HeaderText = "Стюардессы";
            this.stewardessPColumn.Name = "stewardessPColumn";
            // 
            // sellPColumn
            // 
            this.sellPColumn.HeaderText = "Стоимость продажи";
            this.sellPColumn.Name = "sellPColumn";
            this.sellPColumn.ReadOnly = true;
            // 
            // typePColumn
            // 
            this.typePColumn.HeaderText = "Тип самолёта";
            this.typePColumn.Name = "typePColumn";
            this.typePColumn.ReadOnly = true;
            // 
            // volumePColumn
            // 
            this.volumePColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.volumePColumn.HeaderText = "Грузоподъёмность / вместительность";
            this.volumePColumn.Name = "volumePColumn";
            this.volumePColumn.ReadOnly = true;
            this.volumePColumn.Width = 208;
            // 
            // clmnPSpeed
            // 
            this.clmnPSpeed.HeaderText = "Скорость";
            this.clmnPSpeed.Name = "clmnPSpeed";
            // 
            // distancePColumn
            // 
            this.distancePColumn.HeaderText = "Дальность";
            this.distancePColumn.Name = "distancePColumn";
            this.distancePColumn.ReadOnly = true;
            // 
            // costOfServicePColumn
            // 
            this.costOfServicePColumn.HeaderText = "Стоимость обслуживания";
            this.costOfServicePColumn.Name = "costOfServicePColumn";
            this.costOfServicePColumn.ReadOnly = true;
            // 
            // consumptionPColumn
            // 
            this.consumptionPColumn.HeaderText = "Расход";
            this.consumptionPColumn.Name = "consumptionPColumn";
            this.consumptionPColumn.ReadOnly = true;
            // 
            // rentedTab
            // 
            this.rentedTab.Controls.Add(this.returnRentBtn);
            this.rentedTab.Controls.Add(this.dGVRented);
            this.rentedTab.Location = new System.Drawing.Point(4, 22);
            this.rentedTab.Name = "rentedTab";
            this.rentedTab.Padding = new System.Windows.Forms.Padding(3);
            this.rentedTab.Size = new System.Drawing.Size(1147, 257);
            this.rentedTab.TabIndex = 1;
            this.rentedTab.Text = "Аренда";
            this.rentedTab.UseVisualStyleBackColor = true;
            // 
            // returnRentBtn
            // 
            this.returnRentBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.returnRentBtn.Location = new System.Drawing.Point(1018, 227);
            this.returnRentBtn.Name = "returnRentBtn";
            this.returnRentBtn.Size = new System.Drawing.Size(121, 22);
            this.returnRentBtn.TabIndex = 5;
            this.returnRentBtn.Text = "Вернуть";
            this.returnRentBtn.UseVisualStyleBackColor = true;
            this.returnRentBtn.Click += new System.EventHandler(this.returnRentBtn_Click);
            // 
            // dGVRented
            // 
            this.dGVRented.AllowUserToAddRows = false;
            this.dGVRented.AllowUserToDeleteRows = false;
            this.dGVRented.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dGVRented.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVRented.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idRColumn,
            this.modelRColumn,
            this.pilotRColumn,
            this.stewardessRColumn,
            this.nextPaymentRColumn,
            this.monthPriceRColumn,
            this.typeRColumn,
            this.volumeRColumn,
            this.clmnRSpeed,
            this.distanceRColumn,
            this.costOfServiceRColumn,
            this.consumptionRColumn});
            this.dGVRented.Location = new System.Drawing.Point(0, 0);
            this.dGVRented.Name = "dGVRented";
            this.dGVRented.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dGVRented.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dGVRented.Size = new System.Drawing.Size(1147, 221);
            this.dGVRented.TabIndex = 4;
            this.dGVRented.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dGVPurchased_RowsAdded);
            this.dGVRented.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dGVPurchased_RowsRemoved);
            this.dGVRented.SelectionChanged += new System.EventHandler(this.dGVRented_SelectionChanged);
            // 
            // idRColumn
            // 
            this.idRColumn.HeaderText = "id";
            this.idRColumn.Name = "idRColumn";
            this.idRColumn.Visible = false;
            // 
            // modelRColumn
            // 
            this.modelRColumn.HeaderText = "Модель";
            this.modelRColumn.Name = "modelRColumn";
            this.modelRColumn.ReadOnly = true;
            // 
            // pilotRColumn
            // 
            this.pilotRColumn.HeaderText = "Пилоты";
            this.pilotRColumn.Name = "pilotRColumn";
            // 
            // stewardessRColumn
            // 
            this.stewardessRColumn.HeaderText = "Стюардессы";
            this.stewardessRColumn.Name = "stewardessRColumn";
            // 
            // nextPaymentRColumn
            // 
            this.nextPaymentRColumn.HeaderText = "Следующий платёж";
            this.nextPaymentRColumn.Name = "nextPaymentRColumn";
            this.nextPaymentRColumn.ReadOnly = true;
            // 
            // monthPriceRColumn
            // 
            this.monthPriceRColumn.HeaderText = "Сумма платежа";
            this.monthPriceRColumn.Name = "monthPriceRColumn";
            this.monthPriceRColumn.ReadOnly = true;
            // 
            // typeRColumn
            // 
            this.typeRColumn.HeaderText = "Тип самолёта";
            this.typeRColumn.Name = "typeRColumn";
            this.typeRColumn.ReadOnly = true;
            // 
            // volumeRColumn
            // 
            this.volumeRColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.volumeRColumn.HeaderText = "Грузоподъёмность / вместительность";
            this.volumeRColumn.Name = "volumeRColumn";
            this.volumeRColumn.ReadOnly = true;
            this.volumeRColumn.Width = 208;
            // 
            // clmnRSpeed
            // 
            this.clmnRSpeed.HeaderText = "Скорость";
            this.clmnRSpeed.Name = "clmnRSpeed";
            // 
            // distanceRColumn
            // 
            this.distanceRColumn.HeaderText = "Дальность";
            this.distanceRColumn.Name = "distanceRColumn";
            this.distanceRColumn.ReadOnly = true;
            // 
            // costOfServiceRColumn
            // 
            this.costOfServiceRColumn.HeaderText = "Стоимость обслуживания";
            this.costOfServiceRColumn.Name = "costOfServiceRColumn";
            this.costOfServiceRColumn.ReadOnly = true;
            // 
            // consumptionRColumn
            // 
            this.consumptionRColumn.HeaderText = "Расход";
            this.consumptionRColumn.Name = "consumptionRColumn";
            this.consumptionRColumn.ReadOnly = true;
            // 
            // leasingTab
            // 
            this.leasingTab.Controls.Add(this.buyLeasingButton);
            this.leasingTab.Controls.Add(this.returnLeasingButton);
            this.leasingTab.Controls.Add(this.dGVLeasing);
            this.leasingTab.Location = new System.Drawing.Point(4, 22);
            this.leasingTab.Name = "leasingTab";
            this.leasingTab.Size = new System.Drawing.Size(1147, 257);
            this.leasingTab.TabIndex = 2;
            this.leasingTab.Text = "Лизинг";
            this.leasingTab.UseVisualStyleBackColor = true;
            // 
            // buyLeasingButton
            // 
            this.buyLeasingButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buyLeasingButton.Location = new System.Drawing.Point(880, 227);
            this.buyLeasingButton.Name = "buyLeasingButton";
            this.buyLeasingButton.Size = new System.Drawing.Size(121, 22);
            this.buyLeasingButton.TabIndex = 6;
            this.buyLeasingButton.Text = "Купить";
            this.buyLeasingButton.UseVisualStyleBackColor = true;
            this.buyLeasingButton.Click += new System.EventHandler(this.buyLeasingButton_Click);
            // 
            // returnLeasingButton
            // 
            this.returnLeasingButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.returnLeasingButton.Location = new System.Drawing.Point(1018, 227);
            this.returnLeasingButton.Name = "returnLeasingButton";
            this.returnLeasingButton.Size = new System.Drawing.Size(121, 22);
            this.returnLeasingButton.TabIndex = 5;
            this.returnLeasingButton.Text = "Вернуть";
            this.returnLeasingButton.UseVisualStyleBackColor = true;
            this.returnLeasingButton.Click += new System.EventHandler(this.returnLeasingButton_Click);
            // 
            // dGVLeasing
            // 
            this.dGVLeasing.AllowUserToAddRows = false;
            this.dGVLeasing.AllowUserToDeleteRows = false;
            this.dGVLeasing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVLeasing.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idLColumn,
            this.modelLColumn,
            this.pilotLColumn,
            this.stewardessLColumn,
            this.buyCostLColumn,
            this.nextPaymentLColumn,
            this.monthPriceLColumn,
            this.typeLColumn,
            this.volumeLColumn,
            this.clmnLSpeed,
            this.distanceLColumn,
            this.costOfServiceLColumn,
            this.consumptionLColumn});
            this.dGVLeasing.Location = new System.Drawing.Point(0, 0);
            this.dGVLeasing.Name = "dGVLeasing";
            this.dGVLeasing.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dGVLeasing.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dGVLeasing.Size = new System.Drawing.Size(1147, 221);
            this.dGVLeasing.TabIndex = 4;
            this.dGVLeasing.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dGVPurchased_RowsAdded);
            this.dGVLeasing.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dGVPurchased_RowsRemoved);
            this.dGVLeasing.SelectionChanged += new System.EventHandler(this.dGVLeasing_SelectionChanged);
            // 
            // idLColumn
            // 
            this.idLColumn.HeaderText = "id";
            this.idLColumn.Name = "idLColumn";
            this.idLColumn.Visible = false;
            // 
            // modelLColumn
            // 
            this.modelLColumn.HeaderText = "Модель";
            this.modelLColumn.Name = "modelLColumn";
            this.modelLColumn.ReadOnly = true;
            // 
            // pilotLColumn
            // 
            this.pilotLColumn.HeaderText = "Пилоты";
            this.pilotLColumn.Name = "pilotLColumn";
            // 
            // stewardessLColumn
            // 
            this.stewardessLColumn.HeaderText = "Стюардессы";
            this.stewardessLColumn.Name = "stewardessLColumn";
            // 
            // buyCostLColumn
            // 
            this.buyCostLColumn.HeaderText = "Стоимость выкупа";
            this.buyCostLColumn.Name = "buyCostLColumn";
            this.buyCostLColumn.ReadOnly = true;
            // 
            // nextPaymentLColumn
            // 
            this.nextPaymentLColumn.HeaderText = "Следующий платёж";
            this.nextPaymentLColumn.Name = "nextPaymentLColumn";
            this.nextPaymentLColumn.ReadOnly = true;
            // 
            // monthPriceLColumn
            // 
            this.monthPriceLColumn.HeaderText = "Ежемесячный платёж";
            this.monthPriceLColumn.Name = "monthPriceLColumn";
            this.monthPriceLColumn.ReadOnly = true;
            // 
            // typeLColumn
            // 
            this.typeLColumn.HeaderText = "Тип самолёта";
            this.typeLColumn.Name = "typeLColumn";
            this.typeLColumn.ReadOnly = true;
            // 
            // volumeLColumn
            // 
            this.volumeLColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.volumeLColumn.HeaderText = "Грузоподъёмность / вместительность";
            this.volumeLColumn.Name = "volumeLColumn";
            this.volumeLColumn.ReadOnly = true;
            this.volumeLColumn.Width = 208;
            // 
            // clmnLSpeed
            // 
            this.clmnLSpeed.HeaderText = "Скорость";
            this.clmnLSpeed.Name = "clmnLSpeed";
            // 
            // distanceLColumn
            // 
            this.distanceLColumn.HeaderText = "Дальность";
            this.distanceLColumn.Name = "distanceLColumn";
            this.distanceLColumn.ReadOnly = true;
            // 
            // costOfServiceLColumn
            // 
            this.costOfServiceLColumn.HeaderText = "Стоимость обслуживания";
            this.costOfServiceLColumn.Name = "costOfServiceLColumn";
            this.costOfServiceLColumn.ReadOnly = true;
            // 
            // consumptionLColumn
            // 
            this.consumptionLColumn.HeaderText = "Расход";
            this.consumptionLColumn.Name = "consumptionLColumn";
            this.consumptionLColumn.ReadOnly = true;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.okButton.Location = new System.Drawing.Point(548, 233);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(121, 22);
            this.okButton.TabIndex = 11;
            this.okButton.Text = "Совершить сделку";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Items.AddRange(new object[] {
            "Покупка",
            "Аренда",
            "Лизинг"});
            this.comboBox1.Location = new System.Drawing.Point(230, 233);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 12;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.dGVBuy_SelectionChanged);
            // 
            // lbBalance
            // 
            this.lbBalance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbBalance.AutoSize = true;
            this.lbBalance.Location = new System.Drawing.Point(12, 236);
            this.lbBalance.Name = "lbBalance";
            this.lbBalance.Size = new System.Drawing.Size(85, 13);
            this.lbBalance.TabIndex = 14;
            this.lbBalance.Text = "Баланс игрока:";
            // 
            // tbBalance
            // 
            this.tbBalance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbBalance.CausesValidation = false;
            this.tbBalance.Location = new System.Drawing.Point(114, 233);
            this.tbBalance.Name = "tbBalance";
            this.tbBalance.ReadOnly = true;
            this.tbBalance.Size = new System.Drawing.Size(100, 20);
            this.tbBalance.TabIndex = 13;
            // 
            // lbPrice
            // 
            this.lbPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbPrice.AutoSize = true;
            this.lbPrice.Location = new System.Drawing.Point(367, 236);
            this.lbPrice.Name = "lbPrice";
            this.lbPrice.Size = new System.Drawing.Size(55, 13);
            this.lbPrice.TabIndex = 16;
            this.lbPrice.Text = "К оплате:";
            // 
            // tbPrice
            // 
            this.tbPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbPrice.Location = new System.Drawing.Point(428, 233);
            this.tbPrice.Name = "tbPrice";
            this.tbPrice.ReadOnly = true;
            this.tbPrice.Size = new System.Drawing.Size(100, 20);
            this.tbPrice.TabIndex = 15;
            // 
            // airportButton
            // 
            this.airportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.airportButton.Location = new System.Drawing.Point(1030, 233);
            this.airportButton.Name = "airportButton";
            this.airportButton.Size = new System.Drawing.Size(121, 22);
            this.airportButton.TabIndex = 17;
            this.airportButton.Text = "Аэропорт";
            this.airportButton.UseVisualStyleBackColor = true;
            this.airportButton.Click += new System.EventHandler(this.airportButton_Click);
            // 
            // modelColumn
            // 
            this.modelColumn.HeaderText = "Модель";
            this.modelColumn.Name = "modelColumn";
            this.modelColumn.ReadOnly = true;
            // 
            // pilotColumn
            // 
            this.pilotColumn.HeaderText = "Пилоты";
            this.pilotColumn.Name = "pilotColumn";
            this.pilotColumn.ReadOnly = true;
            // 
            // stewardessColumn
            // 
            this.stewardessColumn.HeaderText = "Стюардессы";
            this.stewardessColumn.Name = "stewardessColumn";
            this.stewardessColumn.ReadOnly = true;
            // 
            // costColumn
            // 
            this.costColumn.HeaderText = "Стоимость";
            this.costColumn.Name = "costColumn";
            this.costColumn.ReadOnly = true;
            // 
            // monthPriceColumn
            // 
            this.monthPriceColumn.HeaderText = "Ежемесячный платёж";
            this.monthPriceColumn.Name = "monthPriceColumn";
            this.monthPriceColumn.ReadOnly = true;
            // 
            // typeColumn
            // 
            this.typeColumn.HeaderText = "Тип самолёта";
            this.typeColumn.Name = "typeColumn";
            this.typeColumn.ReadOnly = true;
            // 
            // volumeColumn
            // 
            this.volumeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.volumeColumn.HeaderText = "Грузоподъёмность / вместимость";
            this.volumeColumn.Name = "volumeColumn";
            this.volumeColumn.ReadOnly = true;
            this.volumeColumn.Width = 189;
            // 
            // clmnSpeed
            // 
            this.clmnSpeed.HeaderText = "Скорость";
            this.clmnSpeed.Name = "clmnSpeed";
            this.clmnSpeed.ReadOnly = true;
            // 
            // distanceColumn
            // 
            this.distanceColumn.HeaderText = "Дальность";
            this.distanceColumn.Name = "distanceColumn";
            this.distanceColumn.ReadOnly = true;
            // 
            // costOfServiceColumn
            // 
            this.costOfServiceColumn.HeaderText = "Стоимость обслуживания";
            this.costOfServiceColumn.Name = "costOfServiceColumn";
            this.costOfServiceColumn.ReadOnly = true;
            // 
            // consumptionColumn
            // 
            this.consumptionColumn.HeaderText = "Расход";
            this.consumptionColumn.Name = "consumptionColumn";
            this.consumptionColumn.ReadOnly = true;
            // 
            // BuyOrRent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(1155, 543);
            this.Controls.Add(this.airportButton);
            this.Controls.Add(this.lbPrice);
            this.Controls.Add(this.tbPrice);
            this.Controls.Add(this.lbBalance);
            this.Controls.Add(this.tbBalance);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.playerPlanesTabControl);
            this.Controls.Add(this.dGVBuy);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(816, 469);
            this.Name = "BuyOrRent";
            this.Text = "Аренда/Покупка";
            this.Resize += new System.EventHandler(this.BuyOrRent_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dGVBuy)).EndInit();
            this.playerPlanesTabControl.ResumeLayout(false);
            this.purchasedTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dGVPurchased)).EndInit();
            this.rentedTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dGVRented)).EndInit();
            this.leasingTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dGVLeasing)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dGVBuy;
        private System.Windows.Forms.TabControl playerPlanesTabControl;
        private System.Windows.Forms.TabPage purchasedTab;
        private System.Windows.Forms.DataGridView dGVPurchased;
        private System.Windows.Forms.TabPage rentedTab;
        private System.Windows.Forms.Button sellPurshaseBtn;
        private System.Windows.Forms.Button returnRentBtn;
        private System.Windows.Forms.DataGridView dGVRented;
        private System.Windows.Forms.TabPage leasingTab;
        private System.Windows.Forms.Button returnLeasingButton;
        private System.Windows.Forms.DataGridView dGVLeasing;
        private System.Windows.Forms.Button buyLeasingButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label lbBalance;
        private System.Windows.Forms.TextBox tbBalance;
        private System.Windows.Forms.Label lbPrice;
        private System.Windows.Forms.TextBox tbPrice;
        private System.Windows.Forms.Button airportButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn idPColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelPColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pilotPColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stewardessPColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sellPColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typePColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn volumePColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnPSpeed;
        private System.Windows.Forms.DataGridViewTextBoxColumn distancePColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn costOfServicePColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn consumptionPColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idRColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelRColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pilotRColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stewardessRColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nextPaymentRColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn monthPriceRColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeRColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn volumeRColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnRSpeed;
        private System.Windows.Forms.DataGridViewTextBoxColumn distanceRColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn costOfServiceRColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn consumptionRColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idLColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelLColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pilotLColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stewardessLColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn buyCostLColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nextPaymentLColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn monthPriceLColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeLColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn volumeLColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnLSpeed;
        private System.Windows.Forms.DataGridViewTextBoxColumn distanceLColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn costOfServiceLColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn consumptionLColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pilotColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stewardessColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn costColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn monthPriceColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn volumeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnSpeed;
        private System.Windows.Forms.DataGridViewTextBoxColumn distanceColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn costOfServiceColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn consumptionColumn;
    }
}