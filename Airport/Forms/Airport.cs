﻿using Airport.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Airport.Utils;
using static Airport.ScheduledFlight;

namespace Airport
{
    public partial class Airport : Form
    {
        private Random _rnd;
        private int _balance;
        private int _maxmult = 1024;
        private List<PassengerFlight> _myPassFlights;
        private List<CargoFlight> _myCargoFlights;
        public List<ScheduledFlight> _myScheduledFlights;
        public WorkersForm _workersForm;
        public BuyOrRentSecondly _buyOrRentSecondlyForm;
        private bool _bLoadInProgress;
        private int _count = 1;
        public Competitor _competitor;
        private int _reputation;
        public bool IsCompFail = false;

        public CargoFlights CargoFlights;
        public PassengerFlights PassengerFlights;

        public delegate void RentUpdate();

        public event RentUpdate OnRentUpdate;

        public static int DaysDifferBetweenPayments = 1;

        private List<Plane> _planes;

        public List<Plane> Planes
        {
            get => _planes;
            set
            {
                _planes = value;
                FillPlanes();
            }
        }
        
        public int Balance
        {
            get
            {
                return _balance;
            }
            set
            {
                _balance = value;
                tbBalance.Text = _balance.ToString();
            }
        }

        public void FillPlanes()
        {
            dGVPlane.Rows.Clear();
            if (_planes == null)
                return;

            int sumPayments = 0;
            foreach (Plane plane in Planes)
            {
                int rowIndex = dGVPlane.Rows.Add();
                DataGridViewRow row = dGVPlane.Rows[rowIndex];

                row.Cells["modelAirplane"].Value = plane.Model;
                row.Cells["clmnAge"].Value = plane.Created.ToShortDateString();
                row.Cells["typeAirplane"].Value = plane.TypeName();
                row.Cells["clmnCurrLocation"].Value = plane.CurrentLocation.Name;
                row.Cells["volumeAirplane"].Value = plane is PlaneCargo ?
                    ((PlaneCargo)plane).CarryingCapacity :
                    ((PlanePassenger)plane).Spaciousness;
                row.Cells["distanceAirplane"].Value = plane.MaxRange;
                row.Cells["clmnSpeed"].Value = plane.Speed;
                row.Cells["costOfService"].Value = plane.ServicePrice;
                row.Cells["consumptionAirplane"].Value = plane.FuelConsumption;
                row.Cells["pilotColumn"].Value = plane.RequirePilots;
                row.Cells["stewardessColumn"].Value = plane.RequireStewardessess;
                row.Cells["strikeColumn"].Value = plane.IsStrike ? "Да" : "Нет";
                int leasePrice = (plane.Status == TradeStatus.Rented) ? plane.LeasePrice :
                    (plane.Status == TradeStatus.Leasing) ? System.Math.Min(plane.LeasePrice, plane.LeasingBuyCost) : 0;

                if (plane.NextPayment.Date <= TimeManager.Time.AddDays(1).Date)
                    sumPayments += leasePrice;
                row.Cells["leaseAirplane"].Value = leasePrice;
            }

            tbNextPayment.Text = sumPayments.ToString();
        }

        public int Reputation
        {
            get { return _reputation; }
            set
            {
                _reputation = value;
                tbReputation.Text = _reputation.ToString();
            }
        }

        public WorkersForm WorkersForm
        {
            get { return _workersForm; }
        }

        /// <summary>
        /// Текущее игровое время - начало текущих суток (если новая игра).
        /// Создание форм для выбора пассажирских и грузовых рейсов. Вызов генерации этих рейсов.
        /// Запуск игрового времени.
        /// </summary>
        public Airport()
        {
            InitializeComponent();
            _rnd = new Random();
            TimeManager.SetAirport(this);

            CreatePlanes();
            TimeManager.Start();

            dgvMyPassFlights.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvMyCargoFlights.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dGVSchedule.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            WindowState = FormWindowState.Minimized;
            Show();
            WindowState = FormWindowState.Normal;
        }

        /// <summary>
        /// Увеличение игрового времени. С началом новых суток вызывается
        /// генерация новых рейсов и удаление уже выбранных рейсов
        /// с истекшим сроком.
        /// </summary>
        public void UpdateTime()
        {
            mcCalendar.SetDate(TimeManager.Time.Date);
            lbCurrTime.Text = String.Format("{0:HH:mm:ss}", TimeManager.Time);
            _rnd.Next();
            if (tbTest.Text != _competitor.CompBalance.ToString())
                tbTest.Text = _competitor.CompBalance.ToString(); //тест, надо убрать

            if (_bLoadInProgress ||
                TimeManager.Time.Hour == 0 && TimeManager.Time.Minute == 0 && TimeManager.Time.Second == 0 ||
                TimeManager.Time.Hour < TimeManager.Past.Hour)
            {
                if (!_bLoadInProgress)
                {
                    CargoFlights.GenerateNew(_rnd);
                    PassengerFlights.GenerateNew(_rnd);
                    _workersForm.GenerateNew();
                    _buyOrRentSecondlyForm.CreateHangarPlanes();
                    CheckCompetitorAverageSalary();
                }
                if (TimeManager.Time.Day == 1)
                {
                    PaySalaries();
                }
                UpdateRent();
                CheckExpiration();
                _competitor.SettingsComp();
				UpdatePlanesAndReputation();
            }
            CheckSchedule();
			Info.UpdateFuelPrice(TimeManager.Time);
            _competitor.UpdateAll();
            //CheckBalance();
            _bLoadInProgress = false;
        }

        private void UpdatePlanesAndReputation()
        {
            Planes.ForEach(plane => plane.UpdatePlane());
            _buyOrRentSecondlyForm.UpdatePrices();
            FillPlanes();

            foreach (var plane in _planes)
            {
                if (!plane.IsCheckedForAge && plane.Age.Days / (double) (TimeManager.Time - Plane.Min).Days > 0.7)
                {
                    Reputation -= 5;
                    plane.IsCheckedForAge = true;
                }

                if (!plane.IsCheckedForAge && plane.Age.Days / (double)(TimeManager.Time - Plane.Min).Days < 0.1)
                {
                    Reputation += 5;
                    plane.IsCheckedForAge = true;
                }
            }
        }

        private void PaySalaries()
        {
            foreach (Plane plane in Planes)
            {
                foreach (Worker pilot in plane.Pilots)
                {
                    _balance -= pilot.Salary;
                }
                foreach (Worker stewardess in plane.Stewardesses)
                {
                    _balance -= stewardess.Salary;
                }
            }
            foreach (Worker worker in _workersForm.UnoccupiedWorkers)
            {
                _balance -= worker.Salary;
            }
            tbBalance.Text = _balance.ToString();
            MessageBox.Show("Время платить зарплату сотрудникам!");
        }

        private void UpdateRent()
        {
            if (Planes == null)
                return;

            foreach (Plane plane in Planes)
            {
                if (plane.Status != TradeStatus.Rented &&
                    plane.Status != TradeStatus.Leasing)
                    continue;

                if (plane.NextPayment.Date > TimeManager.Time.Date)
                    continue;

                plane.NextPayment = plane.NextPayment.AddDays(DaysDifferBetweenPayments);
                int leasePrice = 0;
                if (plane.Status == TradeStatus.Rented)
                    leasePrice = plane.LeasePrice;

                if (plane.Status == TradeStatus.Leasing)
                {
                    leasePrice = Math.Min(plane.LeasePrice, plane.LeasingBuyCost);
                    plane.LeasingBuyCost -= leasePrice;

                    // Если открыта форма с ангаром, то статус надо менять там, т.к. нужно обновлять таблицы с самолётами
                    if (OnRentUpdate == null && plane.LeasingBuyCost == 0)
                        plane.Status = TradeStatus.Purchased;
                }

                Balance -= leasePrice;
            }

            if (OnRentUpdate != null)
                OnRentUpdate();
            else
                FillPlanes();
        }

        private void CheckSchedule()
        {
            bool isChanged = _bLoadInProgress;
            for (int i = _myScheduledFlights.Count - 1; i >= 0; i--)
            {
                if (_myScheduledFlights[i].TimeTo <= TimeManager.Time)
                {
                    Balance += _myScheduledFlights[i].Income;
                    _myScheduledFlights[i].Plane.CurrentLocation = _myScheduledFlights[i].CityTo;
                    _myScheduledFlights[i].Plane.Usage = UsingStatus.Ready;
                    _myScheduledFlights.RemoveAt(i);
                    isChanged = true;
                    continue;
                }
                
                if (_myScheduledFlights[i].TimeFrom <= TimeManager.Time)
                {
                    if (_myScheduledFlights[i].Plane.Usage != UsingStatus.IsUsed)
                    {
                        if (_myScheduledFlights[i].Plane.IsStrike)
                        {
                            string flightType = _myScheduledFlights[i].Type == FlightType.Passenger ? "Пассажирский" : "Грузовой";
                            MessageBox.Show($"{flightType} рейс { _myScheduledFlights[i].CityFrom.Name}-{ _myScheduledFlights[i].CityTo.Name} не может быть выполнен из-за забастовки!");
                            StrikeScheduledFlight(_myScheduledFlights[i].Plane);
                            continue;
                        }
                        if (_myScheduledFlights[i].Plane.CurrentLocation.Name == _myScheduledFlights[i].CityFrom.Name)
                        {
                            _myScheduledFlights[i].Plane.CurrentLocation = _myScheduledFlights[i].CityTo;
                            _myScheduledFlights[i].Status = EStatus.Executing;
                            _myScheduledFlights[i].Plane.Usage = UsingStatus.IsUsed;
                        }
                        else
                        {
                            MessageBox.Show((_myScheduledFlights[i].Type == FlightType.Cargo ? "Грузовой" : "Пассажирский") + " рейс \"" +
                                _myScheduledFlights[i].CityFrom.Name + " - " + _myScheduledFlights[i].CityTo.Name +
                                "\" невозможен, так как назначенный самолет находится в городе " + _myScheduledFlights[i].Plane.CurrentLocation.Name);
                            Balance -= _myScheduledFlights[i].Forfeit;
                            Reputation -= 20;
                            if (_myScheduledFlights[i].Type == FlightType.Passenger)
                            {
                                _myPassFlights.Remove(_myPassFlights.Find(x => x.IdBack == _myScheduledFlights[i].IdBack));
                            }
                            _myScheduledFlights.RemoveAt(i);
                        }
                        isChanged = true;
                    }
                    else if (_myScheduledFlights[i].Status != EStatus.Executing)
                    {
                        MessageBox.Show((_myScheduledFlights[i].Type == FlightType.Cargo ? "Грузовой" : "Пассажирский") + " рейс \"" +
                            _myScheduledFlights[i].CityFrom.Name + " - " + _myScheduledFlights[i].CityTo.Name +
                            "\" невозможен, так как назначенный самолет уже используется");
                        Reputation -= 20;
                        Balance -= _myScheduledFlights[i].Forfeit;
                        if (_myScheduledFlights[i].Type == FlightType.Passenger)
                        {
                            _myPassFlights.Remove(_myPassFlights.Find(x => x.IdBack==_myScheduledFlights[i].IdBack));
                        }
                        _myScheduledFlights.RemoveAt(i);
                        isChanged = true;
                    }
                    continue;
                }

                if (_myScheduledFlights[i].Plane.CurrentLocation.Name != _myScheduledFlights[i].CityFrom.Name
                    && (_myScheduledFlights[i].TimeFrom - TimeSpan.FromHours(Info.GetDist(_myScheduledFlights[i].Plane.CurrentLocation, _myScheduledFlights[i].CityFrom) / _myScheduledFlights[i].Plane.Speed) - TimeSpan.FromHours(1)) < TimeManager.Time
                    && _myScheduledFlights[i].Plane.Usage != UsingStatus.IsUsed)
                {
                    if (MessageBox.Show("Самолет, назначенный на " + (_myScheduledFlights[i].Type == FlightType.Cargo ? "грузовой" : "пассажирский") + " рейс \"" +
                            _myScheduledFlights[i].CityFrom.Name + " - " + _myScheduledFlights[i].CityTo.Name +
                            "\" в данный момент находится в городе " + _myScheduledFlights[i].Plane.CurrentLocation.Name + ".\n\nНазначить пустой перелет в " +
                            _myScheduledFlights[i].CityFrom.Name + "?", "Внимание", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Plane p = _myScheduledFlights[i].Plane;
                        ScheduledFlight f = _myScheduledFlights[i];
                        AddEmpty(p, f.Type, f.TimeTo, p.CurrentLocation, f.CityFrom, f.Forfeit);
                    }
                    isChanged = true;
                }
            }
            if (isChanged) UpdateDGV();
        }

        /// <summary>
        /// Проверка баланса игрока.
        /// </summary>
        private void CheckBalance()
        {
            if (_balance < 0)
            {
                MessageBox.Show("Проиграл");
                ActiveForm?.Close();
                Close();
            }
        }

        /// <summary>
        /// Проверка каждого добавленного рейса обоих типов на истечение срока.
        /// При просрочке с баланса списывается неустойка.
        /// </summary>
        private void CheckExpiration()
        {
            bool isChanged = false;
            for (int i = _myPassFlights.Count - 1; i >= 0; i--)
            {
                if (_myPassFlights[i].DateEnd <= TimeManager.Time)
                {
                    MessageBox.Show("Срок для пассажирского рейса \"" + _myPassFlights[i].CityFrom.Name + " - " + _myPassFlights[i].CityTo.Name + "\" истёк.");
                    Balance -= _myPassFlights[i].Forfeit;
                    _myPassFlights.RemoveAt(i);
                    isChanged = true;
                }
            }

            for (int i = _myCargoFlights.Count - 1; i >= 0; i--)
            {
                if (_myCargoFlights[i].DateEnd <= TimeManager.Time)
                {
                    MessageBox.Show("Срок для грузового рейса \"" + _myCargoFlights[i].CityFrom.Name + " - " + _myCargoFlights[i].CityTo.Name + "\" истёк.");
                    Balance -= _myCargoFlights[i].Forfeit;
                    _myCargoFlights.RemoveAt(i);
                    isChanged = true;
                }
            }
            if (isChanged) UpdateDGV();
        }

        private void CreatePlanes()
        {
            CargoFlights = new CargoFlights();
            PassengerFlights = new PassengerFlights();
            _workersForm = new WorkersForm(this);

            //if (MessageBox.Show("Вы хотите загрузить сохранённую игру?", "Загрузка", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //    LoadGame();
            //else
                NewGame();

            _buyOrRentSecondlyForm = new BuyOrRentSecondly(this);
            StartPosition = FormStartPosition.CenterScreen;
            
            UpdateTime();
            SyncBtns();

            btnDown.BackgroundImage = Properties.Resources.double_left;
            btnDown.BackgroundImageLayout = ImageLayout.Zoom;
            btnUp.BackgroundImage = Properties.Resources.double_right;
            btnUp.BackgroundImageLayout = ImageLayout.Zoom;
            btAddPassFlightInSchedule.Enabled = dgvMyPassFlights.SelectedRows.Count > 0;
            btAddCargoFlightInSchedule.Enabled = dgvMyCargoFlights.SelectedRows.Count > 0;
        }

        private void NewGame()
        {
            Balance = 100000;
            Reputation = 100;
            TimeManager.Time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            Planes = new List<Plane>();
            _myPassFlights = new List<PassengerFlight>();
            _myCargoFlights = new List<CargoFlight>();
            _myScheduledFlights = new List<ScheduledFlight>();
            _competitor = new Competitor(this);
        }

        private void LoadGame()
        {
            _bLoadInProgress = true;

            XmlWrapper wrapper = GameXml.Load();
            if (wrapper == null)
            {
                NewGame();
                _bLoadInProgress = false;
                return;
            }

            Balance = wrapper.Balance;
            TimeManager.Time = wrapper.Time;
            Planes = wrapper.Planes;
            _myPassFlights = wrapper.SelectedPassFlights;
            PassengerFlights.Generated = wrapper.NotSelectedPassFlights;
            _myCargoFlights = wrapper.SelectedCargoFlights;
            CargoFlights.Generated = wrapper.NotSelectedCargoFlights;
            _myScheduledFlights = wrapper.ScheduledFlights;
        }

        private void SaveGame()
        {
            XmlWrapper wrapper = new XmlWrapper();
            wrapper.Balance = Balance;
            wrapper.Time = TimeManager.Time;
            wrapper.Planes = Planes;
            wrapper.SelectedPassFlights = _myPassFlights;
            wrapper.NotSelectedPassFlights = PassengerFlights.Generated;
            wrapper.SelectedCargoFlights = _myCargoFlights;
            wrapper.NotSelectedCargoFlights = CargoFlights.Generated;
            wrapper.ScheduledFlights = _myScheduledFlights;

            GameXml.SaveAs(wrapper);
        }

        private void btBuyOrRent_Click(object sender, EventArgs e)
        {
            BuyOrRent formBuy = new BuyOrRent(this);
            Hide();
            OnRentUpdate += formBuy.OnBalanceChanged;
            formBuy.ShowDialog();
            OnRentUpdate -= formBuy.OnBalanceChanged;
            Show();
        }

        private void btnSecMarket_Click(object sender, EventArgs e)
        {
            Hide();
            _buyOrRentSecondlyForm.ShowDialog();
            Show();
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            TimeManager.SlowDown();
            SyncBtns();
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            TimeManager.SpeedUp();
            SyncBtns();
        }

        /// <summary>
        /// Невозможность замедления игрового времени при минимальной скорости и увеличения
        /// при максимальной. Центрирование множителя относительно кнопок регулировки скорости течения времени.
        /// </summary>
        private void SyncBtns()
        {
            int m = TimeManager.Multiplier;
            lbCurrMult.Text = "x" + m.ToString();

            lbCurrMult.Left = btnDown.Right + (btnUp.Left - btnDown.Right) / 2 - lbCurrMult.Width / 2;

            btnDown.Enabled = m != 1;
            btnUp.Enabled = m != _maxmult;
        }

        private void btnAddPassFlights_Click(object sender, EventArgs e)
        {
            PassengerFlights.ShowDialog();
            if (PassengerFlights.State == DialogResult.OK)
            {
                _myPassFlights.AddRange(PassengerFlights.GetSelected());
                UpdateDGV();
            }
        }

        private void btnAddCargoFlights_Click(object sender, EventArgs e)
        {
            CargoFlights.ShowDialog();
            if (CargoFlights.State == DialogResult.OK)
            {
                _myCargoFlights.AddRange(CargoFlights.GetSelected());
                UpdateDGV();
            }
        }

        private void UpdateDGV()
        {
            int passInd = dgvMyPassFlights.SelectedRows.Count > 0 ? dgvMyPassFlights.SelectedRows[0].Index : 0;
            int cargoInd = dgvMyCargoFlights.SelectedRows.Count > 0 ? dgvMyCargoFlights.SelectedRows[0].Index : 0;
            dGVSchedule.Rows.Clear();
            dgvMyPassFlights.Rows.Clear();
            dgvMyCargoFlights.Rows.Clear();

            foreach (PassengerFlight p in _myPassFlights)
            {
                dgvMyPassFlights.Rows.Add(p.Type == FlightType.Cargo ? "Грузовой" : "Пассажирский", p.CityFrom.Name,
                    p.CityTo.Name, p.Regularity.Time, p.Distance, p.Price,
                    p.Peak.Time, p.DateEnd.ToShortDateString(), p.Forfeit);
                dgvMyPassFlights.Rows[dgvMyPassFlights.Rows.Count - 1].Tag = p;
            }

            foreach (CargoFlight c in _myCargoFlights)
            {
                dgvMyCargoFlights.Rows.Add(c.Type == FlightType.Cargo ? "Грузовой" : "Пассажирский", c.CityFrom.Name,
                    c.CityTo.Name, c.Distance,
                    c.Weight + " т.", c.DateEnd.ToShortDateString(), c.Income, c.Forfeit);
                dgvMyCargoFlights.Rows[dgvMyCargoFlights.Rows.Count - 1].Tag = c;
            }

            if (dgvMyPassFlights.Rows.Count > 0) dgvMyPassFlights.Rows[0].Selected = false;
            if (dgvMyCargoFlights.Rows.Count > 0) dgvMyCargoFlights.Rows[0].Selected = false;

            foreach (ScheduledFlight s in _myScheduledFlights)
            {
                dGVSchedule.Rows.Add(s.Status == EStatus.Executing ? "Выполняется" : "Готов", s.Model, s.Type == FlightType.Cargo ? "Грузовой" : "Пассажирский",
                    s.TimeFrom, s.TimeTo, s.CityFrom.Name, s.CityTo.Name, s.Distance, s.Income);
                dGVSchedule.Rows[dGVSchedule.Rows.Count - 1].Tag = s;
            }

            if (dgvMyPassFlights.Rows.Count > passInd) dgvMyPassFlights.Rows[passInd].Selected = true;
            if (dgvMyCargoFlights.Rows.Count > cargoInd) dgvMyCargoFlights.Rows[cargoInd].Selected = true;

            dGVSchedule.Sort(dGVSchedule.Columns["clmnTimeFrom"], System.ComponentModel.ListSortDirection.Ascending);
            FillPlanes();
            dGVSchedule.Refresh();
            dgvMyPassFlights.Refresh();
            dgvMyCargoFlights.Refresh();
        }

        /// <summary>
        /// Освобождение памяти.
        /// </summary>
        private void Airport_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Balance >= 0 && !IsCompFail)
                if (MessageBox.Show("Вы хотите сохранить изменения перед закрытием?", "Сохранение", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    SaveGame();

            dgvMyCargoFlights.DataSource = null;
            dgvMyPassFlights.DataSource = null;
            TimeManager.Dispose();
            CargoFlights.Close();
            PassengerFlights.Close();
        }

        /// <summary>
        /// Нажатие DEL при одной/нескольких выделенных строках таблицы.
        /// </summary>
        private void dgvMyPassFlights_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                foreach (DataGridViewRow row in dgvMyPassFlights.SelectedRows)
                {
                    if (_myPassFlights[row.Index].Regularity.Regularity==ERegularity.Single)
                        Balance -= _myPassFlights[row.Index].Forfeit;
                    _myPassFlights.Remove((PassengerFlight)row.Tag);
                }
                UpdateDGV();
            }
            e.Handled = true;
        }

        /// <summary>
        /// Нажатие DEL при одной/нескольких выделенных строках таблицы.
        /// </summary>
        private void dgvMyCargoFlights_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                foreach (DataGridViewRow row in dgvMyCargoFlights.SelectedRows)
                {
                    Balance -= _myCargoFlights[row.Index].Forfeit;
                    _myCargoFlights.Remove((CargoFlight)row.Tag);
                }
                UpdateDGV();
            }
            e.Handled = true;
        }

        private void dgvMyPassFlights_SelectionChanged(object sender, EventArgs e)
        {
            btAddPassFlightInSchedule.Enabled = dgvMyPassFlights.SelectedRows.Count > 0;
        }

        private void dgvMyCargoFlights_SelectionChanged(object sender, EventArgs e)
        {
            btAddCargoFlightInSchedule.Enabled = dgvMyCargoFlights.SelectedRows.Count > 0;
        }

        private void btAddCargoFlightInSchedule_Click(object sender, EventArgs e)
        {
            CargoFlight f = (CargoFlight)dgvMyCargoFlights.SelectedRows[0].Tag;
            CargoSchedule cargoSchedule = new CargoSchedule((CargoFlight)dgvMyCargoFlights.SelectedRows[0].Tag, this);
            cargoSchedule.ShowDialog();
            if (cargoSchedule.DialogResult == DialogResult.OK)
            {
                Plane p = cargoSchedule.Plane;
                _myCargoFlights.Remove(f);
                ScheduledFlight sch = new ScheduledFlight(p, p.Model, f.Type, f.TimeFrom, f.TimeTo, f.CityFrom, f.CityTo, f.Distance, f.Income, f.Forfeit, 0);
                _myScheduledFlights.Add(sch);
            }
            UpdateDGV();
        }

        private void btAddPassFlightInSchedule_Click(object sender, EventArgs e)
        {
            PassengerFlight f = (PassengerFlight)dgvMyPassFlights.SelectedRows[0].Tag;
            PassSchedule passSchedule = new PassSchedule((PassengerFlight)dgvMyPassFlights.SelectedRows[0].Tag, this);
            passSchedule.ShowDialog();
            if (passSchedule.DialogResult == DialogResult.OK)
            {
                Plane p = passSchedule.Plane;
                if (f.Regularity.Regularity == ERegularity.Single) _myPassFlights.Remove(f);
                ScheduledFlight sch = new ScheduledFlight(p, p.Model, f.Type, f.TimeFrom, f.TimeTo, f.CityFrom, f.CityTo, f.Distance, f.Income, f.Forfeit, 0);
                
                if (f.Regularity.Regularity != ERegularity.Single && f.BackTime == DateTime.MinValue)
                {
                    PassengerFlight back = new PassengerFlight(f);
                    Flight.City tmp = f.CityFrom;
                    back.CityFrom = f.CityTo;
                    back.CityTo = tmp;
                    back.BackTime = f.TimeTo;
                    switch (back.Regularity.Regularity)
                    {
                        case ERegularity.Dayly:
                            back.DateEnd = back.TimeTo.AddDays(1);
                            break;

                        case ERegularity.Weekly:
                            back.DateEnd = back.TimeTo.AddDays(7);
                            break;

                        case ERegularity.Monthly:
                            back.DateEnd = back.TimeTo.AddMonths(1);
                            break;
                    }
                    back.Regularity = new SRegularity(ERegularity.Single, back.Regularity.Time);
                    back.PlaneForRegularFlights = p;
                    back.IdBack = _count;
                    _myPassFlights.Add(back);
                    sch.IdBack = _count; //костыль
                    _count++;
                }
                _myScheduledFlights.Add(sch);
                if (f.Regularity.Regularity != ERegularity.Single) f.Price = Convert.ToInt32(Math.Round(f.Price * 0.95));
            }
            UpdateDGV();
        }


        private void Airport_Resize(object sender, EventArgs e)
        {
            dgvMyPassFlights.AutoResizeColumns();
            dgvMyCargoFlights.AutoResizeColumns();
            dGVPlane.AutoResizeColumns();
            dGVSchedule.AutoResizeColumns();

            if (WindowState == FormWindowState.Minimized)
            {
                TimeManager.Suspend();
            }
            else
            {
                TimeManager.Resume();
            }
        }

        private void btWorkers_Click(object sender, EventArgs e)
        {
            _workersForm.LoadPlanes(Planes);
            _workersForm.UpdateReputation();
            _workersForm.ShowDialog();
        }

        public void StrikeScheduledFlight(Plane plane)
        {
            ScheduledFlight flight = _myScheduledFlights.FirstOrDefault(f => f.Plane == plane);
            if (flight == null)
                return;
            _balance -= flight.Forfeit;
            _myScheduledFlights.Remove(flight);
            _reputation -= 20;
            UpdateDGV();
            
        }

        public void AddToSecondMarket(Plane currentPlane)
        {
            _buyOrRentSecondlyForm.AddToHangar(currentPlane);
		}
        public void AddEmpty(Plane plane, FlightType type, DateTime timeTo, Flight.City currLoc, Flight.City cityFrom, int forf)
        { 
            double dist = Info.GetDist(currLoc, cityFrom);
            ScheduledFlight sch = new ScheduledFlight(plane, plane.Model, type, TimeManager.Time, timeTo, currLoc, cityFrom, dist, 0, forf, 0);
            sch.TimeTo = TimeManager.Time.AddHours(sch.Distance / plane.Speed);

            sch.Income = - (int)(plane.FuelConsumption * sch.Distance * Info.FulePrice + plane.ServicePrice);
            _myScheduledFlights.Add(sch);
        }

        private void btnAd_Click(object sender, EventArgs e)
        {
            AdForm adform = new AdForm(this);
            adform.Show();
        }

        private void CheckCompetitorAverageSalary()
        {
            foreach (Plane plane in Planes)
            {
                double change = (double) plane.CrewAverageSalary / _competitor.AverageSalaryComp;
                if (change < 0.8 && !plane.IsStrike)
                {
                    MessageBox.Show($"Вы платите экипажу самолета {plane.Model} на 20% или даже еще меньше, чем средняя зарплата экипажа у конкурента!" +
                                    $" Экипаж объявляет забастовку!");
                    plane.IsStrike = true;
                    _workersForm.ChangeReputation(-2);
                }
                else if (plane.IsStrike && change >= 0.8)
                {
                    plane.IsStrike = false;
                    MessageBox.Show($"Экипаж самолета {plane.Model} прекратил забастовку!");
                    _workersForm.ChangeReputation(2);
                }
            }
            FillPlanes();
        }
    }
}