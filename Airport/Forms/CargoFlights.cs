﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Airport
{
    public partial class CargoFlights : Form
    {
        private List<CargoFlight> _generated;

        public List<CargoFlight> Generated
        {
            get => _generated;
            set
            {
                _generated = value;
                UpdateDGV();
            }
        }

        public DialogResult State { get; set; }

        public CargoFlights()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            foreach (DataGridViewColumn col in dgvAllCargoFlights.Columns)
            {
                col.ReadOnly = true;
            }
            dgvAllCargoFlights.Columns[0].ReadOnly = false;
            dgvAllCargoFlights.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            _generated = new List<CargoFlight>();
        }

        /// <summary>
        /// Создает рандомное количество грузовых рейсов.
        /// </summary>
        /// <param name="rnd">Генератор псевдослучайных чисел.</param>
        public void GenerateNew(Random rnd)
        {
            int num = rnd.Next(10, 20);
            for (int i = _generated.Count - 1; i >= 0; i--)
            {
                if (_generated[i].DateEnd <= TimeManager.Time || rnd.NextDouble() > 0.5)
                {
                    _generated.RemoveAt(i);
                }
            }
            for (int i = _generated.Count; i < num; i++)
            {
                _generated.Add(new CargoFlight(TimeManager.Time, rnd.Next(0, 10000)));
            }
            UpdateDGV();
        }

        /// <summary>
        /// Очищение таблицы и заполнение её из списка сгенерированных рейсов.
        /// </summary>
        private void UpdateDGV()
        {
            dgvAllCargoFlights.Rows.Clear();
            foreach (CargoFlight c in _generated)
            {
                dgvAllCargoFlights.Rows.Add(false, c.Type == FlightType.Cargo ? "Грузовой" : "Пассажирский", c.CityFrom.Name,
                    c.CityTo.Name, c.Distance,
                    c.Weight + " т.", c.DateEnd.ToShortDateString(), c.Income, c.Forfeit);
                dgvAllCargoFlights.Rows[dgvAllCargoFlights.Rows.Count - 1].Tag = c;
            }

            if (dgvAllCargoFlights.Rows.Count > 0)
            {
                dgvAllCargoFlights.Rows[0].Selected = false;
            }
            dgvAllCargoFlights.Refresh();
        }

        public List<CargoFlight> GetSelected()
        {
            List<CargoFlight> result = new List<CargoFlight>();
            foreach (DataGridViewRow row in dgvAllCargoFlights.Rows)
            {
                if ((bool)((DataGridViewCheckBoxCell)row.Cells[0]).Value)
                {
                    result.Add((CargoFlight)row.Tag);
                    _generated.Remove((CargoFlight)row.Tag);
                }
            }
            UpdateDGV();
            return result;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            State = DialogResult.OK;
            Hide();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            State = DialogResult.Cancel;
            Hide();
        }

        //private void CargoFlights_SizeChanged(object sender, EventArgs e)
        //{
        //    dgvAllCargoFlights.AutoResizeColumns();
        //}

        /// <summary>
        /// Нажатие DEL при одной/нескольких выделенных строках таблицы.
        /// </summary>
        private void dgvAllCargoFlights_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                foreach (DataGridViewRow row in dgvAllCargoFlights.SelectedRows)
                {
                    _generated.Remove((CargoFlight)row.Tag);
                }
                UpdateDGV();
            }
            e.Handled = true;
        }

        private void dgvAllCargoFlights_SelectionChanged(object sender, EventArgs e)
        {
            btnAdd.Enabled = dgvAllCargoFlights.SelectedRows.Count > 0;
        }

        /// <summary>
        /// Просто снятие выделения со всех строк таблицы.
        /// </summary>
        private void CargoFlights_Shown(object sender, EventArgs e)
        {
            UpdateDGV();
        }

        private void CargoFlights_Resize(object sender, EventArgs e)
        {
            dgvAllCargoFlights.AutoResizeColumns();

            if (WindowState == FormWindowState.Minimized)
            {
                TimeManager.Suspend();
            }
            else
            {
                TimeManager.Resume();
            }
        }
    }
}