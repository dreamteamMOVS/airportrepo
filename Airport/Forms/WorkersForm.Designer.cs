﻿namespace Airport.Forms
{
    partial class WorkersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbAirplane = new System.Windows.Forms.Label();
            this.dGVPlane = new System.Windows.Forms.DataGridView();
            this.modelAirplane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pilotColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stewardessColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.strikeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeAirplane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnCurrLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.volumeAirplane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvWorkers = new System.Windows.Forms.DataGridView();
            this.Никнейм = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCrew = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salaryColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.tbChangeSalary = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btChangeSalary = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tbNewSalary = new System.Windows.Forms.TextBox();
            this.btAddWorker = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvNonCrew = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.btFire = new System.Windows.Forms.Button();
            this.btAddToCrew = new System.Windows.Forms.Button();
            this.btOutCrew = new System.Windows.Forms.Button();
            this.tbReputation = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dGVPlane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWorkers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCrew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNonCrew)).BeginInit();
            this.SuspendLayout();
            // 
            // lbAirplane
            // 
            this.lbAirplane.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbAirplane.AutoSize = true;
            this.lbAirplane.Location = new System.Drawing.Point(385, 9);
            this.lbAirplane.Name = "lbAirplane";
            this.lbAirplane.Size = new System.Drawing.Size(118, 13);
            this.lbAirplane.TabIndex = 5;
            this.lbAirplane.Text = "Доступные самолёты";
            // 
            // dGVPlane
            // 
            this.dGVPlane.AllowUserToAddRows = false;
            this.dGVPlane.AllowUserToDeleteRows = false;
            this.dGVPlane.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dGVPlane.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVPlane.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.modelAirplane,
            this.pilotColumn,
            this.stewardessColumn,
            this.strikeColumn,
            this.typeAirplane,
            this.clmnCurrLocation,
            this.volumeAirplane});
            this.dGVPlane.Location = new System.Drawing.Point(12, 41);
            this.dGVPlane.Name = "dGVPlane";
            this.dGVPlane.ReadOnly = true;
            this.dGVPlane.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dGVPlane.Size = new System.Drawing.Size(853, 106);
            this.dGVPlane.TabIndex = 6;
            this.dGVPlane.SelectionChanged += new System.EventHandler(this.dGVPlane_SelectionChanged);
            // 
            // modelAirplane
            // 
            this.modelAirplane.HeaderText = "Модель";
            this.modelAirplane.Name = "modelAirplane";
            this.modelAirplane.ReadOnly = true;
            // 
            // pilotColumn
            // 
            this.pilotColumn.HeaderText = "Пилоты";
            this.pilotColumn.Name = "pilotColumn";
            this.pilotColumn.ReadOnly = true;
            // 
            // stewardessColumn
            // 
            this.stewardessColumn.HeaderText = "Стюардессы";
            this.stewardessColumn.Name = "stewardessColumn";
            this.stewardessColumn.ReadOnly = true;
            // 
            // strikeColumn
            // 
            this.strikeColumn.HeaderText = "Забастовка";
            this.strikeColumn.Name = "strikeColumn";
            this.strikeColumn.ReadOnly = true;
            // 
            // typeAirplane
            // 
            this.typeAirplane.HeaderText = "Тип самолёта";
            this.typeAirplane.Name = "typeAirplane";
            this.typeAirplane.ReadOnly = true;
            // 
            // clmnCurrLocation
            // 
            this.clmnCurrLocation.HeaderText = "Местоположение";
            this.clmnCurrLocation.Name = "clmnCurrLocation";
            this.clmnCurrLocation.ReadOnly = true;
            // 
            // volumeAirplane
            // 
            this.volumeAirplane.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.volumeAirplane.HeaderText = "Грузоподъёмность / вместительность";
            this.volumeAirplane.Name = "volumeAirplane";
            this.volumeAirplane.ReadOnly = true;
            this.volumeAirplane.Width = 208;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(83, 163);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Рынок труда";
            // 
            // dgvWorkers
            // 
            this.dgvWorkers.AllowUserToAddRows = false;
            this.dgvWorkers.AllowUserToDeleteRows = false;
            this.dgvWorkers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvWorkers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWorkers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Никнейм,
            this.workColumn});
            this.dgvWorkers.Location = new System.Drawing.Point(12, 179);
            this.dgvWorkers.Name = "dgvWorkers";
            this.dgvWorkers.ReadOnly = true;
            this.dgvWorkers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvWorkers.Size = new System.Drawing.Size(252, 219);
            this.dgvWorkers.TabIndex = 8;
            // 
            // Никнейм
            // 
            this.Никнейм.HeaderText = "Никнейм";
            this.Никнейм.Name = "Никнейм";
            this.Никнейм.ReadOnly = true;
            // 
            // workColumn
            // 
            this.workColumn.HeaderText = "Квалификация";
            this.workColumn.Name = "workColumn";
            this.workColumn.ReadOnly = true;
            // 
            // dgvCrew
            // 
            this.dgvCrew.AllowUserToAddRows = false;
            this.dgvCrew.AllowUserToDeleteRows = false;
            this.dgvCrew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCrew.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCrew.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.salaryColumn});
            this.dgvCrew.Location = new System.Drawing.Point(454, 179);
            this.dgvCrew.Name = "dgvCrew";
            this.dgvCrew.ReadOnly = true;
            this.dgvCrew.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCrew.Size = new System.Drawing.Size(348, 219);
            this.dgvCrew.TabIndex = 9;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Никнейм";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Квалификация";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // salaryColumn
            // 
            this.salaryColumn.HeaderText = "Зарплата";
            this.salaryColumn.Name = "salaryColumn";
            this.salaryColumn.ReadOnly = true;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(579, 163);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Экипаж самолета";
            // 
            // tbChangeSalary
            // 
            this.tbChangeSalary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbChangeSalary.Location = new System.Drawing.Point(841, 219);
            this.tbChangeSalary.MaxLength = 5;
            this.tbChangeSalary.Name = "tbChangeSalary";
            this.tbChangeSalary.Size = new System.Drawing.Size(100, 20);
            this.tbChangeSalary.TabIndex = 11;
            this.tbChangeSalary.TextChanged += new System.EventHandler(this.tbChangeSalary_TextChanged);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(838, 182);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Изменить зарплату";
            // 
            // btChangeSalary
            // 
            this.btChangeSalary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btChangeSalary.Location = new System.Drawing.Point(853, 263);
            this.btChangeSalary.Name = "btChangeSalary";
            this.btChangeSalary.Size = new System.Drawing.Size(75, 23);
            this.btChangeSalary.TabIndex = 13;
            this.btChangeSalary.Text = "Изменить";
            this.btChangeSalary.UseVisualStyleBackColor = true;
            this.btChangeSalary.Click += new System.EventHandler(this.btChangeSalary_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(281, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(150, 16);
            this.label4.TabIndex = 14;
            this.label4.Text = "Добавить в экипаж";
            // 
            // tbNewSalary
            // 
            this.tbNewSalary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbNewSalary.Location = new System.Drawing.Point(300, 231);
            this.tbNewSalary.MaxLength = 5;
            this.tbNewSalary.Name = "tbNewSalary";
            this.tbNewSalary.Size = new System.Drawing.Size(100, 20);
            this.tbNewSalary.TabIndex = 15;
            this.tbNewSalary.TextChanged += new System.EventHandler(this.tbNewSalary_TextChanged);
            // 
            // btAddWorker
            // 
            this.btAddWorker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btAddWorker.Location = new System.Drawing.Point(317, 275);
            this.btAddWorker.Name = "btAddWorker";
            this.btAddWorker.Size = new System.Drawing.Size(75, 23);
            this.btAddWorker.TabIndex = 16;
            this.btAddWorker.Text = "Добавить";
            this.btAddWorker.UseVisualStyleBackColor = true;
            this.btAddWorker.Click += new System.EventHandler(this.btAddWorker_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(324, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Зарплата";
            // 
            // dgvNonCrew
            // 
            this.dgvNonCrew.AllowUserToAddRows = false;
            this.dgvNonCrew.AllowUserToDeleteRows = false;
            this.dgvNonCrew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvNonCrew.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNonCrew.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dgvNonCrew.Location = new System.Drawing.Point(454, 417);
            this.dgvNonCrew.Name = "dgvNonCrew";
            this.dgvNonCrew.ReadOnly = true;
            this.dgvNonCrew.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvNonCrew.Size = new System.Drawing.Size(348, 181);
            this.dgvNonCrew.TabIndex = 18;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Никнейм";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Квалификация";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Зарплата";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(561, 401);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Незанятые сотрудники";
            // 
            // btFire
            // 
            this.btFire.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btFire.Location = new System.Drawing.Point(853, 447);
            this.btFire.Name = "btFire";
            this.btFire.Size = new System.Drawing.Size(75, 23);
            this.btFire.TabIndex = 20;
            this.btFire.Text = "Уволить";
            this.btFire.UseVisualStyleBackColor = true;
            this.btFire.Click += new System.EventHandler(this.btFire_Click);
            // 
            // btAddToCrew
            // 
            this.btAddToCrew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btAddToCrew.Location = new System.Drawing.Point(853, 489);
            this.btAddToCrew.Name = "btAddToCrew";
            this.btAddToCrew.Size = new System.Drawing.Size(75, 36);
            this.btAddToCrew.TabIndex = 21;
            this.btAddToCrew.Text = "Добавить в экипаж";
            this.btAddToCrew.UseVisualStyleBackColor = true;
            this.btAddToCrew.Click += new System.EventHandler(this.btAddToCrew_Click);
            // 
            // btOutCrew
            // 
            this.btOutCrew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btOutCrew.Location = new System.Drawing.Point(853, 307);
            this.btOutCrew.Name = "btOutCrew";
            this.btOutCrew.Size = new System.Drawing.Size(75, 36);
            this.btOutCrew.TabIndex = 22;
            this.btOutCrew.Text = "Убрать из экипажа";
            this.btOutCrew.UseVisualStyleBackColor = true;
            this.btOutCrew.Click += new System.EventHandler(this.btOutCrew_Click);
            // 
            // tbReputation
            // 
            this.tbReputation.Location = new System.Drawing.Point(81, 6);
            this.tbReputation.Name = "tbReputation";
            this.tbReputation.ReadOnly = true;
            this.tbReputation.Size = new System.Drawing.Size(45, 20);
            this.tbReputation.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Репутация:";
            // 
            // WorkersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 610);
            this.Controls.Add(this.tbReputation);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btOutCrew);
            this.Controls.Add(this.btAddToCrew);
            this.Controls.Add(this.btFire);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dgvNonCrew);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btAddWorker);
            this.Controls.Add(this.tbNewSalary);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btChangeSalary);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbChangeSalary);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvCrew);
            this.Controls.Add(this.dgvWorkers);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dGVPlane);
            this.Controls.Add(this.lbAirplane);
            this.Name = "WorkersForm";
            this.Text = "Рынок труда";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WorkersForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dGVPlane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWorkers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCrew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNonCrew)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbAirplane;
        private System.Windows.Forms.DataGridView dGVPlane;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvWorkers;
        private System.Windows.Forms.DataGridViewTextBoxColumn Никнейм;
        private System.Windows.Forms.DataGridViewTextBoxColumn workColumn;
        private System.Windows.Forms.DataGridView dgvCrew;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn salaryColumn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbChangeSalary;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btChangeSalary;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbNewSalary;
        private System.Windows.Forms.Button btAddWorker;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelAirplane;
        private System.Windows.Forms.DataGridViewTextBoxColumn pilotColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stewardessColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn strikeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeAirplane;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnCurrLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn volumeAirplane;
        private System.Windows.Forms.DataGridView dgvNonCrew;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btFire;
        private System.Windows.Forms.Button btAddToCrew;
        private System.Windows.Forms.Button btOutCrew;
        private System.Windows.Forms.TextBox tbReputation;
        private System.Windows.Forms.Label label7;
    }
}