﻿namespace Airport.Forms
{
    partial class CargoSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CargoSchedule));
            this.dGVPlanes = new System.Windows.Forms.DataGridView();
            this.lbAvailablePlanes = new System.Windows.Forms.Label();
            this.dGVSelectFlight = new System.Windows.Forms.DataGridView();
            this.typeCargoFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toPlaceFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fromPlaceFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.distanceFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.volumeFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endLifeFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.incomeFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forfeitFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbSelectFlight = new System.Windows.Forms.Label();
            this.btAddInSchedule = new System.Windows.Forms.Button();
            this.dTPFlight = new System.Windows.Forms.DateTimePicker();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblOut = new System.Windows.Forms.Label();
            this.modelPlane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pilotColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stewardessColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.strikeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnCurrLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typePlane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.volumePlane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.distancePlane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costOfServicePlane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.consumptionPlane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dGVPlanes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGVSelectFlight)).BeginInit();
            this.SuspendLayout();
            // 
            // dGVPlanes
            // 
            this.dGVPlanes.AllowUserToAddRows = false;
            this.dGVPlanes.AllowUserToDeleteRows = false;
            this.dGVPlanes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dGVPlanes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVPlanes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.modelPlane,
            this.pilotColumn,
            this.stewardessColumn,
            this.strikeColumn,
            this.clmnCurrLocation,
            this.typePlane,
            this.volumePlane,
            this.distancePlane,
            this.costOfServicePlane,
            this.consumptionPlane});
            this.dGVPlanes.Location = new System.Drawing.Point(12, 152);
            this.dGVPlanes.MultiSelect = false;
            this.dGVPlanes.Name = "dGVPlanes";
            this.dGVPlanes.ReadOnly = true;
            this.dGVPlanes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dGVPlanes.Size = new System.Drawing.Size(834, 169);
            this.dGVPlanes.TabIndex = 0;
            this.dGVPlanes.SelectionChanged += new System.EventHandler(this.dGVPlanes_SelectionChanged);
            // 
            // lbAvailablePlanes
            // 
            this.lbAvailablePlanes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbAvailablePlanes.AutoSize = true;
            this.lbAvailablePlanes.Location = new System.Drawing.Point(363, 136);
            this.lbAvailablePlanes.Name = "lbAvailablePlanes";
            this.lbAvailablePlanes.Size = new System.Drawing.Size(118, 13);
            this.lbAvailablePlanes.TabIndex = 1;
            this.lbAvailablePlanes.Text = "Доступные самолёты";
            // 
            // dGVSelectFlight
            // 
            this.dGVSelectFlight.AllowUserToAddRows = false;
            this.dGVSelectFlight.AllowUserToDeleteRows = false;
            this.dGVSelectFlight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dGVSelectFlight.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVSelectFlight.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.typeCargoFlight,
            this.toPlaceFlight,
            this.fromPlaceFlight,
            this.distanceFlight,
            this.volumeFlight,
            this.endLifeFlight,
            this.incomeFlight,
            this.forfeitFlight});
            this.dGVSelectFlight.Location = new System.Drawing.Point(12, 35);
            this.dGVSelectFlight.Name = "dGVSelectFlight";
            this.dGVSelectFlight.ReadOnly = true;
            this.dGVSelectFlight.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dGVSelectFlight.Size = new System.Drawing.Size(834, 75);
            this.dGVSelectFlight.TabIndex = 2;
            // 
            // typeCargoFlight
            // 
            this.typeCargoFlight.HeaderText = "Типа рейса";
            this.typeCargoFlight.Name = "typeCargoFlight";
            this.typeCargoFlight.ReadOnly = true;
            // 
            // toPlaceFlight
            // 
            this.toPlaceFlight.HeaderText = "Место отправления";
            this.toPlaceFlight.Name = "toPlaceFlight";
            this.toPlaceFlight.ReadOnly = true;
            // 
            // fromPlaceFlight
            // 
            this.fromPlaceFlight.HeaderText = "Место назначения";
            this.fromPlaceFlight.Name = "fromPlaceFlight";
            this.fromPlaceFlight.ReadOnly = true;
            // 
            // distanceFlight
            // 
            this.distanceFlight.HeaderText = "Дальность";
            this.distanceFlight.Name = "distanceFlight";
            this.distanceFlight.ReadOnly = true;
            // 
            // volumeFlight
            // 
            this.volumeFlight.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.volumeFlight.HeaderText = "Грузоподъёмность";
            this.volumeFlight.Name = "volumeFlight";
            this.volumeFlight.ReadOnly = true;
            this.volumeFlight.Width = 129;
            // 
            // endLifeFlight
            // 
            this.endLifeFlight.HeaderText = "Конец жизни";
            this.endLifeFlight.Name = "endLifeFlight";
            this.endLifeFlight.ReadOnly = true;
            // 
            // incomeFlight
            // 
            this.incomeFlight.HeaderText = "Доходность";
            this.incomeFlight.Name = "incomeFlight";
            this.incomeFlight.ReadOnly = true;
            // 
            // forfeitFlight
            // 
            this.forfeitFlight.HeaderText = "Неустойка";
            this.forfeitFlight.Name = "forfeitFlight";
            this.forfeitFlight.ReadOnly = true;
            // 
            // lbSelectFlight
            // 
            this.lbSelectFlight.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbSelectFlight.AutoSize = true;
            this.lbSelectFlight.Location = new System.Drawing.Point(370, 19);
            this.lbSelectFlight.Name = "lbSelectFlight";
            this.lbSelectFlight.Size = new System.Drawing.Size(93, 13);
            this.lbSelectFlight.TabIndex = 3;
            this.lbSelectFlight.Text = "Выбранный рейс";
            // 
            // btAddInSchedule
            // 
            this.btAddInSchedule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btAddInSchedule.Location = new System.Drawing.Point(563, 327);
            this.btAddInSchedule.Name = "btAddInSchedule";
            this.btAddInSchedule.Size = new System.Drawing.Size(182, 36);
            this.btAddInSchedule.TabIndex = 4;
            this.btAddInSchedule.Text = "Поставить рейс в расписание";
            this.btAddInSchedule.UseVisualStyleBackColor = true;
            this.btAddInSchedule.Click += new System.EventHandler(this.btAddInSchedule_Click);
            // 
            // dTPFlight
            // 
            this.dTPFlight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dTPFlight.CustomFormat = "dd.MM.yyyy HH:mm:ss";
            this.dTPFlight.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dTPFlight.Location = new System.Drawing.Point(12, 343);
            this.dTPFlight.Name = "dTPFlight";
            this.dTPFlight.Size = new System.Drawing.Size(200, 20);
            this.dTPFlight.TabIndex = 5;
            this.dTPFlight.Value = new System.DateTime(2017, 6, 5, 16, 43, 47, 0);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(751, 327);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(95, 36);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblOut
            // 
            this.lblOut.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOut.AutoSize = true;
            this.lblOut.Location = new System.Drawing.Point(9, 327);
            this.lblOut.Name = "lblOut";
            this.lblOut.Size = new System.Drawing.Size(93, 13);
            this.lblOut.TabIndex = 12;
            this.lblOut.Text = "Время отправки:";
            // 
            // modelPlane
            // 
            this.modelPlane.HeaderText = "Модель";
            this.modelPlane.Name = "modelPlane";
            this.modelPlane.ReadOnly = true;
            // 
            // pilotColumn
            // 
            this.pilotColumn.HeaderText = "Пилоты";
            this.pilotColumn.Name = "pilotColumn";
            this.pilotColumn.ReadOnly = true;
            // 
            // stewardessColumn
            // 
            this.stewardessColumn.HeaderText = "Стюардессы";
            this.stewardessColumn.Name = "stewardessColumn";
            this.stewardessColumn.ReadOnly = true;
            // 
            // strikeColumn
            // 
            this.strikeColumn.HeaderText = "Забастовка";
            this.strikeColumn.Name = "strikeColumn";
            this.strikeColumn.ReadOnly = true;
            // 
            // clmnCurrLocation
            // 
            this.clmnCurrLocation.HeaderText = "Местоположение";
            this.clmnCurrLocation.Name = "clmnCurrLocation";
            this.clmnCurrLocation.ReadOnly = true;
            // 
            // typePlane
            // 
            this.typePlane.HeaderText = "Тип самолёта";
            this.typePlane.Name = "typePlane";
            this.typePlane.ReadOnly = true;
            // 
            // volumePlane
            // 
            this.volumePlane.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.volumePlane.HeaderText = "Грузоподъёмность / вместимость";
            this.volumePlane.Name = "volumePlane";
            this.volumePlane.ReadOnly = true;
            this.volumePlane.Width = 189;
            // 
            // distancePlane
            // 
            this.distancePlane.HeaderText = "Дальность";
            this.distancePlane.Name = "distancePlane";
            this.distancePlane.ReadOnly = true;
            // 
            // costOfServicePlane
            // 
            this.costOfServicePlane.HeaderText = "Стоимость обслуживания";
            this.costOfServicePlane.Name = "costOfServicePlane";
            this.costOfServicePlane.ReadOnly = true;
            // 
            // consumptionPlane
            // 
            this.consumptionPlane.HeaderText = "Расход";
            this.consumptionPlane.Name = "consumptionPlane";
            this.consumptionPlane.ReadOnly = true;
            // 
            // CargoSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(856, 375);
            this.Controls.Add(this.lblOut);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.dTPFlight);
            this.Controls.Add(this.btAddInSchedule);
            this.Controls.Add(this.lbSelectFlight);
            this.Controls.Add(this.dGVSelectFlight);
            this.Controls.Add(this.lbAvailablePlanes);
            this.Controls.Add(this.dGVPlanes);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CargoSchedule";
            this.Text = "Поставить грузовой рейс в расписание";
            this.Resize += new System.EventHandler(this.CargoSchedule_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dGVPlanes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGVSelectFlight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dGVPlanes;
        private System.Windows.Forms.Label lbAvailablePlanes;
        private System.Windows.Forms.DataGridView dGVSelectFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeCargoFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn toPlaceFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn fromPlaceFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn distanceFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn volumeFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn endLifeFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn incomeFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn forfeitFlight;
        private System.Windows.Forms.Label lbSelectFlight;
        private System.Windows.Forms.Button btAddInSchedule;
        private System.Windows.Forms.DateTimePicker dTPFlight;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblOut;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelPlane;
        private System.Windows.Forms.DataGridViewTextBoxColumn pilotColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stewardessColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn strikeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnCurrLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn typePlane;
        private System.Windows.Forms.DataGridViewTextBoxColumn volumePlane;
        private System.Windows.Forms.DataGridViewTextBoxColumn distancePlane;
        private System.Windows.Forms.DataGridViewTextBoxColumn costOfServicePlane;
        private System.Windows.Forms.DataGridViewTextBoxColumn consumptionPlane;
    }
}