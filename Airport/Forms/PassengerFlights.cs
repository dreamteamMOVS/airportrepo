﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Airport
{
    public partial class PassengerFlights : Form
    {
        private List<PassengerFlight> _generated;

        public List<PassengerFlight> Generated
        {
            get => _generated;
            set
            {
                _generated = value;
                UpdateDGV();
            }
        }

        public DialogResult State { get; set; }

        public PassengerFlights()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            foreach (DataGridViewColumn col in dgvAllPassFlights.Columns)
            {
                col.ReadOnly = true;
            }
            dgvAllPassFlights.Columns[0].ReadOnly = false;
            dgvAllPassFlights.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            _generated = new List<PassengerFlight>();
        }

        /// <summary>
        /// Создает рандомное количество пассажирских рейсов.
        /// </summary>
        /// <param name="rnd">Генератор псевдослучайных чисел.</param>
        public void GenerateNew(Random rnd)
        {
            int num = rnd.Next(20, 30);
            for (int i = _generated.Count - 1; i >= 0; i--)
            {
                if (_generated[i].DateEnd <= TimeManager.Time || rnd.NextDouble() > 0.5)
                {
                    _generated.RemoveAt(i);
                }
            }
            for (int i = _generated.Count; i < num; i++)
            {
                _generated.Add(new PassengerFlight(TimeManager.Time, rnd.Next(0, 10000)));
            }
            UpdateDGV();
        }

        /// <summary>
        /// Очищение таблицы и заполнение её из списка сгенерированных рейсов.
        /// </summary>
        private void UpdateDGV()
        {
            dgvAllPassFlights.Rows.Clear();
            foreach (PassengerFlight c in _generated)
            {
                dgvAllPassFlights.Rows.Add(false, c.Type == FlightType.Cargo ? "Грузовой" : "Пассажирский", c.CityFrom.Name,
                    c.CityTo.Name, c.Regularity.Time, c.Distance, c.Price,
                    c.Peak.Time, c.DateEnd.ToShortDateString(), c.Forfeit);
                dgvAllPassFlights.Rows[dgvAllPassFlights.Rows.Count - 1].Tag = c;
            }
            if (dgvAllPassFlights.Rows.Count > 0)
            {
                dgvAllPassFlights.Rows[0].Selected = false;
            }
            dgvAllPassFlights.Refresh();
        }

        public List<PassengerFlight> GetSelected()
        {
            List<PassengerFlight> result = new List<PassengerFlight>();
            foreach (DataGridViewRow row in dgvAllPassFlights.Rows)
            {
                if ((bool)((DataGridViewCheckBoxCell)row.Cells[0]).Value)
                {
                    result.Add((PassengerFlight)row.Tag);
                    _generated.Remove((PassengerFlight)row.Tag);
                }
            }
            UpdateDGV();
            return result;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            State = DialogResult.OK;
            Hide();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            State = DialogResult.Cancel;
            Hide();
        }

        /// <summary>
        /// Нажатие DEL при одной/нескольких выделенных строках таблицы.
        /// </summary>
        private void dgvAllPassFlights_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                foreach (DataGridViewRow row in dgvAllPassFlights.SelectedRows)
                {
                    _generated.Remove((PassengerFlight)row.Tag);
                }
                UpdateDGV();
            }
            e.Handled = true;
        }

        private void dgvAllPassFlights_SelectionChanged(object sender, EventArgs e)
        {
            btnAdd.Enabled = dgvAllPassFlights.SelectedRows.Count > 0;
        }

        /// <summary>
        /// Просто снятие выделения со всех строк таблицы.
        /// </summary>
        private void PassengerFlights_Shown(object sender, EventArgs e)
        {
            UpdateDGV();
        }

        private void PassengerFlights_Resize(object sender, EventArgs e)
        {
            dgvAllPassFlights.AutoResizeColumns();

            if (WindowState == FormWindowState.Minimized)
            {
                TimeManager.Suspend();
            }
            else
            {
                TimeManager.Resume();
            }
        }
    }
}