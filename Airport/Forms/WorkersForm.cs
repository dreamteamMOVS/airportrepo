﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Airport.Forms
{
    public partial class WorkersForm : Form
    {
        private Airport _airport;
        private List<Worker> _workers;
        private int _maxWorkers = 50;
        private Random _rnd = new Random();
        private List<Worker> _unoccupiedWorkers;
        public List<Worker> Workers
        {
            get { return _workers; }
            set
            {
                _workers = value;
                if (_workers!=null) UpdateDGVWorkers();
            }
        }

        public List<Worker> UnoccupiedWorkers
        {
            get => _unoccupiedWorkers;
        }
        public WorkersForm(Airport airport)
        {
            InitializeComponent();
            btChangeSalary.Enabled = false;
            btAddWorker.Enabled = false;
            dgvWorkers.AutoSizeColumnsMode= DataGridViewAutoSizeColumnsMode.Fill;
            LoadCrew();
            _airport = airport;
            _unoccupiedWorkers=new List<Worker>();
            UpdateReputation();
        }

        

        public void GenerateNew()
        {
            _workers=new List<Worker>();
            for (int i = 0; i < _maxWorkers; i++)
            {
                Worker worker=new Worker();
                worker.Nickname = GetHash();
                int postNumber=_rnd.Next(0, 4);
                if (postNumber > 2)
                {
                    worker.WorkType = Post.Pilot;
                    worker.MinSalary = _rnd.Next(3000, 6000);
                }
                else
                {
                    worker.WorkType = Post.Stewardess;
                    worker.MinSalary = _rnd.Next(1000, 4000);
                }
                
                _workers.Add(worker);
            }
            UpdateDGVWorkers();
        }
        /// <summary>
        /// Генерирует рандомный ник (не очень надежно)
        /// </summary>
        public string GetHash()
        {
            int nickNumber=_rnd.Next(0, 100000);
            string nickCharset = Convert.ToChar(65 + _rnd.Next(0, 26)).ToString() +
                                 Convert.ToChar(65 + _rnd.Next(0, 26)).ToString();
            return nickNumber.ToString() + nickCharset;
        }

        private void tbNewSalary_TextChanged(object sender, EventArgs e)
        {
            if (CheckTextBox(tbNewSalary.Text))
                btAddWorker.Enabled = true;
            else
                btAddWorker.Enabled = false;
        }

        private void tbChangeSalary_TextChanged(object sender, EventArgs e)
        {
            if (CheckTextBox(tbChangeSalary.Text))
                btChangeSalary.Enabled = true;
            else
                btChangeSalary.Enabled = false;
        }

        private bool CheckTextBox(string text)
        {
            if (!int.TryParse(text, out int _) || text.Contains("-") || text=="0")
                return false;
            return true;
        }

        private void UpdateDGVWorkers()
        {
            dgvWorkers.Rows.Clear();
            foreach (Worker worker in _workers)
            {
                dgvWorkers.Rows.Add(worker.Nickname, worker.WorkType == Post.Pilot ? "Пилот" : "Стюардесса");
                dgvWorkers.Rows[dgvWorkers.RowCount - 1].Tag = worker;
            }
            if (dgvWorkers.RowCount > 0)
                dgvWorkers.Rows[0].Selected = false;
            dgvWorkers.Refresh();
        }

        public void LoadPlanes(List<Plane> planes)
        {
            dGVPlane.Rows.Clear();
            foreach (Plane plane in planes)
            {
                int rowIndex = dGVPlane.Rows.Add();
                DataGridViewRow row = dGVPlane.Rows[rowIndex];

                row.Cells["modelAirplane"].Value = plane.Model;
                row.Cells["typeAirplane"].Value = plane.TypeName();
                row.Cells["clmnCurrLocation"].Value = plane.CurrentLocation.Name;
                row.Cells["volumeAirplane"].Value = plane is PlaneCargo ?
                    ((PlaneCargo)plane).CarryingCapacity :
                    ((PlanePassenger)plane).Spaciousness;
                row.Cells["pilotColumn"].Value = plane.RequirePilots;
                row.Cells["stewardessColumn"].Value = plane.RequireStewardessess;
                row.Cells["strikeColumn"].Value = plane.IsStrike ? "Да" : "Нет";
                row.Tag = plane;
            }
            dGVPlane.Refresh();
        }

        public void OutCrew(Plane plane, Worker worker)
        {
            plane.DeleteWorker(worker);

            dgvNonCrew.Rows.Add(
                worker.Nickname,
                worker.WorkType == Post.Pilot ? "Пилот" : "Стюардесса",
                worker.Salary);
            dgvNonCrew.Rows[dgvNonCrew.RowCount - 1].Tag = worker;

            plane.UpdateOldCrewAverageSalary();
            _unoccupiedWorkers.Add(worker);
            dgvNonCrew.Refresh();
            LoadCrew();
        }

        public void OutAllWorkers(Plane plane)
        {
            while (plane.Pilots != null && !(plane.Pilots.FirstOrDefault() is null))
            {
                Worker pilot = plane.Pilots.FirstOrDefault();
                OutCrew(plane, pilot);
            }
            while (plane.Stewardesses != null && !(plane.Stewardesses.FirstOrDefault() is null))
            {
                Worker steward = plane.Stewardesses.FirstOrDefault();
                OutCrew(plane, steward);
            }
        }
        private void FireWorker()
        {
            if (dgvNonCrew.SelectedRows.Count == 0)
            {
                MessageBox.Show("Не выбран сотрудник!");
                return;
            }

            Worker worker = (Worker)dgvWorkers.SelectedRows[0].Tag;
            _unoccupiedWorkers.Remove(worker);
            dgvNonCrew.Rows.RemoveAt(dgvNonCrew.SelectedRows[0].Index);
        }

        private bool AddWorker(Worker worker)
        {
            Plane plane = (Plane)dGVPlane.SelectedRows[0].Tag;
            try
            {
                plane.AddWorker(worker);
                plane.UpdateOldCrewAverageSalary();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            LoadCrew();
            return true;
        }

        private void btAddWorker_Click(object sender, EventArgs e)
        {
            if (dgvWorkers.SelectedRows.Count == 0 || dGVPlane.SelectedRows.Count==0)
            {
                MessageBox.Show("Не выбран самолет или работник!");
                return;
            }

            Worker worker = (Worker)dgvWorkers.SelectedRows[0].Tag;
            try
            {
                worker.ChangeSalary(Convert.ToInt32(tbNewSalary.Text));
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            if (AddWorker(worker))
            {
                dgvWorkers.Rows.Remove(dgvWorkers.CurrentRow);
            }
        }

        private void LoadCrew()
        {
            dgvCrew.Rows.Clear();
            if (dGVPlane.SelectedRows.Count==0)
                return;
            Plane plane = (Plane) dGVPlane.SelectedRows[0].Tag;
            foreach (Worker worker in plane.Pilots)
            {
                dgvCrew.Rows.Add(
                    worker.Nickname,
                    "Пилот",
                    worker.Salary
                    );
                dgvCrew.Rows[dgvCrew.RowCount - 1].Tag = worker;
            }
            foreach (Worker worker in plane.Stewardesses)
            {
                dgvCrew.Rows.Add(
                    worker.Nickname,
                    "Стюардесса",
                    worker.Salary
                );
                dgvCrew.Rows[dgvCrew.RowCount - 1].Tag = worker;
            }
            dgvCrew.Refresh();
        }

        private void dGVPlane_SelectionChanged(object sender, EventArgs e)
        {
            LoadCrew();
        }

        private void ChangeSalary()
        {
            if (dgvCrew.SelectedRows.Count == 0)
                return;
            Worker worker = (Worker)dgvCrew.SelectedRows[0].Tag;
            try
            {
                worker.ChangeSalary(Convert.ToInt32(tbChangeSalary.Text));
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            DataGridViewRow row = dGVPlane.SelectedRows[0];
            Plane plane = (Plane)row.Tag;
            double change = plane.CrewAverageSalary / plane.OldCrewAverageSalary;
            double xp = change - 1;
            xp *= 10;
            if (change <= 0.85 && !plane.IsStrike)
            {
                plane.IsStrike = true;
                UpdateDGVWorkers();
                row.Cells["strikeColumn"].Value = "Да";
                dGVPlane.Refresh();
                MessageBox.Show($"Экипаж самолета {plane.Model} объявил забастовку из-за слишком низкой зарплаты! ТРЕБУЮТ ПОВЫСИТЬ ЗАРПЛАТУ");
                _airport.FillPlanes();
                ChangeReputation(Convert.ToInt32(Math.Truncate(xp)));
            }
            else if (plane.IsStrike && change>0.85)
            {
                plane.IsStrike = false;
                UpdateDGVWorkers();
                row.Cells["strikeColumn"].Value = "Нет";
                dGVPlane.Refresh();
                MessageBox.Show($"Экипаж самолета {plane.Model} прекратил забастовку!");
                _airport.FillPlanes();
            }
            else if (change > 1)
            {
                plane.UpdateOldCrewAverageSalary();
                ChangeReputation(Convert.ToInt32(Math.Truncate(xp)));
            }
            LoadCrew();
        }

        private void btChangeSalary_Click(object sender, EventArgs e)
        {
            ChangeSalary();
        }

        private void btOutCrew_Click(object sender, EventArgs e)
        {
            if (dGVPlane.SelectedRows.Count == 0 || dgvCrew.SelectedRows.Count == 0)
            {
                MessageBox.Show("Не выбран самолет или работник!");
                return;
            }
            Plane plane = (Plane)dGVPlane.SelectedRows[0].Tag;
            Worker worker = (Worker)dgvCrew.SelectedRows[0].Tag;

            OutCrew(plane, worker);
        }

        private void btFire_Click(object sender, EventArgs e)
        {
            FireWorker();
        }

        private void btAddToCrew_Click(object sender, EventArgs e)
        {
            if (dgvNonCrew.SelectedRows.Count == 0 || dGVPlane.SelectedRows.Count == 0)
            {
                MessageBox.Show("Не выбран самолет или работник!");
                return;
            }

            Worker worker = (Worker)dgvNonCrew.SelectedRows[0].Tag;
            if (AddWorker(worker))
                dgvNonCrew.Rows.RemoveAt(dgvNonCrew.SelectedRows[0].Index);
        }

        public void ChangeReputation(int change)
        {
            _airport.Reputation = _airport.Reputation+change;
            if (change>0)
                MessageBox.Show($"Репутация повысилась на {change} очков!");
            else
            {
                if (change<0)
                    MessageBox.Show($"Репутация понизилась на {-change} очков!");
            }
            UpdateReputation();
        }

        public void UpdateReputation()
        {
            tbReputation.Text = _airport.Reputation.ToString();
        }

        private void WorkersForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            tbNewSalary.Text = "";
            tbChangeSalary.Text = "";
        }
    }
}
