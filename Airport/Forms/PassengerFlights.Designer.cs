﻿namespace Airport
{
    partial class PassengerFlights
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PassengerFlights));
            this.lbInfo = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.dgvAllPassFlights = new System.Windows.Forms.DataGridView();
            this.clmnChecked = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clmnTypeFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnPlaceFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnPlaceTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnReg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnDist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnPeak = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnDeadline = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnForfeit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllPassFlights)).BeginInit();
            this.SuspendLayout();
            // 
            // lbInfo
            // 
            this.lbInfo.AutoSize = true;
            this.lbInfo.Location = new System.Drawing.Point(9, 9);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(257, 13);
            this.lbInfo.TabIndex = 17;
            this.lbInfo.Text = "* Отметьте галочкой те, которые хотите выбрать";
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(900, 306);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 15;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(981, 306);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // dgvAllPassFlights
            // 
            this.dgvAllPassFlights.AllowUserToAddRows = false;
            this.dgvAllPassFlights.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAllPassFlights.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAllPassFlights.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmnChecked,
            this.clmnTypeFlight,
            this.clmnPlaceFrom,
            this.clmnPlaceTo,
            this.clmnReg,
            this.clmnDist,
            this.clmnPrice,
            this.clmnPeak,
            this.clmnDeadline,
            this.clmnForfeit});
            this.dgvAllPassFlights.Location = new System.Drawing.Point(12, 25);
            this.dgvAllPassFlights.Name = "dgvAllPassFlights";
            this.dgvAllPassFlights.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAllPassFlights.Size = new System.Drawing.Size(1044, 270);
            this.dgvAllPassFlights.TabIndex = 18;
            this.dgvAllPassFlights.SelectionChanged += new System.EventHandler(this.dgvAllPassFlights_SelectionChanged);
            this.dgvAllPassFlights.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvAllPassFlights_KeyDown);
            // 
            // clmnChecked
            // 
            this.clmnChecked.HeaderText = "";
            this.clmnChecked.Name = "clmnChecked";
            this.clmnChecked.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clmnChecked.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // clmnTypeFlight
            // 
            this.clmnTypeFlight.HeaderText = "Тип рейса";
            this.clmnTypeFlight.Name = "clmnTypeFlight";
            // 
            // clmnPlaceFrom
            // 
            this.clmnPlaceFrom.HeaderText = "Место отправления";
            this.clmnPlaceFrom.Name = "clmnPlaceFrom";
            // 
            // clmnPlaceTo
            // 
            this.clmnPlaceTo.HeaderText = "Место назначения";
            this.clmnPlaceTo.Name = "clmnPlaceTo";
            // 
            // clmnReg
            // 
            this.clmnReg.HeaderText = "Регулярность";
            this.clmnReg.Name = "clmnReg";
            // 
            // clmnDist
            // 
            this.clmnDist.HeaderText = "Дальность";
            this.clmnDist.Name = "clmnDist";
            // 
            // clmnPrice
            // 
            this.clmnPrice.HeaderText = "Стоимость билета";
            this.clmnPrice.Name = "clmnPrice";
            // 
            // clmnPeak
            // 
            this.clmnPeak.HeaderText = "Пиковые часы";
            this.clmnPeak.Name = "clmnPeak";
            // 
            // clmnDeadline
            // 
            this.clmnDeadline.HeaderText = "Конец жизни";
            this.clmnDeadline.Name = "clmnDeadline";
            // 
            // clmnForfeit
            // 
            this.clmnForfeit.HeaderText = "Неустойка";
            this.clmnForfeit.Name = "clmnForfeit";
            // 
            // PassengerFlights
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(1064, 335);
            this.Controls.Add(this.dgvAllPassFlights);
            this.Controls.Add(this.lbInfo);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnCancel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PassengerFlights";
            this.Text = "Доступные пассажирские рейсы";
            this.Shown += new System.EventHandler(this.PassengerFlights_Shown);
            this.Resize += new System.EventHandler(this.PassengerFlights_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllPassFlights)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbInfo;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridView dgvAllPassFlights;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmnChecked;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnTypeFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnPlaceFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnPlaceTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnReg;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnDist;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnPeak;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnDeadline;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnForfeit;
    }
}