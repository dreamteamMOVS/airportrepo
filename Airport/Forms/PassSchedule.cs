﻿using System;
using System.Windows.Forms;
using Airport.Utils;

namespace Airport.Forms
{
    public partial class PassSchedule : Form
    {
        private readonly Airport _airport;
        private readonly PassengerFlight _selectedFlight;
        private int _passCount;

        public Plane Plane { get; set; }

        public PassSchedule(PassengerFlight selectedFlight, Airport airport)
        {
            InitializeComponent();
            _airport = airport;
            _selectedFlight = selectedFlight;
            btnCancel.DialogResult = DialogResult.Cancel;
            dGVSelectFlight.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dGVPlanes.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            StartPosition = FormStartPosition.CenterScreen;

            dTPFlight.MinDate = TimeManager.Time;
            dTPFlight.Value = TimeManager.Time;
            dTPFlight.MaxDate = selectedFlight.DateEnd;

            dGVSelectFlight.Rows.Add(selectedFlight.Type == FlightType.Cargo ? "Грузовой" : "Пассажирский", selectedFlight.CityFrom.Name,
                selectedFlight.CityTo.Name, selectedFlight.Regularity.Time, selectedFlight.Distance,
                selectedFlight.Peak.Time, selectedFlight.DateEnd.ToShortDateString(), selectedFlight.Forfeit);
            dGVSelectFlight.Rows[0].Tag = selectedFlight;

            FillPlanesDGV();
            btAddInSchedule.Enabled = dGVPlanes.SelectedRows.Count > 0;
        }

        private void FillPlanesDGV()
        {
            foreach (Plane p in _airport.Planes)
            {
                int ind = dGVPlanes.Rows.Add(
                    p.Model,
                    p.RequirePilots,
                    p.RequireStewardessess,
                    p.IsStrike ? "Да" : "Нет",
                    p.CurrentLocation.Name,
                    p.TypeName(),
                    p is PlaneCargo ?((PlaneCargo)p).CarryingCapacity : ((PlanePassenger)p).Spaciousness,
                    p.MaxRange,
                    p.ServicePrice,
                    p.FuelConsumption);
                dGVPlanes.Rows[ind].Tag = p;
            }
        }

        private void GenPassCount()
        {
            DateTime time = dTPFlight.Value;
            Random rnd = new Random();
            var koeff = time.Hour / ((_selectedFlight.Peak.End + _selectedFlight.Peak.Begin) / 2.0);
            koeff = koeff > 1 ? Math.Abs(2 - koeff) : koeff;
            _passCount = (int)(rnd.Next(90, 110) * koeff * _airport.Reputation / 100.0);
        }

        private void dGVPlanes_SelectionChanged(object sender, EventArgs e)
        {
            btAddInSchedule.Enabled = dGVPlanes.SelectedRows.Count > 0;
            Plane = (Plane)dGVPlanes.SelectedRows[0].Tag;
        }

        private void btAddInSchedule_Click(object sender, EventArgs e)
        {
            Plane = (Plane)dGVPlanes.SelectedRows[0].Tag;
            if (Plane is PlaneCargo)
            {
                MessageBox.Show("Неверный тип самолета");
            }
            if (Plane.MaxRange < _selectedFlight.Distance)
            {
                MessageBox.Show("Максимальная дальность полета выбранного самолета не подходит");
            }
            else if (dTPFlight.Value < _selectedFlight.BackTime)
            {
                MessageBox.Show("Невозможно поставить рейс на выбранное время");
            }
            else if (Plane != _selectedFlight.PlaneForRegularFlights && _selectedFlight.PlaneForRegularFlights != null)
            {
                MessageBox.Show("На обратный рейс можно поставить только тот же самолет, что и на прямой");
            }
            else if (Plane.IsStrike)
            {
                MessageBox.Show("Экипаж объявил забастовку!");
            }
            else if (Plane.RequirePilots > Plane.Pilots.Count ||
                     Plane.RequireStewardessess > Plane.Stewardesses.Count)
            {
                MessageBox.Show("Недостаточно экипажа!");
			}
            else if (Plane.CurrentLocation.Name != _selectedFlight.CityFrom.Name)
            {
                // Типа полчаса еще на всякий случай после перегонки
                if (dTPFlight.Value > TimeManager.Time.AddHours(Info.GetDist(Plane.CurrentLocation, _selectedFlight.CityFrom) / Plane.Speed + 0.5))
                {
                    if (_passCount > ((PlanePassenger)Plane).Spaciousness)
                    {
                        ((PlanePassenger)Plane).Spaciousness = _passCount;
                    }
                    _selectedFlight.Income = (int)(_selectedFlight.Price * _passCount - Plane.FuelConsumption * _selectedFlight.Distance * Info.FulePrice - Plane.ServicePrice);
                    _selectedFlight.TimeFrom = dTPFlight.Value;
                    _selectedFlight.TimeTo = _selectedFlight.TimeFrom.AddHours(_selectedFlight.Distance / Plane.Speed);
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Самолет не успеет к назначенному времени");
                }
            }
            else
            {
                if (_passCount > ((PlanePassenger)Plane).Spaciousness)
                {
                    ((PlanePassenger)Plane).Spaciousness = _passCount;
                }
                _selectedFlight.Income = (int)(_selectedFlight.Price * _airport.Reputation / 100.0 * _passCount - Plane.FuelConsumption * _selectedFlight.Distance * Info.FulePrice - Plane.ServicePrice);
                _selectedFlight.TimeFrom = dTPFlight.Value;
                _selectedFlight.TimeTo = _selectedFlight.TimeFrom.AddHours(_selectedFlight.Distance / Plane.Speed);
                DialogResult = DialogResult.OK;
            }
        }

        private void dTPFlight_ValueChanged(object sender, EventArgs e)
        {
            GenPassCount();
        }

        private void PassSchedule_Resize(object sender, EventArgs e)
        {
            dGVSelectFlight.AutoResizeColumns();
            dGVPlanes.AutoResizeColumns();

            if (WindowState == FormWindowState.Minimized)
            {
                TimeManager.Suspend();
            }
            else
            {
                TimeManager.Resume();
            }
        }
    }
}