﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using static Airport.Utils.Info;

namespace Airport
{
    public partial class BuyOrRent : Form
    {
        private bool _bIsCreatingInProgress;
        private readonly Airport _airportForm;
        private Random _rnd;

        private List<Plane> _hangarPlanes;
        private readonly List<Plane> _allPlanes;

        private int _balance;

        public int Balance
        {
            get => _balance;
            set
            {
                _balance = value;
                tbBalance.Text = _balance.ToString();
                _airportForm.Balance = _balance;
                UpdateOkBtnEnabled();
            }
        }

        public BuyOrRent(Airport airport)
        {
            InitializeComponent();

            StartPosition = FormStartPosition.CenterScreen;

            _airportForm = airport;
            _allPlanes = airport.Planes;
            Balance = airport.Balance;
            _rnd = new Random();

            dGVBuy.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dGVLeasing.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dGVPurchased.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dGVRented.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            comboBox1.SelectedIndex = 0;
            CreateHangarPlanes();
        }
        
        void UpdateAirportInfo()
        {
            if (!_bIsCreatingInProgress)
            {
                _airportForm.Planes = _allPlanes;
                _airportForm.Balance = _balance;
            }
        }

        void CreateHangarPlanes()
        {
            _hangarPlanes = new List<Plane>();
            
            PlaneCargo plane1 = new PlaneCargo();
            plane1.SetTu165();
            _hangarPlanes.Add(plane1);

            PlanePassenger plane2 = new PlanePassenger();
            plane2.SetVzih511();
            _hangarPlanes.Add(plane2);

            PlanePassenger plane3 = new PlanePassenger();
            plane3.SetEtoTozheSamolet25();
            _hangarPlanes.Add(plane3);

            PlaneCargo plane4 = new PlaneCargo();
            plane4.SetVzih621();
            _hangarPlanes.Add(plane4);

            PlanePassenger plane5 = new PlanePassenger();
            plane5.SetOchenDorogoySamolet319();
            _hangarPlanes.Add(plane5);

            PlanePassenger plane6 = new PlanePassenger();
            plane6.SetVzih512();
            _hangarPlanes.Add(plane6);

            PlanePassenger plane7 = new PlanePassenger();
            plane7.SetVzih513();
            _hangarPlanes.Add(plane7);

            FillHangarPlanes();
            FillPlayersPlanes();
        }

        private void FillHangarPlanes()
        {
            dGVBuy.Rows.Clear();

            foreach (Plane plane in _hangarPlanes)
            {
                int rowIndex = dGVBuy.Rows.Add();
                DataGridViewRow row = dGVBuy.Rows[rowIndex];

                row.Cells["modelColumn"].Value = plane.Model;
                row.Cells["costColumn"].Value = plane.PlanePrice;
                row.Cells["monthPriceColumn"].Value = plane.LeasePrice;
                row.Cells["typeColumn"].Value = plane.TypeName();
                row.Cells["volumeColumn"].Value = plane is PlaneCargo ?
                    ((PlaneCargo)plane).CarryingCapacity :
                    ((PlanePassenger)plane).Spaciousness;
                row.Cells["clmnSpeed"].Value = plane.Speed;
                row.Cells["distanceColumn"].Value = plane.MaxRange;
                row.Cells["costOfServiceColumn"].Value = plane.ServicePrice;
                row.Cells["consumptionColumn"].Value = plane.FuelConsumption;
                row.Cells["pilotColumn"].Value = plane.RequirePilots;
                row.Cells["stewardessColumn"].Value = plane.RequireStewardessess;
            }
        }

        private void AddPlaneToPlayersPlanes(Plane plane)
        {
            int rowIndex;
            DataGridViewRow row;

            switch (plane.Status)
            {
                case TradeStatus.Purchased:
                    rowIndex = dGVPurchased.Rows.Add();
                    row = dGVPurchased.Rows[rowIndex];
                    row.Cells["idPColumn"].Value = plane.Id;
                    row.Cells["modelPColumn"].Value = plane.Model;
                    row.Cells["sellPColumn"].Value = plane.PlanePrice;
                    row.Cells["typePColumn"].Value = plane.TypeName();
                    row.Cells["volumePColumn"].Value = plane is PlaneCargo ?
                        ((PlaneCargo)plane).CarryingCapacity :
                        ((PlanePassenger)plane).Spaciousness;
                    row.Cells["clmnPSpeed"].Value = plane.Speed;
                    row.Cells["distancePColumn"].Value = plane.MaxRange;
                    row.Cells["costOfServicePColumn"].Value = plane.ServicePrice;
                    row.Cells["consumptionPColumn"].Value = plane.FuelConsumption;
                    row.Cells["pilotPColumn"].Value = plane.RequirePilots;
                    row.Cells["stewardessPColumn"].Value = plane.RequireStewardessess;
                    break;

                case TradeStatus.Rented:
                    rowIndex = dGVRented.Rows.Add();
                    row = dGVRented.Rows[rowIndex];
                    row.Cells["idRColumn"].Value = plane.Id;
                    row.Cells["modelRColumn"].Value = plane.Model;
                    row.Cells["monthPriceRColumn"].Value = plane.LeasePrice;
                    row.Cells["nextPaymentRColumn"].Value = plane.NextPayment.ToShortDateString();
                    row.Cells["typeRColumn"].Value = plane.TypeName();
                    row.Cells["volumeRColumn"].Value = plane is PlaneCargo ?
                        ((PlaneCargo)plane).CarryingCapacity :
                        ((PlanePassenger)plane).Spaciousness;
                    row.Cells["clmnRSpeed"].Value = plane.Speed;
                    row.Cells["distanceRColumn"].Value = plane.MaxRange;
                    row.Cells["costOfServiceRColumn"].Value = plane.ServicePrice;
                    row.Cells["consumptionRColumn"].Value = plane.FuelConsumption;
                    row.Cells["pilotRColumn"].Value = plane.RequirePilots;
                    row.Cells["stewardessRColumn"].Value = plane.RequireStewardessess;
                    break;

                case TradeStatus.Leasing:
                    rowIndex = dGVLeasing.Rows.Add();
                    row = dGVLeasing.Rows[rowIndex];
                    row.Cells["idLColumn"].Value = plane.Id;
                    row.Cells["modelLColumn"].Value = plane.Model;
                    row.Cells["buyCostLColumn"].Value = plane.LeasingBuyCost;
                    row.Cells["monthPriceLColumn"].Value = plane.LeasePrice;
                    row.Cells["nextPaymentLColumn"].Value = plane.NextPayment.ToShortDateString();
                    row.Cells["typeLColumn"].Value = plane.TypeName();
                    row.Cells["volumeLColumn"].Value = plane is PlaneCargo ?
                        ((PlaneCargo)plane).CarryingCapacity :
                        ((PlanePassenger)plane).Spaciousness;
                    row.Cells["clmnLSpeed"].Value = plane.Speed;
                    row.Cells["distanceLColumn"].Value = plane.MaxRange;
                    row.Cells["costOfServiceLColumn"].Value = plane.ServicePrice;
                    row.Cells["consumptionLColumn"].Value = plane.FuelConsumption;
                    row.Cells["pilotLColumn"].Value = plane.RequirePilots;
                    row.Cells["stewardessLColumn"].Value = plane.RequireStewardessess;
                    break;
            }
        }

        private void FillPlayersPlanes()
        {
            _bIsCreatingInProgress = true;
            dGVPurchased.Rows.Clear();
            dGVRented.Rows.Clear();
            dGVLeasing.Rows.Clear();

            foreach (Plane plane in _allPlanes)
                AddPlaneToPlayersPlanes(plane);

            _bIsCreatingInProgress = false;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = dGVBuy.CurrentRow;
            if (row == null)
                return;

            _balance -= Convert.ToInt32(tbPrice.Text);

            Plane plane;
            bool isCargo = row.Cells["typeColumn"].Value.ToString() == "Грузовой";
            if (isCargo)
                plane = new PlaneCargo()
                {
                    CarryingCapacity = Convert.ToInt32(row.Cells["volumeColumn"].Value)
                };
            else
                plane = new PlanePassenger()
                {
                    Spaciousness = Convert.ToInt32(row.Cells["volumeColumn"].Value)
                };

            plane.Id = IdAble.GetNewId;
            plane.Model = row.Cells["modelColumn"].Value.ToString();
            plane.Created = TimeManager.Time - TimeSpan.FromDays(20);
            plane.PlanePrice = Convert.ToInt32(row.Cells["costColumn"].Value);
            plane.NewPlanePrice = plane.PlanePrice;
            plane.LeasePrice = Convert.ToInt32(row.Cells["monthPriceColumn"].Value);
            plane.MaxRange = Convert.ToInt32(row.Cells["distanceColumn"].Value);
            plane.Speed = Convert.ToInt32(row.Cells["clmnSpeed"].Value);
            plane.ServicePrice = Convert.ToInt32(row.Cells["costOfServiceColumn"].Value);
            plane.NewServicePrice = plane.ServicePrice;
            plane.FuelConsumption = Convert.ToInt32(row.Cells["consumptionColumn"].Value);
            plane.CurrentLocation = Flight.Cities[_rnd.Next(0, Flight.Cities.Length - 1)];
            plane.RequirePilots = Convert.ToInt32(row.Cells["pilotColumn"].Value);
            plane.RequireStewardessess = Convert.ToInt32(row.Cells["stewardessColumn"].Value);

            switch (comboBox1.SelectedItem.ToString())
            {
                case "Покупка":
                    plane.Status = TradeStatus.Purchased;
                    playerPlanesTabControl.SelectedTab = purchasedTab;
                    break;

                case "Аренда":
                    plane.Status = TradeStatus.Rented;
                    plane.NextPayment = TimeManager.Time.AddDays(Airport.DaysDifferBetweenPayments);
                    playerPlanesTabControl.SelectedTab = rentedTab;
                    break;

                case "Лизинг":
                    plane.Status = TradeStatus.Leasing;
                    plane.NextPayment = TimeManager.Time.AddDays(Airport.DaysDifferBetweenPayments);
                    plane.LeasingBuyCost = plane.PlanePrice - plane.LeasePrice;
                    playerPlanesTabControl.SelectedTab = leasingTab;
                    break;
            }
        
            switch (plane.Model)
            {
                case "Ту-165":
                    Plane.Tu165Count++;
                    break;
                case "Вжих-621":
                    Plane.Vzih621Count++;
                    break;
                case "Вжих-511":
                    Plane.Vzih511Count++;
                    break;
                case "Это тоже самолёт-25":
                    Plane.EtoTozheSamolet25Count++;
                    break;
                case "Очень дорогой самолёт-319":
                    Plane.OchenDorogoySamolet319Count++;
                    break;
                case "Вжих-512":
                    Plane.Vzih512Count++;
                    break;
                case "Вжих-513":
                    Plane.Vzih513Count++;
                    break;
            }

            _allPlanes.Add(plane);
            AddPlaneToPlayersPlanes(plane);
        }

        private void sellPurshaseBtn_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = dGVPurchased.CurrentRow;
            Plane currentPlane = null;

            foreach (Plane plane in _allPlanes)
            {
                if (plane.Id == Convert.ToInt32(row.Cells["idPColumn"].Value))
                {
                    currentPlane = plane;
                    _airportForm.WorkersForm.OutAllWorkers(plane);
                    break;
                }
            }

            if (currentPlane == null)
                return;

            switch (currentPlane.Model)
            {
                case "Ту-165":
                    Plane.Tu165Count--;
                    break;
                case "Вжих-621":
                    Plane.Vzih621Count--;
                    break;
                case "Вжих-511":
                    Plane.Vzih511Count--;
                    break;
                case "Это тоже самолёт-25":
                    Plane.EtoTozheSamolet25Count--;
                    break;
                case "Очень дорогой самолёт-319":
                    Plane.OchenDorogoySamolet319Count--;
                    break;
                case "Вжих-512":
                    Plane.Vzih512Count--;
                    break;
                case "Вжих-513":
                    Plane.Vzih513Count--;
                    break;
            }

            _allPlanes.Remove(currentPlane);
            dGVPurchased.Rows.Remove(row);
            _airportForm.AddToSecondMarket(currentPlane);
            _balance += currentPlane.PlanePrice;
        }

        private void returnRentBtn_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = dGVRented.CurrentRow;
            Plane currentPlane = null;

            foreach (Plane plane in _allPlanes)
            {
                if (plane.Id == Convert.ToInt32(row.Cells["idRColumn"].Value))
                {
                    currentPlane = plane;
                    _airportForm.WorkersForm.OutAllWorkers(plane);
                    break;
                }
            }

            if (currentPlane == null)
                return;

            _allPlanes.Remove(currentPlane);
            dGVRented.Rows.Remove(row);
        }
        private void buyLeasingButton_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = dGVLeasing.CurrentRow;
            Plane currentPlane = null;

            foreach (Plane plane in _allPlanes)
            {
                if (plane.Id == Convert.ToInt32(row.Cells["idLColumn"].Value))
                {
                    currentPlane = plane;
                    break;
                }
            }

            if (currentPlane == null)
                return;

            PlaneFromLeasingToPurchased(currentPlane, row);
            _balance -= currentPlane.LeasingBuyCost;
        }

        private void returnLeasingButton_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = dGVLeasing.CurrentRow;
            Plane currentPlane = null;

            foreach (Plane plane in _allPlanes)
            {
                if (plane.Id == Convert.ToInt32(row.Cells["idLColumn"].Value))
                {
                    currentPlane = plane;
                    _airportForm.WorkersForm.OutAllWorkers(plane);
                    break;
                }
            }

            if (currentPlane == null)
                return;

            _allPlanes.Remove(currentPlane);
            dGVLeasing.Rows.Remove(row);
        }

        public void OnBalanceChanged()
        {
            _balance = _airportForm.Balance;

            foreach (Plane plane in _allPlanes)
            {
                if (plane.Status == TradeStatus.Rented)
                {
                    DataGridViewRow currentRow = null;
                    foreach (DataGridViewRow row in dGVRented.Rows)
                    {
                        if (plane.Id == Convert.ToInt32(row.Cells["idRColumn"].Value))
                        {
                            currentRow = row;
                            break;
                        }
                    }

                    if (currentRow != null)
                    {
                        currentRow.Cells["nextPaymentRColumn"].Value = plane.NextPayment.Date;
                    }

                    continue;
                }

                if (plane.Status == TradeStatus.Leasing)
                {
                    DataGridViewRow currentRow = null;
                    foreach (DataGridViewRow row in dGVLeasing.Rows)
                    {
                        if (plane.Id == Convert.ToInt32(row.Cells["idLColumn"].Value))
                        {
                            currentRow = row;
                            break;
                        }
                    }

                    if (currentRow != null)
                    {
                        if (plane.LeasingBuyCost <= 0)
                            PlaneFromLeasingToPurchased(plane, currentRow);
                        else
                        {
                            currentRow.Cells["nextPaymentLColumn"].Value = plane.NextPayment.Date;
                            currentRow.Cells["buyCostLColumn"].Value = plane.LeasingBuyCost;
                        }
                    }
                }
            }
        }
    
        void UpdateOkBtnEnabled()
        {
            DataGridViewRow row = dGVBuy.CurrentRow;
            if (row == null)
            {
                okButton.Enabled = false;
                return;
            }

            if (row.Cells[0].Value == null)
            {
                okButton.Enabled = false;
                return;
            }

            switch (comboBox1.SelectedItem.ToString())
            {
                case "Покупка":
                    tbPrice.Text = row.Cells["costColumn"].Value.ToString();
                    break;
                case "Аренда":
                case "Лизинг":
                    tbPrice.Text = row.Cells["monthPriceColumn"].Value.ToString();
                    if (Convert.ToInt32(row.Cells["costColumn"].Value) - Convert.ToInt32(row.Cells["monthPriceColumn"].Value) <= 0)
                    {
                        okButton.Enabled = false;
                        return;
                    }
                    break;
            }

            okButton.Enabled = _balance >= Convert.ToInt32(tbPrice.Text);
        }

        void PlaneFromLeasingToPurchased(Plane plane, DataGridViewRow row)
        {
            dGVLeasing.Rows.Remove(row);
            plane.Status = TradeStatus.Purchased;
            AddPlaneToPlayersPlanes(plane);
        }

        private void dGVPurchased_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            UpdateAirportInfo();
        }

        private void dGVPurchased_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            UpdateAirportInfo();
        }

        private void dGVBuy_SelectionChanged(object sender, System.EventArgs e)
        {
            UpdateOkBtnEnabled();
        }

        private void dGVRented_SelectionChanged(object sender, EventArgs e)
        {
            returnRentBtn.Enabled = dGVRented.Rows.Count > 0;
        }

        private void dGVPurchased_SelectionChanged(object sender, EventArgs e)
        {
            sellPurshaseBtn.Enabled = dGVPurchased.Rows.Count > 0;
        }

        private void dGVLeasing_SelectionChanged(object sender, EventArgs e)
        {
            buyLeasingButton.Enabled = dGVLeasing.Rows.Count > 0;
            returnLeasingButton.Enabled = dGVLeasing.Rows.Count > 0;
        }

        private void airportButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BuyOrRent_Resize(object sender, EventArgs e)
        {
            dGVBuy.AutoResizeColumns();
            dGVLeasing.AutoResizeColumns();
            dGVPurchased.AutoResizeColumns();
            dGVRented.AutoResizeColumns();

            if (WindowState == FormWindowState.Minimized)
            {
                TimeManager.Suspend();
            }
            else
            {
                TimeManager.Resume();
            }
        }
    }
}