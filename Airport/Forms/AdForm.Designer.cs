﻿namespace Airport.Forms
{
    partial class AdForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdForm));
            this.lbAd = new System.Windows.Forms.Label();
            this.lbComp = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAd = new System.Windows.Forms.Button();
            this.btnAdComp = new System.Windows.Forms.Button();
            this.btnStrike = new System.Windows.Forms.Button();
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.SuspendLayout();
            // 
            // lbAd
            // 
            this.lbAd.AutoSize = true;
            this.lbAd.Location = new System.Drawing.Point(12, 9);
            this.lbAd.Name = "lbAd";
            this.lbAd.Size = new System.Drawing.Size(123, 13);
            this.lbAd.TabIndex = 0;
            this.lbAd.Text = "Реклама (Цена: 50000)";
            // 
            // lbComp
            // 
            this.lbComp.AutoSize = true;
            this.lbComp.Location = new System.Drawing.Point(12, 38);
            this.lbComp.Name = "lbComp";
            this.lbComp.Size = new System.Drawing.Size(206, 13);
            this.lbComp.TabIndex = 1;
            this.lbComp.Text = "Антиреклама конкуренту (Цена: 70000)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Забастовка конкуренту (Цена: 100000)";
            // 
            // btnAd
            // 
            this.btnAd.Location = new System.Drawing.Point(229, 4);
            this.btnAd.Name = "btnAd";
            this.btnAd.Size = new System.Drawing.Size(75, 23);
            this.btnAd.TabIndex = 3;
            this.btnAd.Text = "Заказать";
            this.btnAd.UseVisualStyleBackColor = true;
            this.btnAd.Click += new System.EventHandler(this.btnAd_Click);
            // 
            // btnAdComp
            // 
            this.btnAdComp.Location = new System.Drawing.Point(229, 33);
            this.btnAdComp.Name = "btnAdComp";
            this.btnAdComp.Size = new System.Drawing.Size(75, 23);
            this.btnAdComp.TabIndex = 4;
            this.btnAdComp.Text = "Заказать";
            this.btnAdComp.UseVisualStyleBackColor = true;
            this.btnAdComp.Click += new System.EventHandler(this.btnAdComp_Click);
            // 
            // btnStrike
            // 
            this.btnStrike.Location = new System.Drawing.Point(229, 62);
            this.btnStrike.Name = "btnStrike";
            this.btnStrike.Size = new System.Drawing.Size(75, 23);
            this.btnStrike.TabIndex = 5;
            this.btnStrike.Text = "Заказать";
            this.btnStrike.UseVisualStyleBackColor = true;
            this.btnStrike.Click += new System.EventHandler(this.btnStrike_Click);
            // 
            // AdForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(314, 96);
            this.Controls.Add(this.btnStrike);
            this.Controls.Add(this.btnAdComp);
            this.Controls.Add(this.btnAd);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbComp);
            this.Controls.Add(this.lbAd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AdForm";
            this.Text = "Реклама";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbAd;
        private System.Windows.Forms.Label lbComp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAd;
        private System.Windows.Forms.Button btnAdComp;
        private System.Windows.Forms.Button btnStrike;
    }
}