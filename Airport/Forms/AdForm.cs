﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Airport.Forms
{
    public partial class AdForm : Form
    {
        Airport airport;
        Random rnd;
        public AdForm(Airport airport)
        {
            InitializeComponent();
            this.airport = airport;
            rnd = new Random();
        }

        private void btnAd_Click(object sender, EventArgs e)
        {
            int rand = rnd.Next(20, 60);
            airport.Balance -= 50000;
            airport.Reputation += rand;
            MessageBox.Show("Ваша репутация повысилась на " + rand.ToString() + " очков");
        }

        private void btnAdComp_Click(object sender, EventArgs e)
        {
            airport.Balance -= 70000;
            airport._competitor.AntiAd = true;
            MessageBox.Show("Вы заказали антирекламу для конкурента");
        }

        private void btnStrike_Click(object sender, EventArgs e)
        {
            airport.Balance -= 100000;
            airport._competitor.IsStrike = true;
            MessageBox.Show("Вы заказали забастовку для конкурента");
        }
    }
}
