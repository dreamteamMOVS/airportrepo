﻿namespace Airport
{
    partial class Airport
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Airport));
            this.dGVPlane = new System.Windows.Forms.DataGridView();
            this.modelAirplane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pilotColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stewardessColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.strikeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeAirplane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnCurrLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.volumeAirplane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnSpeed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.distanceAirplane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costOfService = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.consumptionAirplane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.leaseAirplane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Place = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costRegularity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Volume = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Forfeit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costAirplane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMyPassFlights = new System.Windows.Forms.DataGridView();
            this.clmnTypeFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnPlaceFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnPlaceTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnReg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnDist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnPeak = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnDeadline = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnForfeit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbBalance = new System.Windows.Forms.TextBox();
            this.lbBalance = new System.Windows.Forms.Label();
            this.lbAirplane = new System.Windows.Forms.Label();
            this.lbMyPassFlight = new System.Windows.Forms.Label();
            this.btBuyOrRent = new System.Windows.Forms.Button();
            this.gbTimer = new System.Windows.Forms.GroupBox();
            this.lbCurrMult = new System.Windows.Forms.Label();
            this.btnUp = new System.Windows.Forms.Button();
            this.btnDown = new System.Windows.Forms.Button();
            this.lbMult = new System.Windows.Forms.Label();
            this.lbCurrTime = new System.Windows.Forms.Label();
            this.lbTimeText = new System.Windows.Forms.Label();
            this.mcCalendar = new System.Windows.Forms.MonthCalendar();
            this.lbMyCargoFlights = new System.Windows.Forms.Label();
            this.dgvMyCargoFlights = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnIncome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAddPassFlights = new System.Windows.Forms.Button();
            this.btnAddCargoFlights = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbNextPayment = new System.Windows.Forms.TextBox();
            this.btAddPassFlightInSchedule = new System.Windows.Forms.Button();
            this.btAddCargoFlightInSchedule = new System.Windows.Forms.Button();
            this.dGVSchedule = new System.Windows.Forms.DataGridView();
            this.clmnStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modelPlane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnTimeFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnTimeTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toPlaceFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fromPlaceFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Distance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Income = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbSchedule = new System.Windows.Forms.Label();
            this.tbTest = new System.Windows.Forms.TextBox();
            this.labelcomp = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbReputation = new System.Windows.Forms.TextBox();
            this.btWorkers = new System.Windows.Forms.Button();
            this.btnSecMarket = new System.Windows.Forms.Button();
            this.btnAd = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dGVPlane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMyPassFlights)).BeginInit();
            this.gbTimer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMyCargoFlights)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGVSchedule)).BeginInit();
            this.SuspendLayout();
            // 
            // dGVPlane
            // 
            this.dGVPlane.AllowUserToAddRows = false;
            this.dGVPlane.AllowUserToDeleteRows = false;
            this.dGVPlane.AllowUserToResizeColumns = false;
            this.dGVPlane.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dGVPlane.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVPlane.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.modelAirplane,
            this.clmnAge,
            this.pilotColumn,
            this.stewardessColumn,
            this.strikeColumn,
            this.typeAirplane,
            this.clmnCurrLocation,
            this.volumeAirplane,
            this.clmnSpeed,
            this.distanceAirplane,
            this.costOfService,
            this.consumptionAirplane,
            this.leaseAirplane});
            this.dGVPlane.Location = new System.Drawing.Point(16, 43);
            this.dGVPlane.Name = "dGVPlane";
            this.dGVPlane.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dGVPlane.Size = new System.Drawing.Size(1120, 106);
            this.dGVPlane.TabIndex = 0;
            // 
            // modelAirplane
            // 
            this.modelAirplane.HeaderText = "Модель";
            this.modelAirplane.Name = "modelAirplane";
            this.modelAirplane.ReadOnly = true;
            this.modelAirplane.Width = 84;
            // 
            // clmnAge
            // 
            this.clmnAge.HeaderText = "Дата изготовления";
            this.clmnAge.Name = "clmnAge";
            this.clmnAge.Width = 84;
            // 
            // pilotColumn
            // 
            this.pilotColumn.HeaderText = "Пилоты";
            this.pilotColumn.Name = "pilotColumn";
            this.pilotColumn.Width = 84;
            // 
            // stewardessColumn
            // 
            this.stewardessColumn.HeaderText = "Стюардессы";
            this.stewardessColumn.Name = "stewardessColumn";
            this.stewardessColumn.Width = 83;
            // 
            // strikeColumn
            // 
            this.strikeColumn.HeaderText = "Забастовка";
            this.strikeColumn.Name = "strikeColumn";
            this.strikeColumn.Width = 84;
            // 
            // typeAirplane
            // 
            this.typeAirplane.HeaderText = "Тип самолёта";
            this.typeAirplane.Name = "typeAirplane";
            this.typeAirplane.ReadOnly = true;
            this.typeAirplane.Width = 84;
            // 
            // clmnCurrLocation
            // 
            this.clmnCurrLocation.HeaderText = "Местоположение";
            this.clmnCurrLocation.Name = "clmnCurrLocation";
            this.clmnCurrLocation.ReadOnly = true;
            this.clmnCurrLocation.Width = 84;
            // 
            // volumeAirplane
            // 
            this.volumeAirplane.HeaderText = "Грузоподъёмность / вместительность";
            this.volumeAirplane.Name = "volumeAirplane";
            this.volumeAirplane.ReadOnly = true;
            this.volumeAirplane.Width = 84;
            // 
            // clmnSpeed
            // 
            this.clmnSpeed.HeaderText = "Скорость";
            this.clmnSpeed.Name = "clmnSpeed";
            this.clmnSpeed.ReadOnly = true;
            this.clmnSpeed.Width = 84;
            // 
            // distanceAirplane
            // 
            this.distanceAirplane.HeaderText = "Дальность";
            this.distanceAirplane.Name = "distanceAirplane";
            this.distanceAirplane.ReadOnly = true;
            this.distanceAirplane.Width = 83;
            // 
            // costOfService
            // 
            this.costOfService.HeaderText = "Стоимость обслуживания";
            this.costOfService.Name = "costOfService";
            this.costOfService.ReadOnly = true;
            this.costOfService.Width = 84;
            // 
            // consumptionAirplane
            // 
            this.consumptionAirplane.HeaderText = "Расход";
            this.consumptionAirplane.Name = "consumptionAirplane";
            this.consumptionAirplane.ReadOnly = true;
            this.consumptionAirplane.Width = 84;
            // 
            // leaseAirplane
            // 
            this.leaseAirplane.HeaderText = "Аренда";
            this.leaseAirplane.Name = "leaseAirplane";
            this.leaseAirplane.ReadOnly = true;
            this.leaseAirplane.Width = 84;
            // 
            // typeFlight
            // 
            this.typeFlight.Name = "typeFlight";
            // 
            // Place
            // 
            this.Place.Name = "Place";
            // 
            // costRegularity
            // 
            this.costRegularity.Name = "costRegularity";
            // 
            // costFlight
            // 
            this.costFlight.Name = "costFlight";
            // 
            // Volume
            // 
            this.Volume.Name = "Volume";
            // 
            // Forfeit
            // 
            this.Forfeit.Name = "Forfeit";
            // 
            // costAirplane
            // 
            this.costAirplane.HeaderText = "Стоимость";
            this.costAirplane.Name = "costAirplane";
            this.costAirplane.ReadOnly = true;
            // 
            // dgvMyPassFlights
            // 
            this.dgvMyPassFlights.AllowUserToAddRows = false;
            this.dgvMyPassFlights.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMyPassFlights.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMyPassFlights.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmnTypeFlight,
            this.clmnPlaceFrom,
            this.clmnPlaceTo,
            this.clmnReg,
            this.clmnDist,
            this.clmnPrice,
            this.clmnPeak,
            this.clmnDeadline,
            this.clmnForfeit});
            this.dgvMyPassFlights.Location = new System.Drawing.Point(16, 196);
            this.dgvMyPassFlights.MultiSelect = false;
            this.dgvMyPassFlights.Name = "dgvMyPassFlights";
            this.dgvMyPassFlights.ReadOnly = true;
            this.dgvMyPassFlights.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMyPassFlights.Size = new System.Drawing.Size(1018, 119);
            this.dgvMyPassFlights.TabIndex = 1;
            this.dgvMyPassFlights.SelectionChanged += new System.EventHandler(this.dgvMyPassFlights_SelectionChanged);
            this.dgvMyPassFlights.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvMyPassFlights_KeyDown);
            // 
            // clmnTypeFlight
            // 
            this.clmnTypeFlight.HeaderText = "Тип рейса";
            this.clmnTypeFlight.Name = "clmnTypeFlight";
            this.clmnTypeFlight.ReadOnly = true;
            // 
            // clmnPlaceFrom
            // 
            this.clmnPlaceFrom.HeaderText = "Место отправления";
            this.clmnPlaceFrom.Name = "clmnPlaceFrom";
            this.clmnPlaceFrom.ReadOnly = true;
            // 
            // clmnPlaceTo
            // 
            this.clmnPlaceTo.HeaderText = "Место назначения";
            this.clmnPlaceTo.Name = "clmnPlaceTo";
            this.clmnPlaceTo.ReadOnly = true;
            // 
            // clmnReg
            // 
            this.clmnReg.HeaderText = "Регулярность";
            this.clmnReg.Name = "clmnReg";
            this.clmnReg.ReadOnly = true;
            // 
            // clmnDist
            // 
            this.clmnDist.HeaderText = "Дальность";
            this.clmnDist.Name = "clmnDist";
            this.clmnDist.ReadOnly = true;
            // 
            // clmnPrice
            // 
            this.clmnPrice.HeaderText = "Стоимость билета";
            this.clmnPrice.Name = "clmnPrice";
            this.clmnPrice.ReadOnly = true;
            // 
            // clmnPeak
            // 
            this.clmnPeak.HeaderText = "Пиковые часы";
            this.clmnPeak.Name = "clmnPeak";
            this.clmnPeak.ReadOnly = true;
            // 
            // clmnDeadline
            // 
            this.clmnDeadline.HeaderText = "Конец жизни";
            this.clmnDeadline.Name = "clmnDeadline";
            this.clmnDeadline.ReadOnly = true;
            // 
            // clmnForfeit
            // 
            this.clmnForfeit.HeaderText = "Неустойка";
            this.clmnForfeit.Name = "clmnForfeit";
            this.clmnForfeit.ReadOnly = true;
            // 
            // tbBalance
            // 
            this.tbBalance.Location = new System.Drawing.Point(99, 15);
            this.tbBalance.Name = "tbBalance";
            this.tbBalance.ReadOnly = true;
            this.tbBalance.Size = new System.Drawing.Size(100, 20);
            this.tbBalance.TabIndex = 2;
            // 
            // lbBalance
            // 
            this.lbBalance.AutoSize = true;
            this.lbBalance.Location = new System.Drawing.Point(13, 18);
            this.lbBalance.Name = "lbBalance";
            this.lbBalance.Size = new System.Drawing.Size(85, 13);
            this.lbBalance.TabIndex = 3;
            this.lbBalance.Text = "Баланс игрока:";
            // 
            // lbAirplane
            // 
            this.lbAirplane.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbAirplane.AutoSize = true;
            this.lbAirplane.Location = new System.Drawing.Point(464, 27);
            this.lbAirplane.Name = "lbAirplane";
            this.lbAirplane.Size = new System.Drawing.Size(118, 13);
            this.lbAirplane.TabIndex = 4;
            this.lbAirplane.Text = "Доступные самолёты";
            // 
            // lbMyPassFlight
            // 
            this.lbMyPassFlight.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbMyPassFlight.AutoSize = true;
            this.lbMyPassFlight.Location = new System.Drawing.Point(389, 180);
            this.lbMyPassFlight.Name = "lbMyPassFlight";
            this.lbMyPassFlight.Size = new System.Drawing.Size(178, 13);
            this.lbMyPassFlight.TabIndex = 5;
            this.lbMyPassFlight.Text = "Выбранные пассажирские рейсы";
            // 
            // btBuyOrRent
            // 
            this.btBuyOrRent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btBuyOrRent.Location = new System.Drawing.Point(969, 162);
            this.btBuyOrRent.Name = "btBuyOrRent";
            this.btBuyOrRent.Size = new System.Drawing.Size(169, 29);
            this.btBuyOrRent.TabIndex = 6;
            this.btBuyOrRent.Text = "Купить/Арендовать самолёт";
            this.btBuyOrRent.UseVisualStyleBackColor = true;
            this.btBuyOrRent.Click += new System.EventHandler(this.btBuyOrRent_Click);
            // 
            // gbTimer
            // 
            this.gbTimer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbTimer.Controls.Add(this.lbCurrMult);
            this.gbTimer.Controls.Add(this.btnUp);
            this.gbTimer.Controls.Add(this.btnDown);
            this.gbTimer.Controls.Add(this.lbMult);
            this.gbTimer.Controls.Add(this.lbCurrTime);
            this.gbTimer.Controls.Add(this.lbTimeText);
            this.gbTimer.Controls.Add(this.mcCalendar);
            this.gbTimer.Location = new System.Drawing.Point(12, 464);
            this.gbTimer.Name = "gbTimer";
            this.gbTimer.Size = new System.Drawing.Size(187, 221);
            this.gbTimer.TabIndex = 7;
            this.gbTimer.TabStop = false;
            this.gbTimer.Text = "Игровое время";
            // 
            // lbCurrMult
            // 
            this.lbCurrMult.AutoSize = true;
            this.lbCurrMult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCurrMult.Location = new System.Drawing.Point(108, 51);
            this.lbCurrMult.Name = "lbCurrMult";
            this.lbCurrMult.Size = new System.Drawing.Size(18, 13);
            this.lbCurrMult.TabIndex = 6;
            this.lbCurrMult.Text = "x1";
            // 
            // btnUp
            // 
            this.btnUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnUp.Location = new System.Drawing.Point(135, 47);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(26, 18);
            this.btnUp.TabIndex = 5;
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnDown
            // 
            this.btnDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnDown.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnDown.Location = new System.Drawing.Point(73, 47);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(26, 18);
            this.btnDown.TabIndex = 4;
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // lbMult
            // 
            this.lbMult.AutoSize = true;
            this.lbMult.Location = new System.Drawing.Point(9, 50);
            this.lbMult.Name = "lbMult";
            this.lbMult.Size = new System.Drawing.Size(58, 13);
            this.lbMult.TabIndex = 3;
            this.lbMult.Text = "Скорость:";
            // 
            // lbCurrTime
            // 
            this.lbCurrTime.AutoSize = true;
            this.lbCurrTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCurrTime.Location = new System.Drawing.Point(104, 28);
            this.lbCurrTime.Name = "lbCurrTime";
            this.lbCurrTime.Size = new System.Drawing.Size(14, 13);
            this.lbCurrTime.TabIndex = 2;
            this.lbCurrTime.Text = "0";
            // 
            // lbTimeText
            // 
            this.lbTimeText.AutoSize = true;
            this.lbTimeText.Location = new System.Drawing.Point(9, 28);
            this.lbTimeText.Name = "lbTimeText";
            this.lbTimeText.Size = new System.Drawing.Size(90, 13);
            this.lbTimeText.TabIndex = 1;
            this.lbTimeText.Text = "Текущее время:";
            // 
            // mcCalendar
            // 
            this.mcCalendar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.mcCalendar.Enabled = false;
            this.mcCalendar.Location = new System.Drawing.Point(12, 73);
            this.mcCalendar.Name = "mcCalendar";
            this.mcCalendar.ShowToday = false;
            this.mcCalendar.ShowTodayCircle = false;
            this.mcCalendar.TabIndex = 0;
            // 
            // lbMyCargoFlights
            // 
            this.lbMyCargoFlights.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbMyCargoFlights.AutoSize = true;
            this.lbMyCargoFlights.Location = new System.Drawing.Point(403, 334);
            this.lbMyCargoFlights.Name = "lbMyCargoFlights";
            this.lbMyCargoFlights.Size = new System.Drawing.Size(152, 13);
            this.lbMyCargoFlights.TabIndex = 9;
            this.lbMyCargoFlights.Text = "Выбранные грузовые рейсы";
            // 
            // dgvMyCargoFlights
            // 
            this.dgvMyCargoFlights.AllowUserToAddRows = false;
            this.dgvMyCargoFlights.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMyCargoFlights.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMyCargoFlights.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn6,
            this.clmnWeight,
            this.dataGridViewTextBoxColumn8,
            this.clmnIncome,
            this.dataGridViewTextBoxColumn9});
            this.dgvMyCargoFlights.Location = new System.Drawing.Point(16, 350);
            this.dgvMyCargoFlights.MultiSelect = false;
            this.dgvMyCargoFlights.Name = "dgvMyCargoFlights";
            this.dgvMyCargoFlights.ReadOnly = true;
            this.dgvMyCargoFlights.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMyCargoFlights.Size = new System.Drawing.Size(1018, 102);
            this.dgvMyCargoFlights.TabIndex = 10;
            this.dgvMyCargoFlights.SelectionChanged += new System.EventHandler(this.dgvMyCargoFlights_SelectionChanged);
            this.dgvMyCargoFlights.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvMyCargoFlights_KeyDown);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Тип рейса";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Место отправления";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Место назначения";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Дальность";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // clmnWeight
            // 
            this.clmnWeight.HeaderText = "Грузоподъемность";
            this.clmnWeight.Name = "clmnWeight";
            this.clmnWeight.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Конец жизни";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // clmnIncome
            // 
            this.clmnIncome.HeaderText = "Доходность";
            this.clmnIncome.Name = "clmnIncome";
            this.clmnIncome.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Неустойка";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // btnAddPassFlights
            // 
            this.btnAddPassFlights.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddPassFlights.Location = new System.Drawing.Point(1040, 197);
            this.btnAddPassFlights.Name = "btnAddPassFlights";
            this.btnAddPassFlights.Size = new System.Drawing.Size(98, 49);
            this.btnAddPassFlights.TabIndex = 11;
            this.btnAddPassFlights.Text = "Добавить пассажирские рейсы";
            this.btnAddPassFlights.UseVisualStyleBackColor = true;
            this.btnAddPassFlights.Click += new System.EventHandler(this.btnAddPassFlights_Click);
            // 
            // btnAddCargoFlights
            // 
            this.btnAddCargoFlights.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddCargoFlights.Location = new System.Drawing.Point(1040, 350);
            this.btnAddCargoFlights.Name = "btnAddCargoFlights";
            this.btnAddCargoFlights.Size = new System.Drawing.Size(98, 49);
            this.btnAddCargoFlights.TabIndex = 12;
            this.btnAddCargoFlights.Text = "Добавить грузовые рейсы";
            this.btnAddCargoFlights.UseVisualStyleBackColor = true;
            this.btnAddCargoFlights.Click += new System.EventHandler(this.btnAddCargoFlights_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(912, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "К оплате в конце дня:";
            // 
            // tbNextPayment
            // 
            this.tbNextPayment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNextPayment.Location = new System.Drawing.Point(1036, 15);
            this.tbNextPayment.Name = "tbNextPayment";
            this.tbNextPayment.ReadOnly = true;
            this.tbNextPayment.Size = new System.Drawing.Size(100, 20);
            this.tbNextPayment.TabIndex = 13;
            // 
            // btAddPassFlightInSchedule
            // 
            this.btAddPassFlightInSchedule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btAddPassFlightInSchedule.Location = new System.Drawing.Point(1040, 252);
            this.btAddPassFlightInSchedule.Name = "btAddPassFlightInSchedule";
            this.btAddPassFlightInSchedule.Size = new System.Drawing.Size(98, 64);
            this.btAddPassFlightInSchedule.TabIndex = 13;
            this.btAddPassFlightInSchedule.Text = "Поставить пассажирский рейс в расписание";
            this.btAddPassFlightInSchedule.UseVisualStyleBackColor = true;
            this.btAddPassFlightInSchedule.Click += new System.EventHandler(this.btAddPassFlightInSchedule_Click);
            // 
            // btAddCargoFlightInSchedule
            // 
            this.btAddCargoFlightInSchedule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btAddCargoFlightInSchedule.Location = new System.Drawing.Point(1040, 405);
            this.btAddCargoFlightInSchedule.Name = "btAddCargoFlightInSchedule";
            this.btAddCargoFlightInSchedule.Size = new System.Drawing.Size(98, 47);
            this.btAddCargoFlightInSchedule.TabIndex = 14;
            this.btAddCargoFlightInSchedule.Text = "Поставить грузовой рейс в расписание";
            this.btAddCargoFlightInSchedule.UseVisualStyleBackColor = true;
            this.btAddCargoFlightInSchedule.Click += new System.EventHandler(this.btAddCargoFlightInSchedule_Click);
            // 
            // dGVSchedule
            // 
            this.dGVSchedule.AllowUserToAddRows = false;
            this.dGVSchedule.AllowUserToDeleteRows = false;
            this.dGVSchedule.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dGVSchedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVSchedule.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmnStatus,
            this.modelPlane,
            this.Type,
            this.clmnTimeFrom,
            this.clmnTimeTo,
            this.toPlaceFlight,
            this.fromPlaceFlight,
            this.Distance,
            this.Income});
            this.dGVSchedule.Location = new System.Drawing.Point(205, 484);
            this.dGVSchedule.Name = "dGVSchedule";
            this.dGVSchedule.ReadOnly = true;
            this.dGVSchedule.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dGVSchedule.Size = new System.Drawing.Size(933, 217);
            this.dGVSchedule.TabIndex = 15;
            // 
            // clmnStatus
            // 
            this.clmnStatus.HeaderText = "Статус";
            this.clmnStatus.Name = "clmnStatus";
            this.clmnStatus.ReadOnly = true;
            this.clmnStatus.Width = 90;
            // 
            // modelPlane
            // 
            this.modelPlane.HeaderText = "Модель самолёта";
            this.modelPlane.Name = "modelPlane";
            this.modelPlane.ReadOnly = true;
            // 
            // Type
            // 
            this.Type.HeaderText = "Тип рейса";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            // 
            // clmnTimeFrom
            // 
            this.clmnTimeFrom.HeaderText = "Отправка";
            this.clmnTimeFrom.Name = "clmnTimeFrom";
            this.clmnTimeFrom.ReadOnly = true;
            // 
            // clmnTimeTo
            // 
            this.clmnTimeTo.HeaderText = "Прибытие";
            this.clmnTimeTo.Name = "clmnTimeTo";
            this.clmnTimeTo.ReadOnly = true;
            // 
            // toPlaceFlight
            // 
            this.toPlaceFlight.HeaderText = "Место отправления";
            this.toPlaceFlight.Name = "toPlaceFlight";
            this.toPlaceFlight.ReadOnly = true;
            // 
            // fromPlaceFlight
            // 
            this.fromPlaceFlight.HeaderText = "Место назначения";
            this.fromPlaceFlight.Name = "fromPlaceFlight";
            this.fromPlaceFlight.ReadOnly = true;
            // 
            // Distance
            // 
            this.Distance.HeaderText = "Дальность";
            this.Distance.Name = "Distance";
            this.Distance.ReadOnly = true;
            // 
            // Income
            // 
            this.Income.HeaderText = "Доходность";
            this.Income.Name = "Income";
            this.Income.ReadOnly = true;
            // 
            // lbSchedule
            // 
            this.lbSchedule.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbSchedule.AutoSize = true;
            this.lbSchedule.Location = new System.Drawing.Point(644, 468);
            this.lbSchedule.Name = "lbSchedule";
            this.lbSchedule.Size = new System.Drawing.Size(68, 13);
            this.lbSchedule.TabIndex = 16;
            this.lbSchedule.Text = "Расписание";
            // 
            // tbTest
            // 
            this.tbTest.Location = new System.Drawing.Point(700, 13);
            this.tbTest.Name = "tbTest";
            this.tbTest.ReadOnly = true;
            this.tbTest.Size = new System.Drawing.Size(100, 20);
            this.tbTest.TabIndex = 17;
            // 
            // labelcomp
            // 
            this.labelcomp.AutoSize = true;
            this.labelcomp.Location = new System.Drawing.Point(594, 17);
            this.labelcomp.Name = "labelcomp";
            this.labelcomp.Size = new System.Drawing.Size(100, 13);
            this.labelcomp.TabIndex = 18;
            this.labelcomp.Text = "Конкурент (ТЕСТ):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(221, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Репутация:";
            // 
            // tbReputation
            // 
            this.tbReputation.Location = new System.Drawing.Point(284, 15);
            this.tbReputation.Name = "tbReputation";
            this.tbReputation.ReadOnly = true;
            this.tbReputation.Size = new System.Drawing.Size(45, 20);
            this.tbReputation.TabIndex = 18;
            // 
            // btWorkers
            // 
            this.btWorkers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btWorkers.Location = new System.Drawing.Point(755, 161);
            this.btWorkers.Name = "btWorkers";
            this.btWorkers.Size = new System.Drawing.Size(86, 29);
            this.btWorkers.TabIndex = 19;
            this.btWorkers.Text = "Рынок труда";
            this.btWorkers.UseVisualStyleBackColor = true;
            this.btWorkers.Click += new System.EventHandler(this.btWorkers_Click);
            // 
            // btnSecMarket
            // 
            this.btnSecMarket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSecMarket.Location = new System.Drawing.Point(847, 161);
            this.btnSecMarket.Name = "btnSecMarket";
            this.btnSecMarket.Size = new System.Drawing.Size(116, 29);
            this.btnSecMarket.TabIndex = 20;
            this.btnSecMarket.Text = "Вторичный рынок";
            this.btnSecMarket.UseVisualStyleBackColor = true;
            this.btnSecMarket.Click += new System.EventHandler(this.btnSecMarket_Click);
            // 
            // btnAd
            // 
            this.btnAd.Location = new System.Drawing.Point(827, 12);
            this.btnAd.Name = "btnAd";
            this.btnAd.Size = new System.Drawing.Size(75, 23);
            this.btnAd.TabIndex = 21;
            this.btnAd.Text = "Реклама";
            this.btnAd.UseVisualStyleBackColor = true;
            this.btnAd.Click += new System.EventHandler(this.btnAd_Click);
            // 
            // Airport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(1148, 709);
            this.Controls.Add(this.btnAd);
            this.Controls.Add(this.labelcomp);
            this.Controls.Add(this.tbTest);
            this.Controls.Add(this.btnSecMarket);
            this.Controls.Add(this.btWorkers);
            this.Controls.Add(this.tbReputation);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbNextPayment);
            this.Controls.Add(this.lbSchedule);
            this.Controls.Add(this.dGVSchedule);
            this.Controls.Add(this.btAddCargoFlightInSchedule);
            this.Controls.Add(this.btAddPassFlightInSchedule);
            this.Controls.Add(this.btnAddCargoFlights);
            this.Controls.Add(this.btnAddPassFlights);
            this.Controls.Add(this.dgvMyCargoFlights);
            this.Controls.Add(this.lbMyCargoFlights);
            this.Controls.Add(this.gbTimer);
            this.Controls.Add(this.btBuyOrRent);
            this.Controls.Add(this.lbMyPassFlight);
            this.Controls.Add(this.lbAirplane);
            this.Controls.Add(this.lbBalance);
            this.Controls.Add(this.tbBalance);
            this.Controls.Add(this.dgvMyPassFlights);
            this.Controls.Add(this.dGVPlane);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Airport";
            this.Text = "Airport";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Airport_FormClosing);
            this.Resize += new System.EventHandler(this.Airport_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dGVPlane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMyPassFlights)).EndInit();
            this.gbTimer.ResumeLayout(false);
            this.gbTimer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMyCargoFlights)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGVSchedule)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dGVPlane;
        private System.Windows.Forms.DataGridView dgvMyPassFlights;
        private System.Windows.Forms.TextBox tbBalance;
        private System.Windows.Forms.Label lbBalance;
        private System.Windows.Forms.Label lbAirplane;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn Place;
        private System.Windows.Forms.DataGridViewTextBoxColumn costRegularity;
        private System.Windows.Forms.DataGridViewTextBoxColumn costFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn Volume;
        private System.Windows.Forms.DataGridViewTextBoxColumn Forfeit;
        private System.Windows.Forms.Label lbMyPassFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn costAirplane;
        private System.Windows.Forms.Button btBuyOrRent;
        private System.Windows.Forms.GroupBox gbTimer;
        private System.Windows.Forms.MonthCalendar mcCalendar;
        private System.Windows.Forms.Label lbCurrTime;
        private System.Windows.Forms.Label lbTimeText;
        private System.Windows.Forms.Label lbCurrMult;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Label lbMult;
        private System.Windows.Forms.Label lbMyCargoFlights;
        private System.Windows.Forms.DataGridView dgvMyCargoFlights;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnIncome;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.Button btnAddPassFlights;
        private System.Windows.Forms.Button btnAddCargoFlights;
        private System.Windows.Forms.Button btAddPassFlightInSchedule;
        private System.Windows.Forms.Button btAddCargoFlightInSchedule;
        private System.Windows.Forms.DataGridView dGVSchedule;
        private System.Windows.Forms.Label lbSchedule;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelPlane;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnTimeFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnTimeTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn toPlaceFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn fromPlaceFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn Distance;
        private System.Windows.Forms.DataGridViewTextBoxColumn Income;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnTypeFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnPlaceFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnPlaceTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnReg;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnDist;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnPeak;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnDeadline;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnForfeit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbNextPayment;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btWorkers;
        private System.Windows.Forms.TextBox tbReputation;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelAirplane;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnAge;
        private System.Windows.Forms.DataGridViewTextBoxColumn pilotColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stewardessColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn strikeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeAirplane;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnCurrLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn volumeAirplane;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnSpeed;
        private System.Windows.Forms.DataGridViewTextBoxColumn distanceAirplane;
        private System.Windows.Forms.DataGridViewTextBoxColumn costOfService;
        private System.Windows.Forms.DataGridViewTextBoxColumn consumptionAirplane;
        private System.Windows.Forms.DataGridViewTextBoxColumn leaseAirplane;
        private System.Windows.Forms.TextBox tbTest;
        private System.Windows.Forms.Label labelcomp;
        private System.Windows.Forms.Button btnSecMarket;
        private System.Windows.Forms.Button btnAd;
    }
}