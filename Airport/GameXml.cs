﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Airport
{
    [Serializable]
    public class XmlWrapper
    {
        public int Balance { get; set; }

        public DateTime Time { get; set; }

        public List<Plane> Planes { get; set; }

        public List<PassengerFlight> SelectedPassFlights { get; set; }

        public List<PassengerFlight> NotSelectedPassFlights { get; set; }

        public List<CargoFlight> SelectedCargoFlights { get; set; }

        public List<CargoFlight> NotSelectedCargoFlights { get; set; }

        public List<ScheduledFlight> ScheduledFlights { get; set; }
    }

    public static class GameXml
    {
        private static string _oldPath = "";

        public static void SaveAs(XmlWrapper wrapper)
        {
            SaveFileDialog saveDlg = new SaveFileDialog
            {
                FileName = "Аэропорт",
                DefaultExt = "xml",
                Filter = "Файлы XML (*.xml)|*.xml"
            };

            if (saveDlg.ShowDialog() != DialogResult.OK)
                return;

            _oldPath = saveDlg.FileName;
            Save(wrapper);
        }

        public static void Save(XmlWrapper wrapper)
        {
            if (_oldPath == "")
                SaveAs(wrapper);

            XmlSerializer formatter = new XmlSerializer(typeof(XmlWrapper));
            using (FileStream fs = new FileStream(_oldPath, FileMode.Create))
            {
                formatter.Serialize(fs, wrapper);
            }
        }

        public static XmlWrapper Load()
        {
            XmlWrapper wrapper;

            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Файлы XML (*.xml)|*.xml";
            if (dlg.ShowDialog() != DialogResult.OK)
                return null;

            _oldPath = dlg.FileName;

            XmlSerializer formatter = new XmlSerializer(typeof(XmlWrapper));
            using (FileStream fs = new FileStream(_oldPath, FileMode.Open))
            {
                wrapper = (XmlWrapper)formatter.Deserialize(fs);
            }

            return wrapper;
        }
    }
}