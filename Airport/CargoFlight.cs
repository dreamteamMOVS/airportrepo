﻿using System;

namespace Airport
{
    public class CargoFlight : Flight
    {
        public double Weight { get; set; }

        // Дефолтный конструктор необходим для сериализации. Не удалять
        public CargoFlight()
        {
        }
        
        /// <summary>
        /// Конструктор грузового рейса.
        /// </summary>
        /// <param name="dt">Текущее игровое время.</param>
        /// <param name="seed">Начальное значение для генератора псевдослучайных чисел.</param>
        ///
        public CargoFlight(DateTime dt, int seed)
        : base(seed)
        {
            Type = FlightType.Cargo;

            GenDateEnd(dt);
            GenWeight();
            GenIncome();
        }

        /// <summary>
        /// Крайний срок реализации рейса, после которого необходимо уплатить неустойку,
        /// на 1-15 дней позже даты его взятия.
        /// </summary>
        /// <param name="dt">Текущее игровое время.</param>
        private void GenDateEnd(DateTime dt)
        {
            int deadline = Rnd.Next(1, 15);
            DateEnd = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
            DateEnd = DateEnd.AddDays(deadline);
        }

        /// <summary>
        /// Случайная грузоподъемность от 10 до 100 тонн.
        /// </summary>
        private void GenWeight()
        {
            Weight = Rnd.Next(10, 100);
        }

        /// <summary>
        /// Случайный доход с учетом дальности перелёта.
        /// </summary>
        private void GenIncome()
        {
            Income = (int)(Rnd.Next(500, 1000) * Distance);
        }
    }
}