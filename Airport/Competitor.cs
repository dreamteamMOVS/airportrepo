﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Airport
{
    public class Competitor
    {
        private Airport airport;
        public int CompBalance { get; set; }
        Random rnd;
        int h1 = 1, h2 = 7, h3 = 13, h4 = 20, hforfeit = 4, hempty1 = 5, hempty2 = 10;
        bool[] flag = new bool[20];

        List<PassengerFlight> CompPassFlight { get; set; }
        List<CargoFlight> CompCargoFlight { get; set; }
        public List<PassengerFlight> GenPassFlight { get; set; }
        public List<CargoFlight> GenCargoFlight { get; set; }
        List<Plane> CompPlanes { get; set; }
        public List<Plane> GenPlanes { get; set; }
        public double AverageSalaryComp {get; set;}
        public int Reputation { get; set; }
        public List<Worker> Compcrew { get; set; }
        public List<Worker> GenCrew { get; set; }
        public bool IsStrike { get; set; }
        int strikeDay;
        public bool AntiAd { get; set; }
        DateTime DateDeath;
        DateTime playerAd, playerStrike;

        public Competitor(Airport airport)
        {
            this.CompPassFlight = new List<PassengerFlight>();
            this.CompCargoFlight = new List<CargoFlight>();
            this.Compcrew = new List<Worker>();
            this.airport = airport;
            this.CompPlanes = new List<Plane>();
            this.Reputation = 100;
            this.AverageSalaryComp = 0;
            rnd = new Random();
            this.CompBalance = 100000;
            strikeDay = 12;
            DateDeath = TimeManager.Time.AddDays(rnd.Next(10, 50));
            DateDeath = DateDeath.AddHours(rnd.Next(1, 23));
            playerAd = TimeManager.Time.AddDays(rnd.Next(5, 15));
            playerAd = playerAd.AddHours(rnd.Next(1, 23));
            playerStrike = TimeManager.Time.AddDays(rnd.Next(5, 15));
            playerStrike = playerStrike.AddHours(rnd.Next(1, 23));
        }

        public void AddPass()
        {
            PassengerFlight rand = new PassengerFlight();
            int irnd = rnd.Next(1, 4);
            if (irnd > airport.PassengerFlights.Generated.Count - 1) irnd = airport.PassengerFlights.Generated.Count - 1;
            for (int i=0; i < irnd; i++)
            {
                rand = GenPassFlight[rnd.Next(0, GenPassFlight.Count - 1)];
                CompPassFlight.Add(rand);
                GenPassFlight.Remove(rand);
            }
        }

        public void AddCargo()
        {
            CargoFlight rand = new CargoFlight();
            int irnd = rnd.Next(1, 4);
            if (irnd > airport.CargoFlights.Generated.Count - 1) irnd = airport.CargoFlights.Generated.Count - 1;
            for (int i = 0; i < irnd; i++)
            {
                rand = GenCargoFlight[rnd.Next(0, GenCargoFlight.Count - 1)];
                CompCargoFlight.Add(rand);
                GenCargoFlight.Remove(rand);
            }
        }

        void AddFirstPlanes()
        {
            if (TimeManager.Time.Date == DateTime.Now.Date)
            {
                PlanePassenger plane1 = new PlanePassenger();
                plane1.SetVzih511();
                CompPlanes.Add(plane1);
                CompPlanes.Add(plane1);
                Plane.Vzih511Count+=2;
                CompBalance -= plane1.PlanePrice*2;
                PlaneCargo plane2 = new PlaneCargo();
                plane2.SetVzih621();
                CompPlanes.Add(plane2);
                Plane.Vzih511Count ++;
                CompBalance -= plane2.PlanePrice;
            }
        }

        void AddOldPlane()
        {
            if (TimeManager.Time.DayOfWeek == DayOfWeek.Thursday && TimeManager.Time.Date != DateTime.Now.Date &&
                TimeManager.Time.Hour == 2 && !flag[11])
            {
                if (GenPlanes.Count != 0)
                {
                    Plane rand = new Plane();
                    rand = GenPlanes[rnd.Next(0, GenPlanes.Count - 1)];
                    CompPlanes.Add(rand);
                    GenPlanes.Remove(rand);
                    CompBalance -= rand.PlanePrice;
                    switch (rand.Model)
                    {
                        case "Ту-165": Plane.Tu165Count++; break;
                        case "Вжих-621": Plane.Vzih621Count++; break;
                        case "Вжих-511": Plane.Vzih511Count++; break;
                        case "Вжих-512": Plane.Vzih512Count++; break;
                        case "Вжих-513": Plane.Vzih513Count++; break;
                        case "Это тоже самолёт-25": Plane.EtoTozheSamolet25Count++; break;
                        case "Очень дорогой самолёт - 319": Plane.OchenDorogoySamolet319Count++; break;
                    }
                    flag[11] = true;
                }
            }
            if (TimeManager.Time.Hour != 2) flag[11] = false;
        }

        void AddNewPlane()
        {
            
            if (TimeManager.Time.DayOfWeek == DayOfWeek.Thursday && TimeManager.Time.Date != DateTime.Now.Date && 
                TimeManager.Time.Hour == 2 && !flag[10])
            {
                if (GenPlanes.Count != 0)
                {
                    Plane rand = new Plane();
                    rand = GenPlanes[rnd.Next(0, GenPlanes.Count - 1)];
                    if (rand.GetType() == typeof(PlanePassenger))
                    {
                        switch (rnd.Next(1, 5))
                        {

                            case 1: ((PlanePassenger)rand).SetVzih511(); Plane.Vzih511Count++; break;
                            case 2: ((PlanePassenger)rand).SetVzih512(); Plane.Vzih512Count++; break;
                            case 3: ((PlanePassenger)rand).SetVzih513(); Plane.Vzih513Count++; break;
                            case 4: ((PlanePassenger)rand).SetEtoTozheSamolet25(); Plane.EtoTozheSamolet25Count++; break;
                            case 5: ((PlanePassenger)rand).SetOchenDorogoySamolet319(); Plane.OchenDorogoySamolet319Count++; break;
                        }
                    }
                    else
                    {
                        switch (rnd.Next(1, 2))
                        {
                            case 1: ((PlaneCargo)rand).SetTu165(); Plane.Tu165Count++; break;
                            case 2: ((PlaneCargo)rand).SetVzih621(); Plane.Vzih621Count++; break;
                        }
                    }
                    CompPlanes.Add(rand);
                    CompBalance -= rand.PlanePrice;
                    flag[10] = true;
                }
            }
            if (TimeManager.Time.Hour != 2) flag[10] = false;
        }

        public void UpdateAll()
        {
            GenPassFlight = airport.PassengerFlights.Generated;
            GenCargoFlight = airport.CargoFlights.Generated;
            GenPlanes = airport._buyOrRentSecondlyForm.HangarPlanes;
            GenCrew = airport._workersForm.Workers;
            UpdateFlights(TimeManager.Time);
            PerformCompPassFlights(TimeManager.Time);
            PerformCompCargoFlights(TimeManager.Time);
            AddNewPlane();
            AddOldPlane();
            ChangeAverageSalary();
            AddCrew();
            DismissCrew();
            SellPlane();
            Strike();
            SelfAd();
            CheckBalance();
            CompAd();
            Death();
            PlayerAd();
            PlayerStrike();
            airport._buyOrRentSecondlyForm.HangarPlanes = GenPlanes;
            airport._workersForm.Workers = GenCrew;
        }

        void Update()
        {
            AddPass();
            AddCargo();
            airport.PassengerFlights.Generated = GenPassFlight;
            airport.CargoFlights.Generated = GenCargoFlight;
        }

        void AddFirstCrew()
        {
            if (TimeManager.Time.Date == DateTime.Now.Date)
            {
                Worker rand = new Worker();
                for (int i=0; i<8; i++)
                {
                    if (GenCrew != null && GenCrew.Count != 0)
                    {
                        rand = GenCrew[rnd.Next(0, GenCrew.Count - 1)];
                        Compcrew.Add(rand);
                        GenCrew.Remove(rand);
                        AverageSalaryComp += rand.MinSalary;
                    }
                }
                AverageSalaryComp /= 8;
                airport.WorkersForm.Workers = GenCrew;
            }
        }

        void AddCrew()
        {
            if (TimeManager.Time.DayOfWeek == DayOfWeek.Thursday && TimeManager.Time.Date != DateTime.Now.Date &&
                TimeManager.Time.Hour == 3 && !flag[12])
            {
                Worker rand = new Worker();
                for (int i=0; i<5; i++)
                {
                    if (GenCrew.Count != 0)
                    {
                        rand = GenCrew[rnd.Next(0, GenCrew.Count - 1)];
                        Compcrew.Add(rand);
                        GenCrew.Remove(rand);
                        AverageSalaryComp += rand.MinSalary/5;
                    }
                }
                flag[12] = true;
            }
            if (TimeManager.Time.Hour != 3) flag[12] = false;
        }

        void DismissCrew()
        {
            if (TimeManager.Time.DayOfWeek == DayOfWeek.Friday && TimeManager.Time.Date != DateTime.Now.Date &&
                TimeManager.Time.Hour == 2 && !flag[13])
            {
                int rand = rnd.Next(3, 6);
                if (rand > Compcrew.Count-1) rand = Compcrew.Count - 1;
                for (int i = 0; i < rand; i++)
                {
                    if (Compcrew.Count != 0)
                    {
                        Compcrew.Remove(Compcrew[rnd.Next(0, Compcrew.Count-1)]);
                        AverageSalaryComp -= rnd.Next(1000, 3000);
                    }
                }
                SellPlane();
                flag[13] = true;
            }
            if (TimeManager.Time.Hour != 2) flag[13] = false;
        }

        void SellPlane()
        {
            if (CompPlanes.Count != 0)
            {
                Plane rand = CompPlanes[rnd.Next(0, CompPlanes.Count - 1)];
                switch (rand.Model)
                {
                    case "Ту-165": Plane.Tu165Count--; break;
                    case "Вжих-621": Plane.Vzih621Count--; break;
                    case "Вжих-511": Plane.Vzih511Count--; break;
                    case "Вжих-512": Plane.Vzih512Count--; break;
                    case "Вжих-513": Plane.Vzih513Count--; break;
                    case "Это тоже самолёт-25": Plane.EtoTozheSamolet25Count--; break;
                    case "Очень дорогой самолёт - 319": Plane.OchenDorogoySamolet319Count--; break;
                }
                CompPlanes.Remove(rand);
                CompBalance += rand.PlanePrice - rnd.Next(1000, 4000);
            }
        }

        void SalaryWorkers() 
        {
            if (TimeManager.Time.Day == 1)
            {
                CompBalance -= Convert.ToInt32(Math.Round(AverageSalaryComp * Compcrew.Count));
            }
        }

        void ChangeAverageSalary()
        {
            if (TimeManager.Time.Minute > 0)
            {
                if ((CompBalance / 100000) % 2 == 0 && !flag[14])
                {
                    AverageSalaryComp *= 1.5;
                    flag[14] = true;
                }
                
            }
            if ((CompBalance / 100000) % 2 == 1) flag[14] = false;
            //каждые 200 тысяч
        }

        void ChangeReputation()
        {
            Reputation += rnd.Next(-30, 30);
        }

        void Strike()
        { 
            if (IsStrike)
            {
                CompBalance -= rnd.Next(20000, 150000);
                Reputation -= 20;
                AverageSalaryComp *= 1.2;
                IsStrike = false;
            }
        }

        void StrikeRand() //раз в месяц забастовка у конкурента без нашего ведома
        {
            if (TimeManager.Time.Day == strikeDay)
            {
                IsStrike = true;
            }
        }

        void PlayerStrike() //забастовка для игрока
        {
            if (TimeManager.Time >= playerStrike)
            {
                int i = 0;
                if (CompBalance > 100000)
                {
                    if (airport._myScheduledFlights.Count != 0)
                    {
                        CompBalance -= 100000;
                        while (airport._myScheduledFlights[i].Status != ScheduledFlight.EStatus.Pending && i < airport._myScheduledFlights.Count - 1) i++;
                        if (i == airport._myScheduledFlights.Count - 1) airport.Planes[rnd.Next(0, airport.Planes.Count - 1)].IsStrike = true;
                        else
                            airport.Planes[i].IsStrike = true;
                        airport.Reputation -= 20;
                    }
                }
                playerAd = TimeManager.Time.AddDays(rnd.Next(10, 30));
            }
        }

        void PlayerAd() //антиреклама для игрока
        {
            if (TimeManager.Time >= playerAd )
            {
                if (CompBalance > 70000)
                {
                    CompBalance -= 70000;
                    MessageBox.Show("Ваш конкурент написал в СМИ плохое о Вас");
                    airport.Reputation -= rnd.Next(20, 60);
                }
                playerAd = TimeManager.Time.AddDays(rnd.Next(10, 30));
            }
        }

        void CompAd()//антиреклама от игрока
        {
            if (AntiAd)
            {
                Reputation -= rnd.Next(20, 60);
                AntiAd = false;
            }
        }

        void SelfAd()
        {
            if (Reputation <= 30)
            {
                CompBalance -= 50000;
                Reputation += rnd.Next(20, 60);
            }
        }

        void Death()
        {
            if (TimeManager.Time >= DateDeath) CompBalance = -1;
        }

        void CheckBalance()
        {
            if (CompBalance<0)
            {
                if (MessageBox.Show("Вы выиграли, конкурент побежден. Сделать нового конкурента?", "Выигрыш", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    CompPassFlight = new List<PassengerFlight>();
                    CompCargoFlight = new List<CargoFlight>();
                    CompPlanes = new List<Plane>();
                    Reputation = 100;
                    AverageSalaryComp = 0;
                    CompBalance = 100000;
                    AddFirstCrew();
                    AddFirstPlanes();
                    DateDeath = TimeManager.Time.AddDays(rnd.Next(10, 50));
                    DateDeath = DateDeath.AddHours(rnd.Next(1, 23));
                    playerAd = TimeManager.Time.AddDays(rnd.Next(5, 15));
                    playerAd = playerAd.AddHours(rnd.Next(1, 23));
                    playerStrike = TimeManager.Time.AddDays(rnd.Next(5, 15));
                    playerStrike = playerStrike.AddHours(rnd.Next(1, 23));
                }
                else { airport.IsCompFail = true; airport.Close(); }
            }
        }
 
        public void UpdateFlights(DateTime time)
        {
            if (time.Hour == h1 && !flag[0])
            {
                flag[0] = true;
                Update();
                ChangeReputation();
            }
            else if (time.Hour == h2 && !flag[1])
            {
                flag[1] = true;
                Update();
            }

            else if (time.Hour == h3 && !flag[2])
            {
                flag[2] = true;
                Update();
                ChangeReputation();
            }
            else if (time.Hour == h4 && !flag[3])
            {
                flag[3] = true;
                Update();
            }
        }

        public void PerformCompPassFlights(DateTime time)
        {
            if (time.Hour % 2 == 0 && !flag[4])
            {
                if (CompPassFlight.Count != 0)
                {
                    if (time.Hour >= CompPassFlight[CompPassFlight.Count - 1].Peak.Begin && time.Hour <= CompPassFlight[CompPassFlight.Count - 1].Peak.End)
                    {
                        if (Reputation < 50) CompBalance += rnd.Next(15000, 25000);
                        if (Reputation > 50 && Reputation < 100) CompBalance += rnd.Next(20000, 30000);
                        if (Reputation > 100) CompBalance += rnd.Next(25000, 35000);
                    }
                    else
                    {
                        if (Reputation < 50) CompBalance += rnd.Next(6000, 12000);
                        if (Reputation > 50 && Reputation < 100) CompBalance += rnd.Next(9000, 15000);
                        if (Reputation > 100) CompBalance += rnd.Next(11000, 17000);
                    }
                    CompPassFlight.RemoveAt(CompPassFlight.Count - 1);
                    flag[4] = true;
                }
            }
            if (time.Hour % 2 == 1) flag[4] = false;
            
            if (time.Hour == hforfeit && !flag[5])
            {
                if (CompPassFlight.Count != 0)
                {
                    CompBalance -= CompPassFlight[CompPassFlight.Count - 1].Forfeit;
                    flag[5] = true;
                    CompPassFlight.RemoveAt(CompPassFlight.Count - 1);
                }
            }
            if (time.Hour != hforfeit) flag[5] = false;

            if ((time.Hour == hempty1 || time.Hour == hempty2) && !flag[6])
            {
                    CompBalance -= rnd.Next(10000, 40000);
                    flag[6] = true;
            }
            if (time.Hour != hempty1 && time.Hour != hempty2) flag[6] = false;
        }

        public void PerformCompCargoFlights(DateTime time)
        {
            if (time.Hour % 2 == 0 && !flag[7])
            {
                if (CompCargoFlight.Count != 0)
                {
                    if (Reputation < 50) CompBalance += rnd.Next(3000, 7000);
                    if (Reputation > 50 && Reputation < 100) CompBalance += rnd.Next(5000, 10000);
                    if (Reputation > 100) CompBalance += rnd.Next(9000, 13000);
                    CompCargoFlight.RemoveAt(CompCargoFlight.Count - 1);
                    flag[7] = true;
                }
            }
            if (time.Hour % 2 == 1) flag[7] = false;

            if (time.Hour == hforfeit && !flag[8])
            {
                if (CompCargoFlight.Count != 0)
                {
                    CompBalance -= CompCargoFlight[CompCargoFlight.Count - 1].Forfeit;
                    flag[8] = true;
                    CompCargoFlight.RemoveAt(CompCargoFlight.Count - 1);
                }
            }
            if (time.Hour != hforfeit) flag[8] = false;

            if ((time.Hour == hempty1 || time.Hour == hempty2) && !flag[9])
            {
                CompBalance -= rnd.Next(10000, 30000);
                flag[9] = true;
            }
            if (time.Hour != hempty1 && time.Hour != hempty2) flag[9] = false;
        }

        void ForfeitComp()
        {
            for (int i = CompPassFlight.Count-1; i>=0; i--)
            {
                if (CompPassFlight[i].DateEnd <= TimeManager.Time)
                {
                    CompBalance -= CompPassFlight[i].Forfeit;
                    CompPassFlight.Remove(CompPassFlight[i]);
                }
            }
            for (int i = CompCargoFlight.Count-1; i >=0 ; i--)
            {
                if (CompCargoFlight[i].DateEnd <= TimeManager.Time)
                {
                    CompBalance -= CompCargoFlight[i].Forfeit;
                    CompCargoFlight.Remove(CompCargoFlight[i]);
                }
            }
        }

        public void SettingsComp()
        {
            h1 = rnd.Next(1, 6);
            h2 = rnd.Next(7, 12);
            h3 = rnd.Next(13, 20);
            h4 = rnd.Next(20, 23);
            hempty2 = rnd.Next(1, 23);
            hempty1 = rnd.Next(1, 23);
            hforfeit = rnd.Next(1, 23);
            for (int i = 0; i < flag.Length; i++) flag[i] = false;
            MessageBox.Show(h1.ToString() + " " + h2.ToString() + " " + h3.ToString() + " " + h4.ToString() 
                + " " + hforfeit.ToString() + " " + hempty1.ToString() + " " +hempty2.ToString() + Environment.NewLine +
                "Каждый четверг покупается новый и старый самолеты в 2 часа, в 3 набирает экипаж, каждую пятницу в 2 продает самолеты и экипаж" +
                Environment.NewLine + "Дата смерти конкурента " + DateDeath.ToString() +
                Environment.NewLine + "Дата забастовки " + playerStrike.ToString() +
                Environment.NewLine + "Дата антирекламы " + playerAd.ToString());//тест
            ForfeitComp();
            AddFirstPlanes();
            AddFirstCrew();
            SalaryWorkers();
            if (TimeManager.Time.Day == 1)
            {
                strikeDay = rnd.Next(1, 28);
            }
            StrikeRand();
        }
    }
}
